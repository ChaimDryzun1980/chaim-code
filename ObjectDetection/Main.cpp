//objectTrackingTutorial.cpp

//Written by  Kyle Hounslow 2013

//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software")
//, to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//IN THE SOFTWARE.

#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN

#include <winsock2.h>
#include <Ws2tcpip.h>
#include <stdio.h>

// Link with ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")

#include <sstream>
#include <string>
#include <iostream>
#include <math.h>
#include <time.h>
#include <algorithm> 

#include <opencv2/opencv.hpp>
#include <opencv2\videoio.hpp>
#include <opencv2\video\tracking.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\flann.hpp>
#include <opencv2\flann\miniflann.hpp>

#include <Eigen/Dense>

#include <json/json.h>
#include <json/writer.h>

#include <ctype.h>

#include "ImageProcessingFunctions.h"
#include "DLT.h"
#include "Optimization.h"

//using namespace cv;
//using namespace std;

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.283185307179586476925286766559
#define DEGREE_TO_RAD 0.01745329251994329576923690768489
#define RAD_TO_DEGREE 57.295779513082320876798154814105

//initial min and max HSV filter values.
//these will be changed using trackbars

int low_H_bar = 0, low_S_bar = 0, low_V_bar = 0;
int high_H_bar = 180, high_S_bar = 255, high_V_bar = 255;
int Canny_High = 500, Canny_Low = 0;
int Hough_Angle = 360, Hough_AccumThresh = 30, Hough_RHO = 1, Hough_minLineLength = 5, Hough_maxLineGap = 20;
int ScreenAlpha = 500;
int low_H_phone = 0, low_S_phone = 0, low_V_phone = 0;
int high_H_phone = 180, high_S_phone = 255, high_V_phone = 255;


//default capture width and height
int FRAME_WIDTH = 640;
int FRAME_HEIGHT = 480;

//max number of objects to be detected in frame
const int MAX_NUM_OBJECTS = 50;

//minimum and maximum object area
const int MIN_OBJECT_AREA = 20 * 20;
int MAX_OBJECT_AREA = floor(FRAME_HEIGHT * FRAME_WIDTH / 1.5);

const std::string trackbarWindowName = "Trackbars";

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

void on_trackbar(int, void*)
{//This function gets called whenever a
	// trackbar position is changed
}

std::string intToString(int number) {
	std::stringstream ss;
	ss << number;
	return ss.str();
}

void createTrackbars() {
	//create window for trackbars


	cv::namedWindow(trackbarWindowName, 0);
	//create memory to store trackbar name on window
	char TrackbarName[50];
	sprintf(TrackbarName, "H_MIN_BAR %d", low_H_bar);
	sprintf(TrackbarName, "H_MAX_BAR %d", high_H_bar);
	sprintf(TrackbarName, "S_MIN_BAR %d", low_S_bar);
	sprintf(TrackbarName, "S_MAX_BAR %d", high_S_bar);
	sprintf(TrackbarName, "V_MIN_BAR %d", low_V_bar);
	sprintf(TrackbarName, "V_MAX_BAR %d", high_V_bar);
	sprintf(TrackbarName, "Alpha %d", ScreenAlpha);
	
	sprintf(TrackbarName, "H_MIN_PHN %d", low_H_phone);
	sprintf(TrackbarName, "H_MAX_PHN %d", high_H_phone);
	sprintf(TrackbarName, "S_MIN_PHN %d", low_S_phone);
	sprintf(TrackbarName, "S_MAX_PHN %d", high_S_phone);
	sprintf(TrackbarName, "V_MIN_PHN %d", low_V_phone);
	sprintf(TrackbarName, "V_MAX_PHN %d", high_V_phone);

	//sprintf(TrackbarName, "Canny_MIN %d", Canny_Low);
	//sprintf(TrackbarName, "Canny_MAX %d", Canny_High);
	//sprintf(TrackbarName, "Hgh_RHO %d", Hough_RHO);
	//sprintf(TrackbarName, "Hgh_Angle %d", Hough_Angle);
	//sprintf(TrackbarName, "Hgh_Accum %d", Hough_AccumThresh);
	//sprintf(TrackbarName, "Hgh_minLine %d", Hough_minLineLength);
	//sprintf(TrackbarName, "Hgh_maxGap %d", Hough_maxLineGap);
	
		
	//create trackbars and insert them into window
	//3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.H_LOW),
	//the max value the trackbar can move (eg. H_HIGH), 
	//and the function that is called whenever the trackbar is moved(eg. on_trackbar)
	//                                  ---->    ---->     ---->      
	cv::createTrackbar("H_MIN_BAR", trackbarWindowName, &low_H_bar, 180, on_trackbar);
	cv::createTrackbar("H_MAX_BAR", trackbarWindowName, &high_H_bar, 180, on_trackbar);
	cv::createTrackbar("S_MIN_BAR", trackbarWindowName, &low_S_bar, 255, on_trackbar);
	cv::createTrackbar("S_MAX_BAR", trackbarWindowName, &high_S_bar, 255, on_trackbar);
	cv::createTrackbar("V_MIN_BAR", trackbarWindowName, &low_V_bar, 255, on_trackbar);
	cv::createTrackbar("V_MAX_BAR", trackbarWindowName, &high_V_bar, 255, on_trackbar);
	cv::createTrackbar("Alpha", trackbarWindowName, &ScreenAlpha, 1000, on_trackbar);

	cv::createTrackbar("H_MIN_PHN", trackbarWindowName, &low_H_phone, 180, on_trackbar);
	cv::createTrackbar("H_MAX_PHN", trackbarWindowName, &high_H_phone, 180, on_trackbar);
	cv::createTrackbar("S_MIN_PHN", trackbarWindowName, &low_S_phone, 255, on_trackbar);
	cv::createTrackbar("S_MAX_PHN", trackbarWindowName, &high_S_phone, 255, on_trackbar);
	cv::createTrackbar("V_MIN_PHN", trackbarWindowName, &low_V_phone, 255, on_trackbar);
	cv::createTrackbar("V_MAX_PHN", trackbarWindowName, &high_V_phone, 255, on_trackbar);


	//cv::createTrackbar("Canny_MIN", trackbarWindowName, &Canny_Low, 500, on_trackbar);
	//cv::createTrackbar("Canny_MAX", trackbarWindowName, &Canny_High, 500, on_trackbar);
	//cv::createTrackbar("Hgh_RHO", trackbarWindowName, &Hough_RHO, 10, on_trackbar);
	//cv::createTrackbar("Hgh_Angle", trackbarWindowName, &Hough_Angle, 720, on_trackbar);
	//cv::createTrackbar("Hgh_Accum", trackbarWindowName, &Hough_AccumThresh, 500, on_trackbar);
	//cv::createTrackbar("Hgh_minLine", trackbarWindowName, &Hough_minLineLength, 500, on_trackbar);
	//cv::createTrackbar("Hgh_maxGap", trackbarWindowName, &Hough_maxLineGap, 100, on_trackbar);
	

}

void drawObject(int x, int y, cv::Mat &frame) {

	//use some of the openCV drawing functions to draw crosshairs
	//on your tracked image!

	//UPDATE:JUNE 18TH, 2013
	//added 'if' and 'else' statements to prevent
	//memory errors from writing off the screen (ie. (-25,-25) is not within the window!)

	cv::circle(frame, cv::Point(x, y), 20, cv::Scalar(0, 255, 0), 2);
	if (y - 25 > 0)
		line(frame, cv::Point(x, y), cv::Point(x, y - 25), cv::Scalar(0, 255, 0), 2);
	else line(frame, cv::Point(x, y), cv::Point(x, 0), cv::Scalar(0, 255, 0), 2);
	if (y + 25 < FRAME_HEIGHT)
		line(frame, cv::Point(x, y), cv::Point(x, y + 25), cv::Scalar(0, 255, 0), 2);
	else line(frame, cv::Point(x, y), cv::Point(x, FRAME_HEIGHT), cv::Scalar(0, 255, 0), 2);
	if (x - 25 > 0)
		line(frame, cv::Point(x, y), cv::Point(x - 25, y), cv::Scalar(0, 255, 0), 2);
	else line(frame, cv::Point(x, y), cv::Point(0, y), cv::Scalar(0, 255, 0), 2);
	if (x + 25 < FRAME_WIDTH)
		line(frame, cv::Point(x, y), cv::Point(x + 25, y), cv::Scalar(0, 255, 0), 2);
	else line(frame, cv::Point(x, y), cv::Point(FRAME_WIDTH, y), cv::Scalar(0, 255, 0), 2);

	putText(frame, intToString(x) + "," + intToString(y), cv::Point(x, y + 30), 1, 1, cv::Scalar(0, 255, 0), 2);

}

void trackFilteredObject(int &x, int &y, cv::Mat threshold, cv::Mat &cameraFeed) {

	cv::Mat temp;
	threshold.copyTo(temp);
	//these two vectors needed for output of findContours
	std::vector< std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	
	//find contours of filtered image using openCV findContours function
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	//use moments method to find our filtered object
	double refArea = 0;
	bool objectFound = false;
	if (hierarchy.size() > 0) {
		int numObjects = (int)hierarchy.size();
		//if number of objects greater than MAX_NUM_OBJECTS we have a noisy filter
		if (numObjects < MAX_NUM_OBJECTS) {
			for (int index = 0; index >= 0; index = hierarchy[index][0]) {

				cv::Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;

				//if the area is less than 20 px by 20px then it is probably just noise
				//if the area is the same as the 3/2 of the image size, probably just a bad filter
				//we only want the object with the largest area so we safe a reference area each
				//iteration and compare it to the area in the next iteration.
				if (area > MIN_OBJECT_AREA && area<MAX_OBJECT_AREA && area>refArea) {
					x = floor(moment.m10 / area);
					y = floor(moment.m01 / area);
					objectFound = true;
					refArea = area;
				}
				else objectFound = false;


			}
			//let user know you found an object
			if (objectFound == true) {
				putText(cameraFeed, "Tracking Object", cv::Point(0, 50), 2, 1, cv::Scalar(0, 255, 0), 2);
				//draw object location on screen
				drawObject(x, y, cameraFeed);
			}

		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", cv::Point(0, 50), 1, 2, cv::Scalar(0, 0, 255), 2);
	}
}

struct InputParams
{
	unsigned short m_iPort;
	int m_iCameraID, m_iCameraWidth, m_iCameraHight, m_iLowH, m_iHighH, m_iLowS, m_iHighS, m_iLowV, m_iHighV, m_iPointsPerSide, m_iCannyLow, m_iCannyHigh;
	int m_iLowHphone, m_iHighHphone, m_iLowSphone, m_iHighSphone, m_iLowVphone, m_iHighVphone, m_iGrayWidth, m_iGrayHight, m_iGrayInterpolation;
	bool m_bFlipX, m_bFlipY, m_bVerbose, m_bDisplay, m_bCalibrate;
	double m_fFocal, m_fMx, m_fMy, m_fPhoneWidth, m_fPhoneHight, m_dAlpha;
	std::string m_sLog, m_sAddress;
	RealityPlus::PnP_Method m_ePNP_Type;
	bool m_bPNP_RANSAC;

	InputParams()
	{
		m_iCannyLow = 50;
		m_iCannyHigh = 2;
		m_iCameraID = 0;
		m_iCameraWidth = 1220;
		m_iCameraHight = 960;
		m_iLowH = 60; 
		m_iHighH = 90;
		m_iLowS = 50; 
		m_iHighS = 255; 
		m_iLowV = 50;
		m_iHighV = 255;
		m_iLowHphone = 0;
		m_iHighHphone = 255;
		m_iLowSphone = 0;
		m_iHighSphone = 110;
		m_iLowVphone = 0;
		m_iHighVphone = 70;
		m_iPort = 2000;
		m_iPointsPerSide = 4;
		m_iGrayWidth = 128;
		m_iGrayHight = 64;
		m_iGrayInterpolation = 1;
		m_fFocal = 1.2516155117130024e+03;
		m_fMx = 40.0; 
		m_fMy = 40.0;
		m_fPhoneWidth = 8.0;
		m_fPhoneHight = 16.0;
		m_dAlpha = 0.75;
		m_bFlipX = false;
		m_bFlipY = false;
		m_bVerbose = true;
		m_bDisplay = false;
		m_bCalibrate = false;
		m_sLog = "Out.json";
		m_sAddress = "192.168.1.136";
		m_ePNP_Type = RealityPlus::PnP_Method::UPNP;
		m_bPNP_RANSAC = false;
	}
};

struct MyEulerAngles
{
	double m_dPitch, m_dYaw, m_dRoll;
	MyEulerAngles()
	{
		m_dPitch = m_dYaw = m_dRoll = 0.0;
	}
};

int main(int argc, char* argv[])
{
	std::string InputJsonFile = "InputJSON.json";

	if (argc > 1)
		InputJsonFile = argv[1];

	Json::Value jsonData;
	std::ifstream test(InputJsonFile, std::ifstream::binary);
	Json::CharReaderBuilder jsonReader;
	JSONCPP_STRING errs;

	if (!Json::parseFromStream(jsonReader, test, &jsonData, &errs))
	{
		std::cout << "Could not parse input JSON" << std::endl;
		std::cout << errs << std::endl;
		system("pause");
		return 1;
	}

	int AvrageNum = 5;
	int GrayPerFrames = 10;
	InputParams Prms;
	Prms.m_iCameraID = jsonData.get("CAMERA_ID", 0).asInt();
	Prms.m_iCameraHight = jsonData.get("CAMERA_HIGHT", 960).asInt();
	Prms.m_iCameraWidth = jsonData.get("CAMERA_WIDTH", 1220).asInt();
	Prms.m_iGrayHight = jsonData.get("GRAY_HIGHT", 64).asInt();
	Prms.m_iGrayWidth = jsonData.get("GRAY_WIDTH", 128).asInt();
	Prms.m_iGrayInterpolation = std::abs(jsonData.get("GRAY_INTERPOLATION", 1).asInt());
	Prms.m_iLowH = jsonData.get("LOW_H_BAR", 80).asInt();
	Prms.m_iHighH = jsonData.get("HIGH_H_BAR", 95).asInt();
	Prms.m_iLowS = jsonData.get("LOW_S_BAR", 50).asInt();
	Prms.m_iHighS = jsonData.get("HIGH_S_BAR", 255).asInt();
	Prms.m_iLowV = jsonData.get("LOW_V_BAR", 70).asInt();
	Prms.m_iHighV = jsonData.get("HIGH_V_BAR", 255).asInt();
	Prms.m_iLowHphone = jsonData.get("LOW_H_PHONE", 0).asInt();
	Prms.m_iHighHphone = jsonData.get("HIGH_H_PHONE", 255).asInt();
	Prms.m_iLowSphone = jsonData.get("LOW_S_PHONE", 0).asInt();
	Prms.m_iHighSphone = jsonData.get("HIGH_S_PHONE", 110).asInt();
	Prms.m_iLowVphone = jsonData.get("LOW_V_PHONE", 0).asInt();
	Prms.m_iHighVphone = jsonData.get("HIGH_V_PHONE", 70).asInt();
	Prms.m_iPort = (unsigned short)jsonData.get("PORT", 2000).asInt();
	Prms.m_iPointsPerSide = jsonData.get("POINTS_PER_SIDE", 4).asInt();
	Prms.m_iCannyLow = jsonData.get("CANNY_LOW", 60).asInt();
	Prms.m_iCannyHigh = jsonData.get("CANNY_HIGH", 120).asInt();
	Hough_Angle = jsonData.get("HOUGH_ANGLE", 360).asInt();
	Hough_AccumThresh = jsonData.get("HOUGH_ACCUM_THRESHOLD", 30).asInt();
	Hough_RHO = jsonData.get("HOUGH_RHO", 1).asInt();
	Hough_minLineLength = jsonData.get("HOUGH_MIN_LINE", 5).asInt();
	Hough_maxLineGap = jsonData.get("HOUGH_MAX_GAP", 20).asInt();
	AvrageNum = jsonData.get("AVERAGE", 5).asInt();
	GrayPerFrames = jsonData.get("GRAY_FRAMES", 10).asInt();
	Prms.m_fMx = jsonData.get("MX", 40.0).asDouble();
	Prms.m_fMy = jsonData.get("MY", 40.0).asDouble();
	Prms.m_dAlpha = jsonData.get("ALPHA", 0.75).asDouble();
	Prms.m_fFocal = jsonData.get("FOCAL", 1.2516155117130024e+03).asDouble();
	Prms.m_fPhoneHight = jsonData.get("PHONE_HIGHT", 16.0).asDouble();
	Prms.m_fPhoneWidth = jsonData.get("PHONE_WIDTH", 8.0).asDouble();
	Prms.m_bFlipX = jsonData.get("FLIP_X", false).asBool();
	Prms.m_bFlipY = jsonData.get("FLIP_Y", false).asBool();
	Prms.m_bVerbose = jsonData.get("VERBOSE", true).asBool();
	Prms.m_bDisplay = jsonData.get("SHOW_IMAGE", false).asBool();
	Prms.m_bCalibrate = jsonData.get("CALIBRATE", false).asBool();
	//Prms.m_sLog = jsonData.get("LOG_FILE", "Out.json").asString();
	//Prms.m_sAddress = jsonData.get("ADDRESS", "192.168.1.136").asString();

	if ((Prms.m_dAlpha < 0.0) || (Prms.m_dAlpha > 1.0))
		Prms.m_dAlpha = 0.75;

	int PNP = jsonData.get("PNP_TYPE", 3).asInt();

	if (PNP > 3)
		Prms.m_bPNP_RANSAC = true;
	else 
		Prms.m_bPNP_RANSAC = false;

	switch (PNP)
	{
	case 0:
	case 4:
		Prms.m_ePNP_Type = RealityPlus::PnP_Method::ITERATIVE;
		break;		
	case 1:
	case 5:
		Prms.m_ePNP_Type = RealityPlus::PnP_Method::EPNP;
		break;
	case 2:
	case 6:
		Prms.m_ePNP_Type = RealityPlus::PnP_Method::DLS;
		break;
	case 3:
	case 7:
		Prms.m_ePNP_Type = RealityPlus::PnP_Method::UPNP;
		break;
	default:
		Prms.m_ePNP_Type = RealityPlus::PnP_Method::UPNP;
		Prms.m_bPNP_RANSAC = false;
		break;
	}

	if (Prms.m_bVerbose)
	{
		std::cout << "Successfully parsed JSON data" << std::endl;
		std::cout << "\nJSON data received:" << std::endl;
		//std::cout << jsonData.toStyledString() << std::endl;
	}

	srand(time(NULL));

	cv::VideoCapture capture(Prms.m_iCameraID);
	
	// Check if camera opened successfully
	if (!capture.isOpened()) {
		std::cout << "Error opening video stream or file" << std::endl;
		return -1;
	}

	if (Prms.m_bVerbose)
		std::cout << "\nInitial Camera W = " << capture.get(cv::CAP_PROP_FRAME_WIDTH) << "\t H = " << capture.get(cv::CAP_PROP_FRAME_HEIGHT) << "\n";

	capture.set(cv::CAP_PROP_FRAME_WIDTH, Prms.m_iCameraWidth);
	capture.set(cv::CAP_PROP_FRAME_HEIGHT, Prms.m_iCameraHight);

	if (Prms.m_bVerbose)
		std::cout << "\nSet Camera W = " << capture.get(cv::CAP_PROP_FRAME_WIDTH) << "\t H = " << capture.get(cv::CAP_PROP_FRAME_HEIGHT) << "\n";

	low_H_bar = Prms.m_iLowH;
	low_S_bar = Prms.m_iLowS;
	low_V_bar = Prms.m_iLowV;
	high_H_bar = Prms.m_iHighH;
	high_S_bar = Prms.m_iHighS;
	high_V_bar = Prms.m_iHighV;

	low_H_phone = Prms.m_iLowHphone;
	low_S_phone = Prms.m_iLowSphone;
	low_V_phone = Prms.m_iLowVphone;
	high_H_phone = Prms.m_iHighHphone;
	high_S_phone = Prms.m_iHighSphone;
	high_V_phone = Prms.m_iHighVphone;


	Canny_Low = Prms.m_iCannyLow;
	Canny_High = Prms.m_iCannyHigh;
	ScreenAlpha = Prms.m_dAlpha * 1000;

	if (Prms.m_bCalibrate)
	{
		createTrackbars();
	}

	int iResult;
	WSADATA wsaData;

	SOCKET SendSocket = INVALID_SOCKET;
	sockaddr_in RecvAddr;

	char SendBuf[1024];
	int BufLen = 1024;

	std::ofstream file_id;
	file_id.open(Prms.m_sLog, std::ofstream::out | std::ofstream::trunc);

	//----------------------
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		wprintf(L"WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	//---------------------------------------------
	// Create a socket for sending data
	SendSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (SendSocket == INVALID_SOCKET) {
		wprintf(L"socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Set up the RecvAddr structure with the IP address of
	// the receiver (in this example case "192.168.1.1")
	// and the specified port number.
	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_port = htons(Prms.m_iPort);
	RecvAddr.sin_addr.s_addr = inet_addr(Prms.m_sAddress.c_str());

	cv::Mat gray,image;

	gray = cv::Mat(cv::Size(Prms.m_iGrayWidth, Prms.m_iGrayHight), CV_8UC1, cv::Scalar(0, 0, 0));
	
	std::string FileName = "r:\light_cancellator_image.png";

	cv::TermCriteria termcrit(cv::TermCriteria::COUNT | cv::TermCriteria::EPS, 20, 0.03);
	cv::Size subPixWinSize(10, 10), winSize(31, 31);
	bool needToInit = true;
	//bool addRemovePt = true;

	double FocalLengh = Prms.m_fFocal;
	cv::Point2d ProjectionCenter;
	cv::Mat CV_Rotation;
	cv::Mat CV_Translation;
	std::vector<cv::Point3d> CV_ModelPoints;
	std::vector<cv::Point3d> CV_ModelCornerPoints;
	std::vector<cv::Point2d> CV_ImagePoints;
	
	CV_ModelCornerPoints.push_back(cv::Point3d(0.0, 0.0, 0.0));
	CV_ModelCornerPoints.push_back(cv::Point3d(0.0, Prms.m_fPhoneHight, 0.0));
	CV_ModelCornerPoints.push_back(cv::Point3d(Prms.m_fPhoneWidth, Prms.m_fPhoneHight, 0.0));
	CV_ModelCornerPoints.push_back(cv::Point3d(Prms.m_fPhoneWidth, 0.0, 0.0));

	int HowManyPointsPerSide = Prms.m_iPointsPerSide;
			
	cv::Point3d BeginPoint3D, EndPoint3D, TmpCountorPoint3D;
	for (int i = 0; i < (int)(CV_ModelCornerPoints.size()); i++)
	{
		BeginPoint3D = CV_ModelCornerPoints[i];
		EndPoint3D = CV_ModelCornerPoints[(i + 1) % 4];

		CV_ModelPoints.push_back(BeginPoint3D);
		for (int j = 1; j < HowManyPointsPerSide; j++)
		{
			TmpCountorPoint3D.x = ((((float)j) / ((float)HowManyPointsPerSide)) * EndPoint3D.x) + ((1.0 - (((float)j) / ((float)HowManyPointsPerSide))) * BeginPoint3D.x);
			TmpCountorPoint3D.y = ((((float)j) / ((float)HowManyPointsPerSide)) * EndPoint3D.y) + ((1.0 - (((float)j) / ((float)HowManyPointsPerSide))) * BeginPoint3D.y);
			TmpCountorPoint3D.z = ((((float)j) / ((float)HowManyPointsPerSide)) * EndPoint3D.z) + ((1.0 - (((float)j) / ((float)HowManyPointsPerSide))) * BeginPoint3D.z);
			CV_ModelPoints.push_back(TmpCountorPoint3D);
		}
	}	   

	ProjectionCenter = cv::Point2d(capture.get(cv::CAP_PROP_FRAME_WIDTH) * 0.5, capture.get(cv::CAP_PROP_FRAME_HEIGHT) * 0.5);

	int MaxLineSize = std::max(capture.get(cv::CAP_PROP_FRAME_WIDTH), capture.get(cv::CAP_PROP_FRAME_HEIGHT));
	
	if (Prms.m_bDisplay) {
		cv::namedWindow("Original Frame", cv::WINDOW_NORMAL);
		cv::namedWindow("HSV Bar", cv::WINDOW_NORMAL);
		cv::namedWindow("Canny", cv::WINDOW_NORMAL);
	}
		
	std::vector<cv::Point2f> points[2];
	std::vector<cv::Point2f> PhoneBoxPoints;
	std::vector<std::vector<cv::Point> > contoursHSV;

	int FrameCount = 0, ActualFrames = 0;
	float ScreenHeight = (Prms.m_fPhoneHight * 0.5625), ScreenWidth = Prms.m_fPhoneHight, PhoneHeight = Prms.m_fPhoneHight, PhoneWidth = Prms.m_fPhoneWidth, Mx = Prms.m_fMx, My = Prms.m_fMy;

	Eigen::MatrixXf OrigBox, OrigBoundingBox, Box;
	Eigen::Matrix2f RotationMatrix;
	Eigen::Matrix2f ScaleMatrix;
	
	OrigBoundingBox.resize(5, 2);
	OrigBoundingBox(0, 0) = -(PhoneWidth*Mx*0.5);		OrigBoundingBox(0, 1) = -(PhoneHeight*My*0.5);
	OrigBoundingBox(1, 0) = (PhoneWidth*Mx*0.5);		OrigBoundingBox(1, 1) = -(PhoneHeight*My*0.5);
	OrigBoundingBox(2, 0) = (PhoneWidth*Mx*0.5);		OrigBoundingBox(2, 1) = (PhoneHeight*My*0.5);
	OrigBoundingBox(3, 0) = -(PhoneWidth*Mx*0.5);		OrigBoundingBox(3, 1) = (PhoneHeight*My*0.5);
	OrigBoundingBox(4, 0) = 0.0;						OrigBoundingBox(4, 1) = -(PhoneHeight*My*0.33);

	OrigBox.resize(5, 2);
	OrigBox(0, 0) = -(ScreenWidth*Mx*0.5);		OrigBox(0, 1) = -(ScreenHeight*My*0.5);
	OrigBox(1, 0) = (ScreenWidth*Mx*0.5);		OrigBox(1, 1) = -(ScreenHeight*My*0.5);
	OrigBox(2, 0) = (ScreenWidth*Mx*0.5);		OrigBox(2, 1) = (ScreenHeight*My*0.5);
	OrigBox(3, 0) = -(ScreenWidth*Mx*0.5);		OrigBox(3, 1) = (ScreenHeight*My*0.5);
	OrigBox(4, 0) = 0.0;		OrigBox(4, 1) = ((PhoneHeight*My*0.5) + (ScreenHeight*My*0.5));

	bool FindPhoneHSV = false;
	int ApproxDegree = -1;;
	double CalcAngle, HSV_Mask_Area = -100.0, Phone_Area = -100.0;

	std::vector<cv::Point> hull, ApproxHull;
	cv::RotatedRect BB;
	
	Json::Value JsonValues;

	clock_t tm;

	cv::Mat Frame, HSV, HSV_threshold, RGB_threshold, canny_output_HSV, canny_output_RGB;
	cv::Mat HSV_Bar, HSV_MASK;
	cv::Mat Combine_threshold, canny_Combine;
	
	cv::Mat ContDrawing;
	cv::RNG rng(12345);

	double Perimeter;

	std::vector<MyEulerAngles> LastAngles;
	MyEulerAngles GivenAngles;

	// Reading data
	for (;;)
	{
		tm = clock();

		Prms.m_dAlpha = ((float)ScreenAlpha / 1000.0);

		capture >> Frame;
		if (Frame.empty())
			break;

		if (FocalLengh < 0.0)
		{
			FocalLengh = 1.2516155117130024e+03; // Approximate focal length.
			ProjectionCenter = cv::Point2d(Frame.cols / 2, Frame.rows / 2);
		}

		if (Prms.m_bFlipY)
			cv::flip(Frame, Frame, 0);

		if (Prms.m_bFlipX)
			cv::flip(Frame, Frame, 1);

		Frame.copyTo(image);
		std::vector<cv::Vec4i> hierarchy;
		contoursHSV.clear();

		RealityPlus::WhiteBalance(image);
		
		// Initial Image processing the input frame
		cv::cvtColor(image, HSV, cv::COLOR_BGR2HSV);
		cv::inRange(HSV, cv::Scalar(low_H_phone, low_S_phone, low_V_phone), cv::Scalar(high_H_phone, high_S_phone, high_V_phone), HSV_threshold);
		cv::inRange(HSV, cv::Scalar(low_H_bar, low_S_bar, low_V_bar), cv::Scalar(high_H_bar, high_S_bar, high_V_bar), HSV_Bar);
		RealityPlus::morphClean(HSV_Bar, 5, 1,2);
		cv::Canny(HSV_Bar, canny_output_HSV, 100, 200, 3, true);
		cv::findContours(canny_output_HSV, contoursHSV, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, cv::Point(0, 0));

		int SizeContoursHSV = (int)contoursHSV.size();

		if (SizeContoursHSV < 1)
			FindPhoneHSV = false;
		else
			FindPhoneHSV = true;

		/// Find the convex hull object for each contour
		std::vector<float> AreaHSV;
		for (int i = 0; i < SizeContoursHSV; i++)
		{
			cv::convexHull(cv::Mat(contoursHSV[i]), hull, false);
			BB = cv::minAreaRect(hull);
			//Perimeter = 0.05*cv::arcLength(hull, true);
			//cv::approxPolyDP(hull, ApproxHull, Perimeter, true);
			//BB = cv::minAreaRect(ApproxHull);
			//if ((ApproxHull.size() < 3) || ((BB.size.width*BB.size.height) < 3))
			//{
			//	contoursHSV.erase(contoursHSV.begin() + i);
			//	i--;
			//	SizeContoursHSV--;
			//	continue;
			//}
			AreaHSV.push_back((BB.size.width*BB.size.height));
		}

		float TmpAreaHSV;
		for (int i = 0; i < (SizeContoursHSV - 1); i++)
		{
			for (int j = i + 1; j < SizeContoursHSV; j++)
			{
				if (AreaHSV[i] < AreaHSV[j])
				{
					hull = contoursHSV[i];
					contoursHSV[i] = contoursHSV[j];
					contoursHSV[j] = hull;

					TmpAreaHSV = AreaHSV[i];
					AreaHSV[i] = AreaHSV[j];
					AreaHSV[j] = TmpAreaHSV;
				}
			}
		}

		HSV_MASK = cv::Mat(HSV_Bar.size(), CV_8UC1, cv::Scalar(0, 0, 0));
		if (SizeContoursHSV > 0) {
			cv::convexHull(cv::Mat(contoursHSV[0]), hull, false);
			BB = cv::minAreaRect(hull);

			cv::Mat TmpCheck;
			TmpCheck = cv::Mat(HSV_Bar.size(), CV_8UC1, cv::Scalar(0, 0, 0));
				
			cv::Point TmpNewPoint;
		
			Box = OrigBoundingBox;

			ScaleMatrix(0, 0) = 1.17 * (std::max(BB.size.width, BB.size.height) / (Mx * 6.9));
			ScaleMatrix(1, 1) = 1.01 * (std::min(BB.size.width, BB.size.height) / (My * 1.31));
			ScaleMatrix(1, 0) = 0.0;
			ScaleMatrix(0, 1) = 0.0;

			while (BB.angle > 45.0)
				BB.angle -= 90.0;

			while (BB.angle < -45.0)
				BB.angle += 90.0;

			CalcAngle = BB.angle * DEGREE_TO_RAD;

			Eigen::Rotation2Df t(CalcAngle);
			RotationMatrix = t.toRotationMatrix();

			Box = (RotationMatrix * (ScaleMatrix * (Box.transpose()))).transpose();

			std::vector<cv::Point> BoundingBoxPoints;
			for (int i = 0; i < 4; i++)
			{
				TmpNewPoint.x = Box(i, 0) + BB.center.x - Box(4, 0);
				TmpNewPoint.y = Box(i, 1) + BB.center.y - Box(4, 1);
				BoundingBoxPoints.push_back(TmpNewPoint);
			}

			double A_Term = 0.0, B_Term = 0.0;
			for (int i = 0; i < 4; i++)
			{
				A_Term += BoundingBoxPoints[i].x * BoundingBoxPoints[(i + 1) % 4].y;
				B_Term += BoundingBoxPoints[i].y * BoundingBoxPoints[(i + 1) % 4].x;
			}
			HSV_Mask_Area = (A_Term - B_Term)*0.5;			
			cv::fillConvexPoly(HSV_MASK, BoundingBoxPoints, cv::Scalar(255, 255, 255), 8, 0);

			//const cv::Point *pts = (const cv::Point*) cv::Mat(BoundingBoxPoints).data;
			//int npts = cv::Mat(BoundingBoxPoints).rows;
			//cv::polylines(Frame, &pts, &npts, 1, true, cv::Scalar(255, 0, 0), 3, 8);
		}	
				
		cv::cvtColor(image, RGB_threshold, cv::COLOR_BGR2GRAY);
		
		if (ActualFrames == 0)
		{
			RGB_threshold.copyTo(gray);
			cv::resize(gray, gray, cv::Size(Prms.m_iGrayWidth, Prms.m_iGrayHight), 0, 0, Prms.m_iGrayInterpolation);
			cv::imwrite(FileName.c_str(), gray);
			ActualFrames++;
		}
		else if (ActualFrames == GrayPerFrames)
		{
			ActualFrames = 0;
		}
		else
		{
			ActualFrames++;
		}

		//cv::equalizeHist(RGB_threshold, RGB_threshold);
		//RealityPlus::imageBlur(RGB_threshold, 13, 3);
		cv::threshold(RGB_threshold, RGB_threshold, 64, 255, cv::THRESH_BINARY_INV);
		//cv::adaptiveThreshold(RGB_threshold, RGB_threshold, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 19, 5);
		
		cv::addWeighted(RGB_threshold, 0.5, HSV_threshold, 0.5, 0.0, Combine_threshold);
		cv::threshold(Combine_threshold, Combine_threshold, 190, 255, cv::THRESH_BINARY);

		Combine_threshold.copyTo(RGB_threshold);
		HSV_threshold.copyTo(Combine_threshold);
		
		RealityPlus::RemoveBlack(image, Combine_threshold);
		RealityPlus::RemoveBlack(image, RGB_threshold);

		cv::bitwise_and(Combine_threshold, HSV_MASK, Combine_threshold);
		cv::bitwise_and(RGB_threshold, HSV_MASK, RGB_threshold);
		cv::Canny(RGB_threshold, canny_output_RGB, Canny_Low, Canny_High, 3);

		contoursHSV.clear();
		cv::findContours(canny_output_RGB, contoursHSV, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, cv::Point(0, 0));

		/// Draw contours
		ContDrawing = cv::Mat::zeros(canny_output_RGB.size(), CV_8UC3);
		int ContSize = (int)contoursHSV.size();
		
		for (int i = 0; i < ContSize; i++)
		{
			if (contoursHSV[i].size() < 100)
			{
				contoursHSV.erase(contoursHSV.begin() + i);
				i--;
				ContSize--;
			}
		}
		std::vector<cv::Point> TmpCntr;
		for (int i = 0; i < ContSize - 1; i++)
			for (int j = i + 1; j < ContSize; j++)
			{
				if (contoursHSV[i].size() < contoursHSV[j].size())
				{
					TmpCntr = contoursHSV[i];
					contoursHSV[i] = contoursHSV[j];
					contoursHSV[j] = TmpCntr;
				}
			}


		cv::RotatedRect BBa;
		double PhoneAngle;
		double BB_Tmp_Size, BB_Size = 0;
		int BB_indx = -1;
		for (int i = 0; i < ContSize; i++)
		{
			cv::drawContours(ContDrawing, contoursHSV, i, cv::Scalar(255, 255, 255), 1, 8);
			BBa = cv::minAreaRect(contoursHSV[i]);
			BB_Tmp_Size = BBa.size.height*BBa.size.width;
			if (BB_Tmp_Size > BB_Size)
			{
				BB_Size = BB_Tmp_Size;
				BB_indx = i;
			}			
		}

		if (BB_indx >= 0)
		{
			BBa = cv::minAreaRect(contoursHSV[BB_indx]);
			cv::ellipse(ContDrawing, BBa, cv::Scalar(0, 0, 255), 3, 8);
		}

		std::vector<cv::Point> PhnPoints;
		for (int iIndx = 0; iIndx < Combine_threshold.rows; iIndx++)
			for (int jIndx = 0; jIndx < Combine_threshold.cols; jIndx++)
				if (Combine_threshold.at<unsigned char>(iIndx, jIndx) != 0)
					PhnPoints.push_back(cv::Point2d(jIndx, iIndx));
			
		if ((FindPhoneHSV) && ((needToInit) || (points[0].size() < 4)))
		{	
			FrameCount = 0;
			//std::vector<cv::Vec4i> lines;
			//std::vector<cv::Vec4i> NewLines;
			//cv::Vec4i TempLine;
			//cv::HoughLinesP(canny_output_RGB, lines, Hough_RHO, PI / Hough_Angle, Hough_AccumThresh, Hough_minLineLength, Hough_maxLineGap);
			//int LineNumber = (int)lines.size();
			//std::vector<bool> UseLine(LineNumber, true);

			//if (LineNumber < 3)
			//	FindPhoneHSV = false;

			//for (int i = 0; i < LineNumber; i++)
			//{
			//	if (!UseLine[i])
			//		continue;

			//	TempLine = lines[i];
			//	UseLine[i] = false;

			//	int LineCount = 1;

			//	double Y2Y1 = (TempLine[3] - TempLine[1]);
			//	double X2X1 = (TempLine[2] - TempLine[0]);
			//	double X2Y1 = (TempLine[2] * TempLine[1]);
			//	double X1Y2 = (TempLine[0] * TempLine[3]);
			//	Eigen::Vector2d NormA(X2X1, Y2Y1);
			//	double NormSizeA = NormA.norm();
			//	NormA.normalize();
			//	for (int j = i + 1; j < LineNumber; j++)
			//	{
			//		cv::Vec4i l = lines[j];
			//		double Dist1 = (std::fabs((Y2Y1 * l[0]) - ((X2X1)* l[1]) + X2Y1 - X1Y2) / std::sqrt(std::pow(Y2Y1, 2.0) + std::pow(X2X1, 2.0)));
			//		double Dist2 = (std::fabs((Y2Y1 * l[2]) - ((X2X1)* l[3]) + X2Y1 - X1Y2) / std::sqrt(std::pow(Y2Y1, 2.0) + std::pow(X2X1, 2.0)));

			//		Dist1 = (Dist1 < Dist2) ? Dist1 : Dist2;

			//		Eigen::Vector2d NormB((l[2] - l[0]), (l[3] - l[1]));
			//		double NormSizeB = NormB.norm();
			//		NormB.normalize();

			//		double SinQ2 = 1.0 - std::pow(std::fabs(NormA.dot(NormB)), 2.0);
			//		if ((SinQ2 <0.75) && (Dist1 < 25.0))
			//		{
			//			if (NormSizeB > NormSizeA)
			//			{
			//				TempLine = l;
			//				NormSizeA = NormSizeB;
			//			}
			//			//TempLine += l;
			//			LineCount++;
			//			UseLine[j] = false;
			//		}
			//	}
			//	//TempLine /= (LineCount);
			//	if (LineCount > 1)
			//		NewLines.push_back(TempLine);
			//}
			//int NewLinesSize = NewLines.size();

			//for (int i = 0; i < NewLinesSize; i++)
			//{
			//	cv::Vec4i l = NewLines[i];
			//	Eigen::Vector2d NormB((l[2] - l[0]), (l[3] - l[1]));
			//	double Norm = NormB.norm();
			//	if ((Norm > MaxLineSize) || (Norm < 5.0))
			//	{
			//		NewLines.erase(NewLines.begin() + i);
			//		i--;
			//		NewLinesSize--;
			//	}
			//	/*else {
			//		cv::line(Frame, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(0, 0, 255), 2, CV_AA);
			//	}*/
			//}

			//if (NewLinesSize < 3)
			//	FindPhoneHSV = false;

			//std::vector<cv::Point> HSV_Points;
			//cv::Point TmpHSV_Point;
			//if (NewLinesSize > 0)
			//{
			//	TempLine = NewLines[(NewLinesSize - 1)];
			//	cv::line(Frame, cv::Point(TempLine[0], TempLine[1]), cv::Point(TempLine[2], TempLine[3]), cv::Scalar(255, 0, 0), 1, 8);
			//}
			//for (int i = 0; i < (NewLinesSize - 1); i++)
			//{
			//	TempLine = NewLines[i];
			//	cv::line(Frame, cv::Point(TempLine[0], TempLine[1]), cv::Point(TempLine[2], TempLine[3]), cv::Scalar(255, 0, 0), 1, 8);
			//	Eigen::Vector2d NormA((TempLine[2] - TempLine[0]), (TempLine[3] - TempLine[1]));
			//	NormA.normalize();

			//	for (size_t j = i + 1; j < NewLinesSize; j++)
			//	{
			//		cv::Vec4i l = NewLines[j];
			//		Eigen::Vector2d NormB((l[2] - l[0]), (l[3] - l[1]));
			//		NormB.normalize();

			//		double CosQ = std::fabs(NormA.dot(NormB));
			//		if (CosQ < 0.5)
			//		{
			//			double x1 = TempLine[0];
			//			double y1 = TempLine[1];
			//			double x2 = TempLine[2];
			//			double y2 = TempLine[3];

			//			double x3 = l[0];
			//			double y3 = l[1];
			//			double x4 = l[2];
			//			double y4 = l[3];

			//			double X3mX4 = x3 - x4;
			//			double X1mX2 = x1 - x2;
			//			double Y3mY4 = y3 - y4;
			//			double Y1mY2 = y1 - y2;
			//			double X1Y2mY1X2 = ((x1*y2) - (y1*x2));
			//			double X3Y4mY3X4 = ((x3*y4) - (y3*x4));
			//			double Nominator = (X1mX2 * Y3mY4) - (Y1mY2 * X3mX4);


			//			double Px = (((X1Y2mY1X2 * X3mX4) - (X1mX2 * X3Y4mY3X4)) / (Nominator));
			//			double Py = (((X1Y2mY1X2 * Y3mY4) - (Y1mY2 * X3Y4mY3X4)) / (Nominator));
			//			TmpHSV_Point = cv::Point(Px, Py);
			//			HSV_Points.push_back(TmpHSV_Point);
			//			cv::circle(Frame, cv::Point(Px, Py), 5, cv::Scalar(0, 255, 0), 2, 8);
			//		}
			//	}
			//}

			ApproxHull.clear();
			//if ((FindPhoneHSV) && (HSV_Points.size() > 0))
			if((FindPhoneHSV) && (PhnPoints.size() > 0))
			{
				cv::convexHull(cv::Mat(PhnPoints), ApproxHull, false);
				//cv::convexHull(cv::Mat(PhnPoints), hull, false);
				//cv::convexHull(cv::Mat(HSV_Points), hull, false);
				//Perimeter = 0.01*cv::arcLength(hull, true);
				//cv::approxPolyDP(hull, ApproxHull, Perimeter, true);
				//cv::fillConvexPoly(Frame, ApproxHull, cv::Scalar(128, 128, 128), 8, 0);
			}

			if (ApproxHull.size() > 3)
				FindPhoneHSV = true;
			else
				FindPhoneHSV = false;

			// automatic initialization

			if (points[1].size() > 0)
				points[1].clear();

			PhoneBoxPoints.clear();
			if (FindPhoneHSV)
			{
				cv::Point2f TmpCountorPoint;
				for (int i = 0; i < ApproxHull.size(); i++)
				{
					TmpCountorPoint.x = ApproxHull[i].x;
					TmpCountorPoint.y = ApproxHull[i].y;
					//points[1].push_back(TmpCountorPoint);
					PhoneBoxPoints.push_back(TmpCountorPoint);
				}

				cv::Point2f BeginPoint, EndPoint;
				for (int i = 0; i < ApproxHull.size(); i++)
				{
					BeginPoint = PhoneBoxPoints[(i + 2) % ApproxHull.size()];
					EndPoint = PhoneBoxPoints[(i + 3) % ApproxHull.size()];

					points[1].push_back(BeginPoint);
					for (int j = 1; j < HowManyPointsPerSide; j++)
					{
						TmpCountorPoint.x = ((((float)j) / ((float)HowManyPointsPerSide)) * EndPoint.x) + ((1.0 - (((float)j) / ((float)HowManyPointsPerSide))) * BeginPoint.x);
						TmpCountorPoint.y = ((((float)j) / ((float)HowManyPointsPerSide)) * EndPoint.y) + ((1.0 - (((float)j) / ((float)HowManyPointsPerSide))) * BeginPoint.y);
						points[1].push_back(TmpCountorPoint);
					}
				}

				/*for (int i = 0; i < 2; i++)
				{
					TmpCountorPoint = PhoneBoxPoints[(i + 2) % 4];
					PhoneBoxPoints[(i + 2) % 4] = PhoneBoxPoints[i];
					PhoneBoxPoints[i] = TmpCountorPoint;
				}*/
			}
			//InitialSize = (int)points[1].size();
			//PrevSize = (int)points[1].size();

			//cornerSubPix(gray, points[1], subPixWinSize, cv::Size(-1, -1), termcrit);
		}
		//else if ((!points[0].empty()) && (FindPhoneHSV))
		//{
		//	std::vector<uchar> status;
		//	std::vector<float> err;
		//	if (prevGray.empty())
		//		gray.copyTo(prevGray);
		//	calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);
		//	size_t i, k;
		//	for (i = k = 0; i < points[1].size(); i++)
		//	{
		//		/*point = points[1][k];
		//		if (addRemovePt)
		//		{
		//			if (cv::norm(point - points[1][i]) <= 5)
		//			{
		//				addRemovePt = false;
		//				continue;
		//			}
		//		}*/
		//		if (!status[i])
		//			continue;
		//		points[1][k++] = points[1][i];
		//		//circle(image, points[1][i], 3, cv::Scalar(0, 255, 0), -1, 8);
		//	}
		//	points[1].resize(k);
		//}

		if ((points[1].size() < 3) || (ApproxDegree != 4) || (FrameCount == 60) || ((Phone_Area / HSV_Mask_Area) < 0.75) || ((Phone_Area / HSV_Mask_Area) > 1.1))
		{
			FrameCount = 0;
			needToInit = true;
			FindPhoneHSV = false;
		}
		else {
			FrameCount++;
			needToInit = true;
			FindPhoneHSV = false;
		}


		if (points[1].size() > 3)
		{
			std::vector < cv::Point> CurrentCountor;
			cv::Point TmpCurrentCountor;
			for (int i = 0; i < points[1].size(); i++)
			{
				TmpCurrentCountor.x = points[1][i].x;
				TmpCurrentCountor.y = points[1][i].y;
				CurrentCountor.push_back(TmpCurrentCountor);
			}
		
			//cv::convexHull(cv::Mat(CurrentCountor), ApproxHull, false);
			cv::convexHull(cv::Mat(CurrentCountor), hull, false);
			Perimeter = 0.1*cv::arcLength(hull, true);
			cv::approxPolyDP(hull, ApproxHull, Perimeter, true);			

			//const cv::Point *pts = (const cv::Point*) cv::Mat(ApproxHull).data;
			//int npts = cv::Mat(ApproxHull).rows;
			const cv::Point *pts = (const cv::Point*) cv::Mat(hull).data;
			int npts = cv::Mat(hull).rows;
			cv::polylines(Frame, &pts, &npts, 1, true, cv::Scalar(0, 0, 255), 3, 8);

			ApproxDegree = ApproxHull.size();
			BB = cv::minAreaRect(ApproxHull);

			Phone_Area = BB.size.width * BB.size.height;
			bool PnP_State = false;
			//if ((PhoneArea > 0) && ((FindPhone)))
			{
				cv::Point TmpNewPoint;
				std::vector<cv::Point> BoxPoints;
				for (int i = 0; i < ApproxHull.size(); i++)
				{
					TmpNewPoint.x = std::floor(ApproxHull[(i + 2) % ApproxHull.size()].x);
					TmpNewPoint.y = std::floor(ApproxHull[(i + 2) % ApproxHull.size()].y);
					BoxPoints.push_back(TmpNewPoint);
				}
								
				Box = OrigBox;

				ScaleMatrix(0, 0) = std::min(BB.size.width, BB.size.height) / (Mx * PhoneWidth);
				ScaleMatrix(1, 1) = std::max(BB.size.width, BB.size.height) / (My * PhoneHeight);
				ScaleMatrix(1, 0) = 0.0;
				ScaleMatrix(0, 1) = 0.0;

				while (BB.angle > 45.0)
					BB.angle -= 90.0;

				while (BB.angle < -45.0)
					BB.angle += 90.0;
							
				CalcAngle = BB.angle * DEGREE_TO_RAD;

				Eigen::Rotation2Df t(CalcAngle);
				RotationMatrix = t.toRotationMatrix();

				Box = (RotationMatrix * (ScaleMatrix * (Box.transpose()))).transpose();

				tm = clock() - tm;

				if (Prms.m_bVerbose)
					printf("It took me %d clicks (%f seconds).\n", tm, ((float)tm) / CLOCKS_PER_SEC);

				std::vector<cv::Point> ScreenPoints;
				for (int i = 0; i < 4; i++)
				{
					TmpNewPoint.x = Box(i, 0) + BB.center.x - Box(4, 0);
					TmpNewPoint.y = Box(i, 1) + BB.center.y - Box(4, 1);
					ScreenPoints.push_back(TmpNewPoint);
				}

				cv::Mat roi;
				Frame.copyTo(roi);
				cv::fillConvexPoly(roi, ScreenPoints, cv::Scalar(0, 0, 255), 8, 0);
				cv::addWeighted(roi, Prms.m_dAlpha, Frame, 1.0 - Prms.m_dAlpha, 0.0, Frame);
								
				CV_ImagePoints.clear();
				//for (int i = 0; i < points[1].size(); i++)
				//	CV_ImagePoints.push_back(cv::Point2d(points[1][i].x, points[1][i].y));
							
				cv::Point2d BeginPoint, EndPoint, TmpCountorPoint;
				for (int i = 0; i < ApproxHull.size(); i++)
				{
					BeginPoint.x = ApproxHull[(i + 2) % 4].x;
					BeginPoint.y = ApproxHull[(i + 2) % 4].y;

					EndPoint.x = ApproxHull[(i + 3) % 4].x;
					EndPoint.y = ApproxHull[(i + 3) % 4].y;

					CV_ImagePoints.push_back(BeginPoint);
					for (int j = 1; j < HowManyPointsPerSide; j++)
					{
						TmpCountorPoint.x = ((((float)j) / ((float)HowManyPointsPerSide)) * EndPoint.x) + ((1.0 - (((float)j) / ((float)HowManyPointsPerSide))) * BeginPoint.x);
						TmpCountorPoint.y = ((((float)j) / ((float)HowManyPointsPerSide)) * EndPoint.y) + ((1.0 - (((float)j) / ((float)HowManyPointsPerSide))) * BeginPoint.y);
						CV_ImagePoints.push_back(TmpCountorPoint);
					}
				}

				bool UseInitialGuess = (FrameCount == 1) ? false : true;

				RealityPlus::PnP MyPnP;
				MyPnP.SetFocalLength(FocalLengh);
				MyPnP.SetProjectionCenter(ProjectionCenter);
				MyPnP.SetRotation(CV_Rotation);
				MyPnP.SetTranslation(CV_Translation);
				MyPnP.SetImagePoints(CV_ImagePoints);
				MyPnP.SetModelPoints(CV_ModelPoints);
				MyPnP.SetPnP_Method(Prms.m_ePNP_Type);
				MyPnP.OptimizeCameraParameters(false);
				MyPnP.UseInitialGuess(UseInitialGuess);
				MyPnP.UseRANSAC(Prms.m_bPNP_RANSAC);
				MyPnP.SetIterrationNum(100);
				PnP_State = MyPnP.CalculatePnP();
				//bool PnP_State = MyPnP.OptimizePnP();
				if (PnP_State)
				{
					CV_Rotation = MyPnP.GetRotation();
					CV_Translation = MyPnP.GetTranslation();
				}
			}

			if (PnP_State)
			{
				double X = CV_Translation.at<double>(0, 0);
				double Y = CV_Translation.at<double>(1, 0);
				double Z = CV_Translation.at<double>(2, 0);

				if (Prms.m_bVerbose)
					std::cout << "\nTranslation:\n\t" << X << ", " << Y << ", " << Z << "\n\n";

				Eigen::Vector3d Rodrigez(CV_Rotation.at<double>(0, 0), CV_Rotation.at<double>(1, 0), CV_Rotation.at<double>(2, 0));
				double RodrigezAngle = Rodrigez.norm();
				Rodrigez.normalize();

				Eigen::Matrix3d RodrigezMatrix;

				RodrigezMatrix = Eigen::AngleAxisd(RodrigezAngle, Rodrigez);
				RodrigezMatrix.transpose().eval();

				//std::cout << "Rotation:\n" << RodrigezMatrix << "\n\n";

				Eigen::Vector3d ea = RodrigezMatrix.eulerAngles(2, 1, 0);
				GivenAngles.m_dYaw = (double)ea(0);
				GivenAngles.m_dPitch = (double)ea(1);
				GivenAngles.m_dRoll = (double)ea(2);

				LastAngles.push_back(GivenAngles);
				GivenAngles.m_dYaw = 0.0;
				GivenAngles.m_dPitch = 0.0;
				GivenAngles.m_dRoll = 0.0;

				int LastAnglesSize = (int)LastAngles.size();
				for (int i = 0; i < LastAnglesSize; i++)
				{
					GivenAngles.m_dYaw += LastAngles[i].m_dYaw / (double)LastAnglesSize;
					GivenAngles.m_dPitch += LastAngles[i].m_dPitch / (double)LastAnglesSize;
					GivenAngles.m_dRoll += LastAngles[i].m_dRoll / (double)LastAnglesSize;
				}

				if (LastAnglesSize > AvrageNum)
				{
					LastAngles.erase(LastAngles.begin());
				}

				if (Prms.m_bVerbose)
					std::cout << "Euler angles:\n" << ea << "\n\n";

				JsonValues["SENDER"] = "PHONEDETECTOR";
				JsonValues["X"] = (double)X;
				JsonValues["Y"] = (double)Y;
				JsonValues["Z"] = (double)Z;
				JsonValues["YAW"] = (double)GivenAngles.m_dYaw;
				JsonValues["PITCH"] = (double)GivenAngles.m_dPitch;
				JsonValues["ROLL"] = (double)GivenAngles.m_dRoll;
			}
		} 
		else {
		}
		
		Json::StreamWriterBuilder wbuilder;
		wbuilder.settings_["commentStyle"] = "None";
		wbuilder.settings_["indentation"] = "";
		std::string document = Json::writeString(wbuilder, JsonValues);

		file_id << document << "\n";

		//---------------------------------------------
	// Send a datagram to the receiver
		iResult = sendto(SendSocket,
			document.c_str(), document.length(), 0, (SOCKADDR *)& RecvAddr, sizeof(RecvAddr));
		if (iResult == SOCKET_ERROR) {
			wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
			closesocket(SendSocket);
			WSACleanup();
			return 1;
		}

		if (Prms.m_bDisplay) {
			cv::imshow("Original Frame", Frame);
			cv::imshow("HSV Bar", HSV_Bar);
			cv::imshow("Canny", ContDrawing);
		}
		char c = (char)cv::waitKey(1);
		//if (c == 27)
		if (GetKeyState(VK_CANCEL) & 0x8000)
			break;
		
		std::swap(points[1], points[0]);
		//cv::swap(prevGray, gray);
	}

	capture.release();
	cv::destroyAllWindows();

	file_id.close();

	//---------------------------------------------
	// When the application is finished sending, close the socket.
	iResult = closesocket(SendSocket);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"closesocket failed with error: %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Clean up and quit.
	WSACleanup();

	if (Prms.m_bCalibrate)
	{
		jsonData["LOW_H_BAR"] = (int)low_H_bar;
		jsonData["LOW_S_BAR"] = (int)low_S_bar;
		jsonData["LOW_V_BAR"] = (int)low_V_bar;
		jsonData["HIGH_H_BAR"] = (int)high_H_bar;
		jsonData["HIGH_S_BAR"] = (int)high_S_bar;
		jsonData["HIGH_V_BAR"] = (int)high_V_bar;
		jsonData["ALPHA"] = ((float)ScreenAlpha / 1000.0);
		jsonData["LOW_H_PHONE"] = (int)low_H_phone;
		jsonData["LOW_S_PHONE"] = (int)low_S_phone;
		jsonData["LOW_V_PHONE"] = (int)low_V_phone;
		jsonData["HIGH_H_PHONE"] = (int)high_H_phone;
		jsonData["HIGH_S_PHONE"] = (int)high_S_phone;
		jsonData["HIGH_V_PHONE"] = (int)high_V_phone;
		//jsonData["CANNY_LOW"] = (int)Canny_Low;
		//jsonData["CANNY_HIGH"] = (int)Canny_High;
		//jsonData["HOUGH_ANGLE"] = (int)Hough_Angle;
		//jsonData["HOUGH_ACCUM_THRESHOLD"] = (int)Hough_AccumThresh;
		//jsonData["HOUGH_RHO"] = (int)Hough_RHO;
		//jsonData["HOUGH_MIN_LINE"] = (int)Hough_minLineLength;
		//jsonData["HOUGH_MAX_GAP"] = (int)Hough_maxLineGap;
		//jsonData["CALIBRATE"] = false;

		std::ofstream Tmpfile_id;
		Tmpfile_id.open(InputJsonFile, std::ofstream::out | std::ofstream::trunc);

		Json::StreamWriterBuilder wbuilderTmp;
		//wbuilderTmp.settings_["commentStyle"] = "None";
		//wbuilderTmp.settings_["indentation"] = "";
		std::string TmpDocument = Json::writeString(wbuilderTmp, jsonData);

		Tmpfile_id << TmpDocument << "\n";
		Tmpfile_id.close();
	}

	return 0;
}


//Optical Flow LK
//int main(int argc, char* argv[])
//{
//	srand(time(NULL));
//
//	//video capture object to acquire webcam feed
//	//cv::VideoCapture capture("D:\\Data\\Videos\\SmartphoneAnchoring\\VID_20190415_104550.mp4");
//	//cv::VideoCapture capture("D:\\Data\\Videos\\SmartphoneAnchoring\\VID_20190415_104740_Trim.mp4");
//	cv::VideoCapture capture("D:\\Data\\Videos\\SmartphoneAnchoring\\VID_20190415_105531.mp4");
//
//	// Check if camera opened successfully
//	if (!capture.isOpened()) {
//		std::cout << "Error opening video stream or file" << std::endl;
//		return -1;
//	}
//
//	cv::TermCriteria termcrit(cv::TermCriteria::COUNT | cv::TermCriteria::EPS, 20, 0.03);
//	cv::Size subPixWinSize(10, 10), winSize(31, 31);
//	const int MAX_COUNT = 1500;
//	bool needToInit = true;
//	bool nightMode = false;
//	bool grayMode = false;
//	//bool addRemovePt = true;
//
//	cv::namedWindow("LK Demo", 1);
//	cv::Mat gray, prevGray, image, frame;
//	std::vector<cv::Point2f> points[2];
//	std::vector<cv::Point2f> GoodPoints;
//	std::vector<double> Variables;
//	std::vector<double> StepSize;
//	Variables.resize(6);
//	StepSize.resize(6);
//
//	StepSize[0] = 5.0;
//	StepSize[1] = 5.0;
//	StepSize[2] = 0.1;
//	StepSize[3] = 0.1;
//	StepSize[4] = 1.0;
//	StepSize[5] = 1.0;
//
//	Eigen::MatrixXf OrigBox, Box;
//	Eigen::Matrix2f RotationMatrix;
//
//	OrigBox.resize(5, 2);
//	OrigBox(0, 0) = -500.0;		OrigBox(0, 1) = -300.0;
//	OrigBox(1, 0) = 500.0;		OrigBox(1, 1) = -300.0;
//	OrigBox(2, 0) = 500.0;		OrigBox(2, 1) = 300.0;
//	OrigBox(3, 0) = -500.0;		OrigBox(3, 1) = 300.0;
//	OrigBox(4, 0) = 0.0;		OrigBox(4, 1) = 300.0;
//
//	for (;;)
//	{
//		capture >> frame;
//		if (frame.empty())
//			break;
//		frame.copyTo(image);
//
//		cvtColor(image, gray, cv::COLOR_BGR2GRAY);
//
//		//GrayImageCLAHE(gray);
//		cv::equalizeHist(gray, gray);
//
//		cv::GaussianBlur(gray, gray, cv::Size(3, 3), 0);
//		//cv::adaptiveThreshold(gray, gray, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 75, 27);
//		cv::adaptiveThreshold(gray, gray, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 75, 15);
//		//cv::bitwise_not(gray, gray);
//		morphOps(gray);
//		//morphSkel(gray);
//				
//		if (nightMode)
//			image = cv::Scalar::all(0);
//
//		if ((grayMode) && (!nightMode))
//			gray.copyTo(image);
//
//		//if ((needToInit) || (points[0].size() < 6))
//		{
//			// automatic initialization
//			cv::goodFeaturesToTrack(gray, points[1], MAX_COUNT, 0.01, 4, cv::Mat());
//			cornerSubPix(gray, points[1], subPixWinSize, cv::Size(-1, -1), termcrit);
//			//addRemovePt = false;
//			size_t i, k;
//			for (i = k = 0; i < points[1].size(); i++)
//			{
//				points[1][k++] = points[1][i];
//				//circle(image, points[1][i], 3, cv::Scalar(0, 255, 0), -1, 8);
//			}
//			{
//				pcl::PointCloud<pcl::PointXY>::Ptr cloud(new pcl::PointCloud<pcl::PointXY>());
//				std::vector<bool> IsUsed;
//				pcl::PointXY Pi;
//
//				int PointsNum = points[1].size();
//
//				for (int i = 0; i < PointsNum; i++)
//				{
//					Pi.x = points[1][i].x;
//					Pi.y = points[1][i].y;
//					cloud->push_back(Pi);
//					IsUsed.push_back(false);
//				}
//
//				pcl::search::Search<pcl::PointXY>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXY> >(new pcl::search::KdTree<pcl::PointXY>);
//				tree->setInputCloud(cloud);
//				std::vector< pcl::PointCloud<pcl::PointXY>::Ptr> Clusters;
//
//				double res = 0.0;
//				int n_points = 0;
//
//				std::vector<int> ResIndices(2);
//				std::vector<float> ResSqr_distances(2);
//
//				std::vector<float> Distances;
//				double Median = 0.0;;
//
//				for (int i = 0; i < PointsNum; i++)
//				{
//					if (!pcl_isfinite((*cloud)[i].x))
//					{
//						continue;
//					}
//
//					//Considering the second neighbor since the first is the point itself.
//					int nres = tree->nearestKSearch(i, 2, ResIndices, ResSqr_distances);
//					if (nres == 2)
//					{
//						Distances.push_back(sqrt(ResSqr_distances[1]));
//						res += sqrt(ResSqr_distances[1]);
//						n_points += 1;
//					}
//				}
//				if (n_points != 0)
//				{
//					std::sort(Distances.begin(), Distances.end());
//					if ((n_points % 2) == 0)
//					{
//						Median = (Distances[std::floor(n_points * 0.5)] + Distances[std::floor(n_points * 0.5) - 1])*0.5;
//					}
//					else
//					{
//						Median = Distances[std::floor((n_points - 1) * 0.5)];
//					}
//					res /= n_points;
//				}
//
//				double MaxDist = std::pow(Median * 6.0, 2.0);
//				for (int i = 0; i < PointsNum; i++)
//				{
//					if (!pcl_isfinite((*cloud)[i].x))
//					{
//						continue;
//					}
//
//					//Considering the second neighbor since the first is the point itself.
//					int nres = tree->nearestKSearch(i, 2, ResIndices, ResSqr_distances);
//					if ((nres != 2) || (ResSqr_distances[1] > MaxDist))
//					{
//						cloud->erase(cloud->begin() + i);
//						points[1].erase(points[1].begin() + i);
//						i--;
//						PointsNum--;
//					}
//				}
//
//				for (int i = 0; i < PointsNum; i++)
//				{
//					circle(image, points[1][i], 3, cv::Scalar(0, 255, 0), -1, 8);
//				}
//
//					int Indx = 0;
//					int UnUsedPoints = PointsNum;
//
//					double MaxDistance = std::floor(10.0 * res);
//
//					tree->setInputCloud(cloud);
//					std::vector<int> indices;
//					std::vector<float> sqr_distances;
//
//					while (UnUsedPoints > 0)
//					{
//						pcl::PointCloud<pcl::PointXY>::Ptr TmpCluster(new pcl::PointCloud<pcl::PointXY>());;
//
//						Indx = -1;
//						for (int i = 0; (Indx < 0) && (i < PointsNum); i++)
//							if (!IsUsed[i])
//								Indx = i;
//
//						IsUsed[Indx] = true;
//						UnUsedPoints--;
//						std::queue<int> ClosePointsIndexes;
//
//						do {
//							TmpCluster->push_back(cloud->points[Indx]);
//							tree->radiusSearch(Indx, MaxDistance, indices, sqr_distances);
//
//							for (int i = 0; i < indices.size(); i++)
//								if ((indices[i] != Indx) && (!IsUsed[indices[i]]))
//								{
//									ClosePointsIndexes.push(indices[i]);
//									IsUsed[indices[i]] = true;
//									UnUsedPoints--;
//								}
//
//							if (!ClosePointsIndexes.empty()) {
//								Indx = ClosePointsIndexes.front(); ;
//								ClosePointsIndexes.pop();
//							}
//
//						} while (!ClosePointsIndexes.empty());
//						Clusters.push_back(TmpCluster);
//					}
//
//					cloud->clear();
//					GoodPoints.clear();
//					cv::Point2f TmpPoint;
//					for (int i = 0; i < Clusters.size(); i++)
//					{
//						if (Clusters[i]->size() < 19)
//							continue;
//
//						for (int j = 0; j < Clusters[i]->size(); j++)
//						{
//							TmpPoint.x = Clusters[i]->points[j].x;
//							TmpPoint.y = Clusters[i]->points[j].y;
//							cloud->push_back(Clusters[i]->points[j]);
//							GoodPoints.push_back(TmpPoint);
//							//circle(image, TmpPoint, 3, cv::Scalar(0,0,255), -1, 8);
//						}
//					}
//
//					std::vector<cv::Point2f> ConvexHull;
//					cv::convexHull(GoodPoints, ConvexHull);
//
//					cv::RotatedRect BB;
//					BB = cv::minAreaRect(ConvexHull);
//					
//					//SuperEllipse BoundingSE;
//					//BoundingSE.m_Angle = BB.angle * DEGREE_TO_RAD;
//					//BoundingSE.m_Center = BB.center;
//					//BoundingSE.m_Size.height =  BB.size.height * 0.5;
//					//BoundingSE.m_Size.width = BB.size.width * 0.5;
//					//BoundingSE.m_Power = 8.0;
//
//					cv::Point2f rect_points[4];
//					BB.points(rect_points);					
//
//					std::vector<cv::Point> NewPoints;
//					cv::Point TmpNewPoint;
//					for (int i = 0; i < 4; i++)
//					{
//						TmpNewPoint.x = std::floor(rect_points[i].x);
//						TmpNewPoint.y = std::floor(rect_points[i].y);
//						NewPoints.push_back(TmpNewPoint);
//					}
//					Box = OrigBox;
//
//					//Eigen::Vector2f Head;
//					//Head(0) = rect_points[0].x - rect_points[1].x;
//					//Head(1) = rect_points[0].y - rect_points[1].y;
//
//					//double CalcAngle = BB.angle * DEGREE_TO_RAD;
//					//double Scale = std::fabs(Head.norm() / 500.0);
//					//cv::Point2f Transl((rect_points[0].x + rect_points[1].x)*0.5, (rect_points[0].y + rect_points[1].y)*0.5);
//					//
//					//Eigen::Rotation2Df t(CalcAngle);
//
//					//Box *= Scale;
//					//Box = (t.toRotationMatrix() * (Box.transpose())).transpose();
//
//					//cv::Point TmpNewPoint;
//					//for (int i = 0; i < 4; i++)
//					//{
//					//	TmpNewPoint.x = Box(i, 0) + Transl.x + Box(4, 0);
//					//	TmpNewPoint.y = Box(i, 1) + Transl.y + Box(4, 1);
//					//	NewPoints.push_back(TmpNewPoint);
//					//}
//					
//					cv::fillConvexPoly(image, NewPoints, cv::Scalar(255, 0, 0), 8, 0);
//
//					//Variables[0] = BoundingSE.m_Center.x;
//					//Variables[1] = BoundingSE.m_Center.y;
//					//Variables[2] = BoundingSE.m_Angle;
//					//Variables[3] = BoundingSE.m_Power;
//					//Variables[4] = BoundingSE.m_Size.width;
//					//Variables[5] = BoundingSE.m_Size.height;
//
//
//					//float InitX, InitY, TmpX, TmpY;
//					//float CosQ = std::cos(BoundingSE.m_Angle);
//					//float SinQ = std::sin(BoundingSE.m_Angle);
//					//for (float angle = 0.0; angle < TWO_PI; angle += 0.0001)
//					//{
//					//	InitX = BoundingSE.m_Size.width * std::pow(std::fabs(std::cos(angle)), (2.0 / BoundingSE.m_Power)) * sgn<double>(cos(angle));
//					//	InitY = BoundingSE.m_Size.height * std::pow(std::fabs(std::sin(angle)), (2.0 / BoundingSE.m_Power)) * sgn<double>(sin(angle));
//					//	
//					//	TmpX = (InitX*CosQ) - (InitY*SinQ) + BoundingSE.m_Center.x;
//					//	TmpY = (InitX*SinQ) + (InitY*CosQ) + BoundingSE.m_Center.y;
//					//	
//					//	TmpPoint.x = TmpX;
//					//	TmpPoint.y = TmpY;
//
//					//	circle(image, TmpPoint, 2, cv::Scalar(0, 0, 255), -1, 8);
//					//}
//			}
//		}
//		//else if (!points[0].empty())
//		//{
//		//	std::vector<uchar> status;
//		//	std::vector<float> err;
//		//	if (prevGray.empty())
//		//		gray.copyTo(prevGray);
//		//	calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);
//		//	size_t i, k;
//		//	for (i = k = 0; i < points[1].size(); i++)
//		//	{
//		//		/*point = points[1][k];
//		//		if (addRemovePt)
//		//		{
//		//			if (cv::norm(point - points[1][i]) <= 5)
//		//			{
//		//				addRemovePt = false;
//		//				continue;
//		//			}
//		//		}*/
//		//		if (!status[i])
//		//			continue;
//		//		points[1][k++] = points[1][i];
//		//		//circle(image, points[1][i], 3, cv::Scalar(0, 255, 0), -1, 8);
//		//	}
//		//	points[1].resize(k);
//		//}
//			
//		if (points[0].size() > 5)
//		{
//			needToInit = false;
//		}
//
//
//		imshow("LK Demo", image);
//		char c = (char)cv::waitKey(1);
//		if (c == 27)
//			break;
//		switch (c)
//		{
//		case 'r':
//			needToInit = true;
//			break;
//		case 'c':
//			points[0].clear();
//			points[1].clear();
//			needToInit = true;
//			break;
//		case 'n':
//			nightMode = !nightMode;
//			needToInit = true;
//			break;
//		case 'g':
//			grayMode = !grayMode;
//			needToInit = true;
//			break;
//		}
//		std::swap(points[1], points[0]);
//		cv::swap(prevGray, gray);
//	}
//
//	return 0;
//}

//int main(int argc, char* argv[])
//{
//	//some boolean variables for different functionality within this
//	//program
//	bool trackObjects = true;
//	bool useMorphOps = true;
//
//	srand(time(NULL));
//
//	//matrix storage for HSV image
//	Mat HSV;
//	//matrix storage for binary threshold image
//	Mat threshold;
//	//x and y values for the location of the object
//	int x = 0, y = 0;
//	
//	//create slider bars for HSV filtering
//	createTrackbars();
//
//	//video capture object to acquire webcam feed
//	cv::VideoCapture capture("D:\\Data\\Videos\\SmartphoneAnchoring\\VID_20190415_104550.mp4");
//	//cv::VideoCapture capture("D:\\Data\\Videos\\SmartphoneAnchoring\\VID_20190415_104740_Trim.mp4");
//	//cv::VideoCapture capture("D:\\Data\\Videos\\SmartphoneAnchoring\\VID_20190415_105531.mp4");
//
//	// Check if camera opened successfully
//	if (!capture.isOpened()) {
//		std::cout << "Error opening video stream or file" << std::endl;
//		return -1;
//	}
//
//	cv::Mat frame;
//	capture >> frame;
//
//	FRAME_WIDTH = frame.cols;
//	FRAME_HEIGHT = frame.rows;
//	MAX_OBJECT_AREA = floor((FRAME_HEIGHT * FRAME_WIDTH) / 1.5);
//
//	Mat canny_output;
//	vector<vector<Point> > contours;
//	vector<Vec4i> hierarchy;
//	int thresh = 100;
//	RNG rng(12345);
//
//	std::vector<double> Variables;
//	Variables.resize(6);
//
//	std::vector<double> StepSize;
//	StepSize.resize(6);
//
//	StepSize[0] = 10.0;
//	StepSize[1] = 10.0;
//	StepSize[2] = 0.1;
//	StepSize[3] = 0.1;
//	StepSize[4] = 10.0;
//	StepSize[5] = 10.0;
//
//	//start an infinite loop where webcam feed is copied to cameraFeed matrix
//	//all of our operations will be performed within this loop
//	while (!frame.empty()) {
//		
//		//convert frame from BGR to HSV colorspace
//		cvtColor(frame, HSV, COLOR_BGR2HSV);
//		
//		//filter HSV image between values and store filtered image to
//		//threshold matrix
//		inRange(HSV, Scalar(H_MIN, S_MIN, V_MIN), Scalar(H_MAX, S_MAX, V_MAX), threshold);
//		
//		//perform morphological operations on thresholded image to eliminate noise
//		//and emphasize the filtered object(s)
//		if (useMorphOps)
//			morphOps(threshold);
//		
//		//pass in thresholded frame to our object tracking function
//		//this function will return the x and y coordinates of the
//		//filtered object
//		if (trackObjects)
//			trackFilteredObject(x, y, threshold, frame);
//
//		
//		/// Detect edges using canny
//		Canny(threshold, canny_output, thresh, thresh * 2, 3);
//		/// Find contours
//		findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
//
//		Variables[0] = x;
//		Variables[1] = y;
//		Variables[2] = 0.00000001;
//		Variables[3] = 5.0;
//		Variables[4] = 100.0;
//		Variables[5] = 250.0;
//
//
//		int MaxSize = -1;
//		for (int i = 0; i < contours.size(); i++) {
//			int CntI = contours[i].size();
//			if (CntI > MaxSize)
//				MaxSize = CntI;
//		}
//		//MaxSize = ceil((float)MaxSize / 2.0);
//		MaxSize -= 1;
//		for (int i = 0; i < contours.size(); i++) {
//			if (contours[i].size() < MaxSize)
//			{
//				contours.erase(contours.begin() + i);
//				i--;
//			}
//		}
//
//		for (int i = 0; i < contours.size(); i++) {
//			for (int j = 0; j < contours[i].size(); j++) {
//				int ContX = contours[i][j].x;
//				if ((ContX > 1249) || (ContX < 750))
//				{
//					contours[i].erase(contours[i].begin() + j);
//					j--;
//				}
//			}
//			int CntIsize = contours[i].size();
//			if (CntIsize < 1)
//			{
//				contours.erase(contours.begin() + i);
//				i--;
//			}
//		}
//
//
//		/// Draw contours
//		Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
//		int Step = floor(255.0 / (float)contours.size());
//		for (int i = 0; i < contours.size(); i++)
//		{
//			Scalar color = Scalar(i*Step, 0, 255 - (i*Step));
//			drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, Point());
//		}
//
//		vector<Vec4i> lines;
//		HoughLinesP(canny_output, lines, 1, CV_PI / 180, 50, 25, 25);
//		
//		cv::Vec3b GreenColor;
//		Vec4i BoundingBox;
//		BoundingBox[0] = 6500;
//		BoundingBox[1] = 6500;
//		BoundingBox[2] = 0;
//		BoundingBox[2] = 0;
//
//		std::vector<cv::Point> GoodPoints;
//		cv::Point TmpGoodPoint;
//
//		for (int i = 0; i < lines.size(); i++)
//		{
//			Vec4i l = lines[i];
//			if ((l[0] > 1249) || (l[2] > 1249) || (l[0] < 750) || (l[2] < 750))
//				continue;
//			if ((abs(l[0] - l[2]) < 5) || (abs(l[1] - l[2]) < 5))
//			{
//				if (BoundingBox[0] > l[0])
//					BoundingBox[0] = l[0];
//				if (BoundingBox[0] > l[2])
//					BoundingBox[0] = l[2];
//				if (BoundingBox[2] < l[0])
//					BoundingBox[2] = l[0];
//				if (BoundingBox[2] < l[2])
//					BoundingBox[2] = l[2];
//				if (BoundingBox[1] > l[1])
//					BoundingBox[1] = l[1];
//				if (BoundingBox[1] > l[3])
//					BoundingBox[1] = l[3];
//				if (BoundingBox[3] < l[1])
//					BoundingBox[3] = l[1];
//				if (BoundingBox[3] < l[3])
//					BoundingBox[3] = l[3];
//
//				TmpGoodPoint.x = l[0];
//				TmpGoodPoint.y = l[1];
//				GoodPoints.push_back(TmpGoodPoint);
//
//				TmpGoodPoint.x = l[2];
//				TmpGoodPoint.y = l[3];
//				GoodPoints.push_back(TmpGoodPoint);
//
//			}
//			//line(drawing, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 255, 0), 3, CV_AA);
//		}
//
//		if ((BoundingBox[0] < 6500) && (BoundingBox[1] < 6500) && (BoundingBox[2] > 0) && (BoundingBox[3] > 0))
//		{
//			Variables[0] = 0.5*(BoundingBox[0] + BoundingBox[2]);
//			Variables[1] = 0.5*(BoundingBox[1] + BoundingBox[3]);
//
//			Variables[4] = 0.5 * fabs(BoundingBox[2] - BoundingBox[0]);
//			Variables[5] = 0.5 * fabs(BoundingBox[3] - BoundingBox[1]);
//		}
//
//
//		if ((contours.size() > 0) && (contours[0].size() > 4))
//			SmartphoneDetectionOptimization::SuperEllipseFit_RANSAC(contours[0], GoodPoints, Variables, StepSize);
//		
//		double TmpX, TmpY;
//		GreenColor.val[0] = 0;
//		GreenColor.val[1] = 255;
//		GreenColor.val[2] = 0;
//		for (double angle = 0.0; angle < TWO_PI; angle += 0.0001)
//		{
//			TmpX = Variables[4] * std::pow(std::fabs(std::cos(angle)), (2.0 / Variables[3])) * sgn<double>(cos(angle));
//			TmpY = Variables[5] * std::pow(std::fabs(std::sin(angle)), (2.0 / Variables[3])) * sgn<double>(sin(angle));
//			TmpX += Variables[0];
//			TmpY += Variables[1];
//
//			if ((TmpX < 700) || (TmpX > 1500) || (TmpY < 1) || (TmpY > 1050))
//				continue;
//
//			drawing.at<Vec3b>(std::floor(TmpY), std::floor(TmpX)) = GreenColor;
//		}
//
//
//		//show frames 
//		imshow(windowName2, threshold);
//		imshow(windowName, frame);
//		imshow(windowName1, drawing);
//
//		capture >> frame;
//
//		//delay 30ms so that screen can refresh.
//		//image will not appear without this waitKey() command
//		waitKey(30);
//	}
//	return 0;
//}

