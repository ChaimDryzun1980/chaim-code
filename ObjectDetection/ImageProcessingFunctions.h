#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>

namespace RealityPlus
{
	void morphOps(cv::Mat &thresh, unsigned int size = 5, unsigned int type = 1, unsigned int Itter = 1);
	void morphClosing(cv::Mat &thresh, unsigned int size = 5, unsigned int type = 1, unsigned int Itter = 1);
	void morphOpening(cv::Mat &thresh, unsigned int size = 5, unsigned int type = 1, unsigned int Itter= 1);
	void morphClean(cv::Mat &thresh, unsigned int size = 5, unsigned int type = 1, unsigned int Itter = 1);
	void morphSkel(cv::Mat &img, unsigned int size = 5, unsigned int type = 1, unsigned int Itter = 1);
	void imageBlur(cv::Mat &img, unsigned int size = 5, unsigned int type = 1);
	void imageCLAHE(cv::Mat &img);
	void GrayImageCLAHE(cv::Mat &img);
	void RemoveBlack(cv::Mat &img);
	void RemoveBlack(const cv::Mat &ColorImg, cv::Mat &BinaryImage);
	void RGB_HistEqualization(cv::Mat &img);
	void equalizeIntensity(cv::Mat& inputImage);
	void equalizeIntensity_HSV(cv::Mat& inputImage);
	void Sharpen(const cv::Mat& myImage, cv::Mat& Result);
	void WhiteBalance(cv::Mat& inputImage);
}