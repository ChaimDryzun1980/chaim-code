#include "Optimization.h"
#include <iostream>
#include <random>
#include <ctime>
#include <math.h>
#include <Eigen/SVD>
#include <Eigen/Eigenvalues> 

double SmartphoneDetectionOptimization::SuperEllipseFunction(cv::Point2d &points, std::vector<double> &Variables)
{
	if (Variables.size() < 6)
		return INFINITY;

	if (Variables[4] < 0.1)
		Variables[4] = 0.1;

	if (Variables[5] < 0.1)
		Variables[5] = 0.1;

	if (Variables[3] < 1.9)
		Variables[3] = 1.9;

	return pow((pow(std::fabs((((points.x - Variables[0])*cos(Variables[2])) - ((points.y - Variables[1])*sin(Variables[2]))) / Variables[4]), (2.0 / Variables[3])) + pow(std::fabs((((points.y - Variables[1])*cos(Variables[2])) + ((points.x - Variables[0])*sin(Variables[2]))) / Variables[5]), (2.0 / Variables[3])) - 1.0), 2.0);
}

double SmartphoneDetectionOptimization::SuperEllipseFunction(std::vector<cv::Point2d> &points, std::vector<double> &Variables)
{
	if (Variables.size() < 6)
		return INFINITY;

	size_t PointNum = points.size();

	double Resid = 0.0;

	if (Variables[4] < 0.1)
		Variables[4] = 0.1;

	if (Variables[5] < 0.1)
		Variables[5] = 0.1;

	if (Variables[3] < 1.9)
		Variables[3] = 1.9;

	for (int i = 0; i < PointNum; i++)
	{
		Resid = pow((pow(std::fabs((((points[i].x - Variables[0])*cos(Variables[2])) - ((points[i].y - Variables[1])*sin(Variables[2]))) / Variables[4]), (Variables[3])) + pow(std::fabs((((points[i].y - Variables[1])*cos(Variables[2])) + ((points[i].x - Variables[0])*sin(Variables[2]))) / Variables[5]), (Variables[3])) - 1.0), 2.0) * (1.0 / (double)PointNum);
	}

	return Resid;
}

double SmartphoneDetectionOptimization::SuperEllipseFit_PatternSearch(std::vector<cv::Point2f> &points, std::vector<double> &Variables, std::vector<double> &StepSize, int MaxIterationNum)
{
	std::vector<cv::Point2d> NewPoints;
	cv::Point2d TmpPoint;
	for (int i = 0; i < points.size(); i++)
	{
		TmpPoint.x = points[i].x;
		TmpPoint.y = points[i].y;
		NewPoints.push_back(TmpPoint);
	}

	return SuperEllipseFit_PatternSearch(NewPoints, Variables, StepSize, MaxIterationNum);
}

double SmartphoneDetectionOptimization::SuperEllipseFit_PatternSearch(std::vector<cv::Point2d> &points, std::vector<double> &Variables, std::vector<double> &StepSize, int MaxIterationNum)
{
	//int PointNum = (int)points.size();

	int VarIndex = 0;
	bool Dir = 0;

	double GoldenRatioFactor = 2.0 / (1.0 + sqrt(5));

	double Solution = SuperEllipseFunction(points, Variables);
	double NewSol = Solution;

	std::vector<double> X = Variables;
	std::vector<double> tmpStep = StepSize;
	int i;
	int iter = 0;
	size_t Dim = Variables.size();

	//clock_t NewTime = clock();

	double AvrgStpSz = 0.0;
	for (i = 0; (i < Dim); i++) {
		AvrgStpSz += (tmpStep[i] / ((double)Dim));
	}

	int InnerItr = 0;

	double Epsilon = 0.00001;

	std::cout << "\tItr = " << iter << "\tVars = ";
	for (int VarbIndx = 0; VarbIndx < Dim; VarbIndx++)
		std::cout << Variables[VarbIndx] << " ";
	std::cout << "\tVal = " << Solution << "\tAvg Step Size = " << AvrgStpSz << "\n";

	double MinSolVal = 0.000000001;

	while ((Solution > MinSolVal) && (AvrgStpSz > Epsilon) && (iter < MaxIterationNum)) {

		for (i = 0; ((NewSol >= Solution) && (i < Dim)); i++) {
			if (Dir == 0) {
				X[(i + VarIndex) % Dim] -= tmpStep[(i + VarIndex) % Dim];
			}
			else {
				X[(i + VarIndex) % Dim] += tmpStep[(i + VarIndex) % Dim];
			}

			NewSol = SuperEllipseFunction(points, X);

			if (Dir == 0) {
				X[(i + VarIndex) % Dim] += tmpStep[(i + VarIndex) % Dim];
			}
			else {
				X[(i + VarIndex) % Dim] -= tmpStep[(i + VarIndex) % Dim];
			}
		}

		if (NewSol >= Solution) {
			InnerItr = 0;
			Dir = (Dir + 1) % 2;
			for (i = 0; ((NewSol >= Solution) && (i < Dim)); i++) {
				if (Dir == 0) {
					X[(i + VarIndex) % Dim] -= tmpStep[(i + VarIndex) % Dim];
				}
				else {
					X[(i + VarIndex) % Dim] += tmpStep[(i + VarIndex) % Dim];
				}

				NewSol = SuperEllipseFunction(points, X);

				if (Dir == 0) {
					X[(i + VarIndex) % Dim] += tmpStep[(i + VarIndex) % Dim];
				}
				else {
					X[(i + VarIndex) % Dim] -= tmpStep[(i + VarIndex) % Dim];
				}
			}

			if (NewSol >= Solution) {
				InnerItr = 0;
				VarIndex = 0;
				Dir = 0;
				AvrgStpSz = 0.0;
				for (i = 0; (i < Dim); i++) {
					tmpStep[i] *= GoldenRatioFactor;
					AvrgStpSz += (tmpStep[i] / ((double)Dim));
					if (tmpStep[i] < Epsilon) {
						tmpStep[i] = Epsilon;
					}
				}
			}
			else {
				InnerItr = 0;
				VarIndex = (i - 1 + VarIndex) % Dim;
				if (Dir == 0) {
					X[VarIndex] -= tmpStep[VarIndex];
				}
				else {
					X[VarIndex] += tmpStep[VarIndex];
				}
				Variables = X;
				Solution = NewSol;
			}
		}
		else {
			InnerItr = 0;
			VarIndex = (i - 1 + VarIndex) % Dim;
			if (Dir == 0) {
				X[VarIndex] -= tmpStep[VarIndex];
			}
			else {
				X[VarIndex] += tmpStep[VarIndex];
			}
			Variables = X;
			Solution = NewSol;

			if (InnerItr == 2) {
				InnerItr = 0;
				tmpStep[VarIndex] *= (1.0 / GoldenRatioFactor);
			}

		}

			std::cout << "\tItr = " << iter+1 << "\tVars = ";
			for (int VarbIndx = 0; VarbIndx < Dim; VarbIndx++)
				std::cout << Variables[VarbIndx] << " ";
			std::cout << "\tVal = " << Solution << "\tAvg Step Size = " << AvrgStpSz << "\n";
		iter++;
	}

	//system("pause");

	return Solution;
}

double SmartphoneDetectionOptimization::SuperEllipseFit_Fireflys(std::vector<cv::Point2d> &points, std::vector<double> &Variables, std::vector<double> &MinVar, std::vector<double> &MaxVar, int FireFlysNum, int MaxIterationNum)
{
	std::vector<std::vector<double>> FireFlys;
	FireFlys.resize(FireFlysNum);

	std::vector<double> LightIntensities;
	LightIntensities.resize(FireFlysNum);

	double Tmpvl, BestVal;

	int Dim = (int)Variables.size();

	FireFlys[0].resize(Dim);
	for (int j = 0; j < Dim; j++) {
		Tmpvl = Variables[j];
		if ((Tmpvl < MinVar[j]) || (Tmpvl > MaxVar[j])) {
			int vl = rand() % RAND_MAX;
			Tmpvl = ((double)vl / (double)RAND_MAX);
			Tmpvl *= (MaxVar[j] - MinVar[j]);
			Tmpvl += MinVar[j];
		}
		FireFlys[0][j] = Tmpvl;
	}

	LightIntensities[0] = SuperEllipseFunction(points, FireFlys[0]);
	LightIntensities[0] = (fabs(LightIntensities[0]) >= FireFlysZeps) ? (1.0 / LightIntensities[0]) : InvFireFlysZeps;
	

	for (int i = 1; i < FireFlysNum; i++) {
		do {
			FireFlys[i].resize(Dim);
			for (int j = 0; j < Dim; j++) {
				int vl = rand() % RAND_MAX;
				Tmpvl = ((double)vl / (double)RAND_MAX);
				Tmpvl *= (MaxVar[j] - MinVar[j]);
				Tmpvl += MinVar[j];
				FireFlys[i][j] = Tmpvl;
			}
			Tmpvl = SuperEllipseFunction(points, FireFlys[i]);
			Tmpvl = (fabs(Tmpvl >= FireFlysZeps)) ? (1.0 / Tmpvl) : InvFireFlysZeps;
		} while ((Tmpvl  < FireFlysZeps) || (!std::isfinite(Tmpvl)));
		LightIntensities[i] = Tmpvl;
	}

	//sort
	for (int i = 0; i < (FireFlysNum - 1); i++) {
		for (int j = i + 1; j < FireFlysNum; j++) {
			if (LightIntensities[j] < LightIntensities[i]) {
				double TmpVal = LightIntensities[j];
				LightIntensities[j] = LightIntensities[i];
				LightIntensities[i] = TmpVal;

				std::vector<double> TmpVect = FireFlys[j];
				FireFlys[j] = FireFlys[i];
				FireFlys[i] = TmpVect;
			}
		}
	}
	
	BestVal = LightIntensities[FireFlysNum - 1];
	Variables = FireFlys[FireFlysNum - 1];
	BestVal = (fabs(BestVal) > FireFlysZeps) ? (1.0 / BestVal) : InvFireFlysZeps;
	
	double Beta0 = 1.0;
	double Alpha0 = 0.05;
	double Delta = 0.96;
	double Gamma = 43.0;
	double Distance, Attractivness, GammaFactor = 1.0, RandomizationFactor = Alpha0;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0, 1.0);
	//std::normal_distribution<double> distribution(0.0, 1.0);
	int iter = 0;
	
	double MinSolVal = 1e+12;

	std::cout << "\tFireflys optimization --> Iter #" << iter << "\tBest result: " << BestVal << "\n";

	std::vector<double> TmpVect;
	TmpVect.resize(Dim);
	
	do {

		for (int i = FireFlysNum - 1; i > 0; i--) {
			for (int j = i + 1; j < FireFlysNum; j++) {

				for (int k = 0; k < Dim; k++) {
					TmpVect[k] = FireFlys[i][k] - FireFlys[j][k];
				}

				Distance = Norm2(TmpVect);
				if (!std::isfinite(Distance)) {
					Distance = 1.0;
				}

				GammaFactor = pow((fabs(LightIntensities[i]) / (fabs(LightIntensities[j]) + FireFlysZeps)), (1.0 / (iter + 1)));
				if (!std::isfinite(GammaFactor)) {
					GammaFactor = 1.0;
				}

				Attractivness = Beta0 * exp(-Gamma * GammaFactor*Distance);
				for (int k = 0; k < Dim; k++) {

					FireFlys[i][k] += Attractivness * (FireFlys[j][k] - FireFlys[i][k]);
					//m_FireFlys[i][k] = Attractivness * m_FireFlys[j][k];
					//m_FireFlys[i][k] += (1.0 - Attractivness) * m_FireFlys[i][k];
					//double chk = (distribution(generator));
					FireFlys[i][k] += (RandomizationFactor * ((distribution(generator) - 0.5)) * (MaxVar[k] - MinVar[k]) * 0.01);

					if (FireFlys[i][k] < MinVar[k]) {
						FireFlys[i][k] = MinVar[k];
					}
					if (FireFlys[i][k] > MaxVar[k]) {
						FireFlys[i][k] = MaxVar[k];
					}
				}

				//LightIntensities[i] = (fabs(LightIntensities[i]) >= FireFlysZeps) ? 1.0 / LightIntensities[i] : InvFireFlysZeps;
			}
		}

		for (int k = 0; k < Dim; k++) {
			//double chk = (distribution(generator));
			FireFlys[FireFlysNum - 1][k] += RandomizationFactor * ((distribution(generator) - 0.5)) * (MaxVar[k] - MinVar[k]) * 0.01;

			if (FireFlys[FireFlysNum - 1][k] < MinVar[k]) {
				FireFlys[FireFlysNum - 1][k] = MinVar[k];
			}
			if (FireFlys[FireFlysNum - 1][k] > MaxVar[k]) {
				FireFlys[FireFlysNum - 1][k] = MaxVar[k];
			}
		}

		for (int i = 0; i < FireFlysNum; i++) {
			Tmpvl = SuperEllipseFunction(points, FireFlys[i]);
			
			if (Tmpvl < 1.0) {
				Tmpvl *= 1.0;
			}
			if (!std::isfinite(Tmpvl)) {
				Tmpvl = 0.0;
			}

			LightIntensities[i] = (fabs(Tmpvl) >= FireFlysZeps) ? 1.0 / Tmpvl : InvFireFlysZeps;
			if (!std::isfinite(LightIntensities[i])) {
				LightIntensities[i] = InvFireFlysZeps;
			}
		}

		//sort
		for (int i = 0; i < (FireFlysNum - 1); i++) {
			for (int j = i + 1; j < FireFlysNum; j++) {
				if (LightIntensities[j] < LightIntensities[i]) {
					double TmpVal = LightIntensities[j];
					LightIntensities[j] = LightIntensities[i];
					LightIntensities[i] = TmpVal;

					std::vector<double> TmpVect = FireFlys[j];
					FireFlys[j] = FireFlys[i];
					FireFlys[i] = TmpVect;
				}
			}
		}

		BestVal = LightIntensities[FireFlysNum - 1];
		Variables = FireFlys[FireFlysNum - 1];
		BestVal = (fabs(BestVal) > FireFlysZeps) ? 1.0 / BestVal : InvFireFlysZeps;
		
		RandomizationFactor *= Delta;
		iter++;

		std::cout << "\tFireflys optimization --> Iter #" << iter << "\tBest result: " << BestVal << "\n";
	} while ((LightIntensities[FireFlysNum - 1] < MinSolVal) && (iter < MaxIterationNum));

	MinSolVal = (fabs(MinSolVal) > FireFlysZeps) ? 1.0 / MinSolVal : InvFireFlysZeps;
	
	return BestVal;
}

double  SmartphoneDetectionOptimization::SuperEllipseFit_RANSAC(std::vector<cv::Point> &InputPoints, std::vector<cv::Point> &GoodPoints, std::vector<double> &Variables, std::vector<double> &StepSize)
{
	//int InitialSize = 15;
	double Treshold = 0.15;

	size_t PointNum = InputPoints.size();

	std::vector<cv::Point2d> AllPoints;
	AllPoints.resize(PointNum);

	std::vector<cv::Point2d> InitialPoints;

	for (int i = 0; i < PointNum; i++)
	{
		AllPoints[i].x = (double)InputPoints[i].x;
		AllPoints[i].y = (double)InputPoints[i].y;
	}

	std::vector<cv::Point2d>InlierPoints;

	std::set<int> Used;
	std::set<int>::iterator it;

	int InLiers = 0;
	double Residual, BestResidual = 10000000000.0;
	//int RANSAC_runs = 0 , MaxRANSAC_runs = 10;

	std::vector<double> BestVariables;
	size_t Dim = Variables.size();
	BestVariables.resize(Dim);
	for (int i = 0; i < Dim; i++)
		BestVariables[i] = Variables[i];

	std::vector<double> MinVar;
	MinVar.resize(Dim);

	std::vector<double> MaxVar;
	MaxVar.resize(Dim);
		
	MinVar[0] = 10.0;			MaxVar[0] = 1000.0;
	MinVar[1] = 10.0;			MaxVar[1] = 1000.0;
	MinVar[2] = -TWO_PI;		MaxVar[2] = TWO_PI;
	MinVar[3] = 1.9;			MaxVar[3] = 50.0;
	MinVar[4] = 10.0;			MaxVar[4] = 600.0;
	MinVar[5] = 10.0;			MaxVar[5] = 600.0;

	cv::Point2d TmpGoodPoint;

	//while ((BestResidual > 0.000001) && (RANSAC_runs < MaxRANSAC_runs))
	//{

		do {
			InitialPoints.clear();
			Used.clear();
			//int Index;

			//for (int i = 0; i < InitialSize; i++)
			//{
			//	do {
			//		Index = rand() % PointNum;
			//		it = Used.find(Index);
			//	} while (it != Used.end());
			//	Used.insert(Index);
			//	InitialPoints.push_back(AllPoints[Index]);
			//}

			for (int i = 0; i < GoodPoints.size(); i++)
			{
				TmpGoodPoint.x = (double)GoodPoints[i].x;
				TmpGoodPoint.y = (double)GoodPoints[i].y;
				InitialPoints.push_back(TmpGoodPoint);
			}

			//if (RANSAC_runs == 0)
			//	Residual = SuperEllipseFit_Fireflys(InitialPoints, Variables, MinVar, MaxVar, 75, 75);

			Residual = SuperEllipseFit_PatternSearch(InitialPoints, Variables, StepSize, 500);
			if ((Variables[0] < 1) || (Variables[1] < 1) || (Variables[0] > 1000) || (Variables[1] > 1000) || (Variables[5] < 4) || (Variables[4] < 4))
			{
				Residual = 20000000000.0;
				for (int i = 0; i < Dim; i++)
					Variables[i] = BestVariables[i];
				Residual = SuperEllipseFit_Fireflys(InitialPoints, Variables, MinVar, MaxVar, 25, 10);
			}
		} while (Residual >= 10000000000.0);


		InlierPoints.clear();
		InLiers = 0;
		for (int i = 0; i < PointNum; i++)
		{
			Residual = SuperEllipseFunction(AllPoints[i], Variables);
			if (Residual < Treshold)
			{
				InlierPoints.push_back(AllPoints[i]);
				InLiers++;
			}
		}

		Residual = SuperEllipseFit_PatternSearch(InlierPoints, Variables, StepSize, 250);
		if ((Variables[0] < 1) || (Variables[1] < 1) || (Variables[0] > 1000) || (Variables[1] > 1000) || (Variables[5] < 4) || (Variables[4] < 4))
		{
			for (int i = 0; i < Dim; i++)
				Variables[i] = BestVariables[i];
		}

		//Residual += (1.0 - ((double)InLiers / (double)PointNum));

		//if (Residual < BestResidual)
		else{
			BestResidual = Residual;
			for (int i = 0; i < Dim; i++)
				BestVariables[i] = Variables[i];
		}

		//RANSAC_runs++;

		//std::cout << "RANSAC --> Iter: " << RANSAC_runs << "\tResult: " << Residual << "\tBest: " << BestResidual << "\n";*/
	//}

	for (int i = 0; i < Dim; i++)
		Variables[i] = BestVariables[i];

	return BestResidual;
}

double SmartphoneDetectionOptimization::Norm2(std::vector<double> Vect)
{
	double VectNorm2 = 0.0;
	for (int i = 0; i < Vect.size(); i++)
		VectNorm2 += Vect[i] * Vect[i];

	return VectNorm2;
}


void  SmartphoneDetectionOptimization::EllipseFit(std::vector<cv::Point2f> &InputPoints, std::vector<double> &Variables)
{
	int PointNum = (int)InputPoints.size();

	Eigen::VectorXd D;
	D.resize(6);

	Eigen::MatrixXd DtD, S, C;

	S.resize(6, 6);
	C.resize(6, 6);

	S.setZero();
	C.setZero();


	C(0, 2) = 2.0;
	C(2, 0) = 2.0;
	C(1, 1) = -1.0;

	double x, y;

	for (int i = 0; i < PointNum; i++)
	{
		x = (double)InputPoints[i].x;
		y = (double)InputPoints[i].y;
		D(0) = x * x;
		D(1) = x * y;
		D(2) = y * y;
		D(3) = x;
		D(4) = y;
		D(5) = 1.0;

		DtD = D * D.transpose();

		S += DtD;
	}

	Eigen::MatrixXd SinvC;
	SinvC = (S.inverse())*C;


	Eigen::EigenSolver<Eigen::MatrixXd> es(SinvC);
	Eigen::VectorXcd v_complex = es.eigenvectors().col(0);

	Eigen::VectorXd v = v_complex.real();

	double b, c, d, f, g, a;
	a = v(0);
	b = v(1) *0.5;
	c = v(2);
	d = v(3) *0.5;
	f = v(4) *0.5;
	g = v(5);
	double 	num = (b * b) - (a * c);
	double x0 = ((c * d) - (b * f)) / num;
	double y0 = ((a * f) - (b * d)) / num;

	double up = 2.0 * ((a * f * f) + (c * d * d) + (g * b * b) - (2 * b * d * f) - (a * c * g));
	double down1 = ((b * b) - (a * c))*((c - a)*std::sqrt(1 + 4 * b*b / ((a - c)*(a - c))) - (c + a));
	double down2 = (b*b - a * c)*((a - c)*std::sqrt(1 + 4 * b*b / ((a - c)*(a - c))) - (c + a));
	double res1 = std::sqrt(up / down1);
	double res2 = std::sqrt(up / down2);

	if ((std::isnormal(res1)) && (!std::isnormal(res2)))
		res2 = res1;

	if ((std::isnormal(res2)) && (!std::isnormal(res1)))
		res1 = res2;

	double angle = 0.0;
	if (b == 0)
	{
		if (a > c)
			angle = 0.0;
		else
			angle = HALF_PI;
	}
	else
	{
		if (a > c)
			angle = std::atan(2.0 * b / (a - c))*0.5;
		else
			angle = (HALF_PI * 0.5) + std::atan(2.0 * b / (a - c))*0.5;
	}

	Variables.resize(6);
	Variables[0] = x0;
	Variables[1] = y0;
	Variables[2] = angle;
	Variables[3] = 5.0;
	Variables[4] = res1;
	Variables[5] = res2;
}

void  SmartphoneDetectionOptimization::EllipseFit_RANSAC(std::vector<cv::Point2f> &InputPoints, std::vector<double> &Variables)
{
	size_t PointNum = InputPoints.size();

	std::vector<cv::Point2d> AllPoints;
	AllPoints.resize(PointNum);

	for (int i = 0; i < PointNum; i++)
	{
		AllPoints[i].x = (double)InputPoints[i].x;
		AllPoints[i].y = (double)InputPoints[i].y;
	}

	std::vector<cv::Point2f> InitialPoints;
	std::vector<cv::Point2d>InlierPoints;

	std::set<int> Used;
	std::set<int>::iterator it;

	int InLiers = 0;
	double Residual, BestResidual = 10000000000.0;
	int RANSAC_runs = 0, MaxRANSAC_runs = 1000;

	std::vector<double> BestVariables;
	size_t Dim = Variables.size();
	BestVariables.resize(Dim);
	for (int i = 0; i < Dim; i++)
		BestVariables[i] = Variables[i];

	int  InitialSize = 15;
	double Treshold = 0.5;

	std::vector<double> StepSize;
	StepSize.resize(6);

	StepSize[0] = 10.0;
	StepSize[1] = 10.0;
	StepSize[2] = 0.1;
	StepSize[3] = 0.1;
	StepSize[4] = 10.0;
	StepSize[5] = 10.0;

	while (RANSAC_runs < MaxRANSAC_runs)
	{
		InitialPoints.clear();
		Used.clear();
		int Index;

		for (int i = 0; i < InitialSize; i++)
		{
			do {
				Index = rand() % PointNum;
				it = Used.find(Index);
			} while (it != Used.end());
			Used.insert(Index);
			InitialPoints.push_back(InputPoints[Index]);
		}

		EllipseFit(InitialPoints, Variables);
	
	InlierPoints.clear();
	InLiers = 0;
	for (int i = 0; i < PointNum; i++)
	{
		Residual = SuperEllipseFunction(AllPoints[i], Variables);
		if (Residual < Treshold)
		{
			InlierPoints.push_back(AllPoints[i]);
			InLiers++;
		}
	}

	Residual = SuperEllipseFit_PatternSearch(InlierPoints, Variables, StepSize, 250);
	if ((Variables[0] < 1) || (Variables[1] < 1) || (Variables[0] > 1000) || (Variables[1] > 1000) || (Variables[5] < 4) || (Variables[4] < 4))
	{
		Residual = 10000000000.0;
		for (int i = 0; i < Dim; i++)
			Variables[i] = BestVariables[i];
	}

	Residual += (1.0 - ((double)InLiers / (double)PointNum));

	if (Residual < BestResidual)
	{
		BestResidual = Residual;
		for (int i = 0; i < Dim; i++)
			BestVariables[i] = Variables[i];
	}

	RANSAC_runs++;

	std::cout << "RANSAC --> Iter: " << RANSAC_runs << "\tResult: " << Residual << "\tBest: " << BestResidual << "\n";
  }

	for (int i = 0; i < Dim; i++)
		Variables[i] = BestVariables[i];

}
