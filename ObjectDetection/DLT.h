#pragma once
#include <Eigen/Dense>
#include <sstream>
#include <string>
#include <iostream>
#include <math.h>
#include <time.h>
#include <algorithm> 
#include <vector>

#include <opencv2/opencv.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>

namespace RealityPlus
{
	enum PnP_Method
	{
		ITERATIVE =0,
		EPNP,
		DLS,
		UPNP,
		AP3P
	};

	class DLT
	{
	public:
		DLT() {}
		~DLT() {}

		void SetPoints2D(const std::vector< Eigen::Vector2f> &vPoints2D)
		{
			m_vPoints2D.resize(vPoints2D.size(), 2);
			for (int i = 0; i < vPoints2D.size(); i++)
			{
				m_vPoints2D(i, 0) = vPoints2D[i](0);
				m_vPoints2D(i, 1) = vPoints2D[i](1);
			}
		}

		void SetPoints2D(const Eigen::MatrixX2d &vPoints2D)
		{
			m_vPoints2D = vPoints2D;
		}

		void SetPoints3D(const std::vector< Eigen::Vector3f> &vPoints3D)
		{
			m_vPoints3D.resize(vPoints3D.size(), 3);
			for (int i = 0; i < vPoints3D.size(); i++)
			{
				m_vPoints3D(i, 0) = vPoints3D[i](0);
				m_vPoints3D(i, 1) = vPoints3D[i](1);
				m_vPoints3D(i, 2) = vPoints3D[i](2);
			}
		}

		void SetPoints3D(const Eigen::MatrixX3d &vPoints3D)
		{
			m_vPoints3D = vPoints3D;
		}


		void CalcDLT();

	private:
		Eigen::MatrixX2d m_vPoints2D;
		Eigen::MatrixX3d m_vPoints3D;
	};

	class PnP
	{
	public:
		PnP() 
		{
			m_bUseRANSAC = false;
			m_bUseInitialGuess = false;
			m_bOptimizeCameraParams = false;
			m_dFocal = -1.0;
			m_vModelPoints.clear();
			m_vImagePoints.clear();
			m_iIterration = 100;
			m_ePnP_Method = UPNP;
		}
		
		~PnP() {}

		void SetModelPoints(const std::vector<cv::Point3d> &ModelPoints)
		{
			m_vModelPoints = ModelPoints;
		}

		void SetImagePoints(const std::vector<cv::Point2d> &ImagePoints)
		{
			m_vImagePoints = ImagePoints;
		}

		void SetFocalLength(const double FocalLength)
		{
			m_dFocal = FocalLength;
		}

		void SetProjectionCenter(const cv::Point2d &ProjectionCenter)
		{
			m_vProjectionCenter = ProjectionCenter;
		}

		void SetRotation(const cv::Mat &Rotation)
		{
			m_vRotation = Rotation;
		}

		void SetTranslation(const cv::Mat &Translation)
		{
			m_vTranslation = Translation;
		}

		void OptimizeCameraParameters(const bool OptimizeCameraParams)
		{
			m_bOptimizeCameraParams = OptimizeCameraParams;
		}

		void UseRANSAC(const bool UseRANSAC)
		{
			m_bUseRANSAC = UseRANSAC;
		}

		void UseInitialGuess(const bool UseInitialGuess)
		{
			m_bUseInitialGuess = UseInitialGuess;
		}

		void SetIterrationNum(const int Iterration)
		{
			m_iIterration = Iterration;
		}

		double GetFocalLength()
		{
			return m_dFocal;
		}

		cv::Point2d GetProjectionCenter()
		{
			return m_vProjectionCenter;
		}

		cv::Mat GetRotation()
		{
			return m_vRotation;
		}

		cv::Mat GetTranslation()
		{
			return m_vTranslation;
		}

		bool CalculatePnP();

		void SetPnP_Method(const PnP_Method Method)
		{
			m_ePnP_Method = Method;
		}
		
		//bool OptimizePnP();
		//double ProjectionError(std::vector<double> X);

	private:
		bool m_bUseRANSAC;
		bool m_bUseInitialGuess;
		bool m_bOptimizeCameraParams;
		double m_dFocal;
		cv::Point2d m_vProjectionCenter;
		cv::Mat m_vRotation;
		cv::Mat m_vTranslation;
		std::vector<cv::Point3d> m_vModelPoints;
		std::vector<cv::Point2d> m_vImagePoints;
		int m_iIterration;
		PnP_Method m_ePnP_Method;	
	};
	
}


