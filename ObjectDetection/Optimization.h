#pragma once

#include <iostream>
#include <math.h>
#include <time.h>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv\highgui.h>
#include <opencv\cv.h>

#include <Eigen/Dense>

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.283185307179586476925286766559
#define HALF_PI 1.5707963267948966192313216916398
#define ZEPS 0.0000000001
#define EPSILON 0.000001
#define FireFlysZeps 0.0000000001
#define InvFireFlysZeps 10000000000.0

namespace SmartphoneDetectionOptimization
{
	double SuperEllipseFunction(cv::Point2d &points, std::vector<double> &Variables);
	double SuperEllipseFunction(std::vector<cv::Point2d> &points, std::vector<double> &Variables);
	double SuperEllipseFit_PatternSearch(std::vector<cv::Point2d> &points, std::vector<double> &Variables, std::vector<double> &StepSize, int MaxIterationNum = 100);
	double SuperEllipseFit_PatternSearch(std::vector<cv::Point2f> &points, std::vector<double> &Variables, std::vector<double> &StepSize, int MaxIterationNum = 100);
	double SuperEllipseFit_Fireflys(std::vector<cv::Point2d> &points, std::vector<double> &Variables, std::vector<double> &MinVar, std::vector<double> &MaxVar, int FireFlysNum = 50, int MaxIterationNum = 100);
	double SuperEllipseFit_RANSAC(std::vector<cv::Point> &InputPoints, std::vector<cv::Point> &GoodPoints, std::vector<double> &Variables, std::vector<double> &StepSize);
	double Norm2(std::vector<double> Vect);

	void  EllipseFit(std::vector<cv::Point2f> &InputPoints, std::vector<double> &Variables);
	void  EllipseFit_RANSAC(std::vector<cv::Point2f> &InputPoints, std::vector<double> &Variables);


}