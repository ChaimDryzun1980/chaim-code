#include "ImageProcessingFunctions.h"

void RealityPlus::morphOps(cv::Mat &thresh, unsigned int size, unsigned int type, unsigned int Itter)
{

	cv::Mat Element;
	unsigned int RealSize = (std::floor(size / 2) * 2) + 1;
	switch (type)
	{
	case 0:
	case 1:
		Element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(RealSize, RealSize));
		break;
	case 2:
		Element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(RealSize, RealSize));
		break;
	case 3:
		Element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(RealSize, RealSize));
		break;
	default:
		Element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(RealSize, RealSize));
		break;
	}

	cv::erode(thresh, thresh, Element, cv::Point(-1, -1), Itter);
	cv::erode(thresh, thresh, Element);

	cv::dilate(thresh, thresh, Element);
	cv::dilate(thresh, thresh, Element);
}

void RealityPlus::morphClosing(cv::Mat &thresh, unsigned int size, unsigned int type, unsigned int Itter)
{
	cv::Mat Element;
	unsigned int RealSize = (std::floor(size / 2) * 2) + 1;
	switch (type)
	{
		case 0:
		case 1:
			Element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(RealSize, RealSize));
			break;
		case 2:
			Element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(RealSize, RealSize));
			break;
		case 3:
			Element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(RealSize, RealSize));
			break;
	default:
		Element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(RealSize, RealSize));
		break;
	}	
	cv::morphologyEx(thresh, thresh, cv::MORPH_CLOSE, Element, cv::Point(-1, -1), Itter);
}

void RealityPlus::morphOpening(cv::Mat &thresh, unsigned int size, unsigned int type, unsigned int Itter)
{
	cv::Mat Element;
	unsigned int RealSize = (std::floor(size / 2) * 2) + 1;
	switch (type)
	{
	case 0:
	case 1:
		Element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(RealSize, RealSize));
		break;
	case 2:
		Element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(RealSize, RealSize));
		break;
	case 3:
		Element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(RealSize, RealSize));
		break;
	default:
		Element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(RealSize, RealSize));
		break;
	}
	cv::morphologyEx(thresh, thresh, cv::MORPH_OPEN, Element, cv::Point(-1, -1), Itter);
}

void RealityPlus::morphClean(cv::Mat &thresh, unsigned int size, unsigned int type, unsigned int Itter)
{
	cv::Mat Element;
	unsigned int RealSize = (std::floor(size / 2) * 2) + 1;
	switch (type)
	{
	case 0:
	case 1:
		Element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(RealSize, RealSize));
		break;
	case 2:
		Element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(RealSize, RealSize));
		break;
	case 3:
		Element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(RealSize, RealSize));
		break;
	default:
		Element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(RealSize, RealSize));
		break;
	}
	cv::morphologyEx(thresh, thresh, cv::MORPH_CLOSE, Element, cv::Point(-1, -1), Itter);
	cv::morphologyEx(thresh, thresh, cv::MORPH_OPEN, Element, cv::Point(-1, -1), Itter);
}

void RealityPlus::morphSkel(cv::Mat &img, unsigned int size, unsigned int type, unsigned int Itter)
{
	cv::Mat skel(img.size(), CV_8UC1, cv::Scalar(0));
	cv::Mat temp;
	cv::Mat eroded;

	 cv::Mat Element;
	unsigned int RealSize = (std::floor(size / 2) * 2) + 1;
	switch (type)
	{
	case 0:
	case 1:
		Element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(RealSize, RealSize));
		break;
	case 2:
		Element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(RealSize, RealSize));
		break;
	case 3:
		Element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(RealSize, RealSize));
		break;
	default:
		Element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(RealSize, RealSize));
		break;
	}

	bool done;
	do
	{
		cv::erode(img, eroded, Element, cv::Point(-1, -1), Itter);
		cv::dilate(eroded, temp, Element, cv::Point(-1, -1), Itter); // temp = open(img)
		cv::subtract(img, temp, temp);
		cv::bitwise_or(skel, temp, skel);
		eroded.copyTo(img);

		done = (cv::countNonZero(img) == 0);
	} while (!done);

	skel.copyTo(img);
}
void RealityPlus::imageBlur(cv::Mat &img, unsigned int size, unsigned int type)
{
	unsigned int RealSize = (std::floor(size / 2) * 2) + 1;
	switch (type)
	{
	case 0:
	case 1:
		cv::GaussianBlur(img, img, cv::Size(RealSize, RealSize), 0);
		break;
	case 2:
		cv::blur(img, img, cv::Size(RealSize, RealSize));
		break;
	case 3:
		cv::medianBlur(img, img, RealSize);
		break;
	case 4:
		cv::bilateralFilter(img, img, 5, 75, 75);
		break;
	default:
		cv::GaussianBlur(img, img, cv::Size(RealSize, RealSize), 0);
		break;
	}
}

void  RealityPlus::imageCLAHE(cv::Mat &img)
{
	cv::Mat lab_image;
	cv::cvtColor(img, lab_image, CV_BGR2Lab);

	// Extract the L channel
	std::vector<cv::Mat> lab_planes(3);
	cv::split(lab_image, lab_planes);  // now we have the L image in lab_planes[0]

	// apply the CLAHE algorithm to the L channel
	cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
	clahe->setClipLimit(4);
	cv::Mat dst;
	clahe->apply(lab_planes[0], dst);

	// Merge the the color planes back into an Lab image
	dst.copyTo(lab_planes[0]);
	cv::merge(lab_planes, lab_image);

	// convert back to RGB
	cv::cvtColor(lab_image, img, CV_Lab2BGR);
}

void  RealityPlus::GrayImageCLAHE(cv::Mat &img)
{
	cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
	clahe->setClipLimit(4);
	cv::Mat dst;
	clahe->apply(img, dst);

	dst.copyTo(img);
}

void  RealityPlus::RemoveBlack(cv::Mat &img)
{
	int cols = img.cols;
	int rows = img.rows;

	if (img.channels() >= 3)
	{
		std::vector<cv::Mat> channels;
		cv::split(img, channels);

		int Val;
		int MaxVal = 0, MinVal = 800;

		for (int i = 0; i < cols; i++)
		{
			for (int j = 0; j < rows; j++)
			{
				Val = channels[0].at<uchar>(j, i) + channels[1].at<uchar>(j, i) + channels[2].at<uchar>(j, i);
				if (MaxVal < Val)
					MaxVal = Val;
				if (MinVal > Val)
					MinVal = Val;
			}
		}

		int ApplyVal = std::floor((float)(((MaxVal - MinVal)* 0.9) + (float)MinVal) / 3.0);

		for (int i = 0; i < cols; i++)
		{
			for (int j = 0; j < rows; j++)
			{
				Val = channels[0].at<uchar>(j, i) + channels[1].at<uchar>(j, i) + channels[2].at<uchar>(j, i);

				if (Val < 3)
				{
					channels[0].at<uchar>(j, i) = ApplyVal;
					channels[1].at<uchar>(j, i) = ApplyVal;
					channels[2].at<uchar>(j, i) = ApplyVal;
				}
			}
		}
		cv::merge(channels, img);
	}
	else {

		int Val;

		int MaxVal = 0, MinVal = 256;

		for (int i = 0; i < cols; i++)
		{
			for (int j = 0; j < rows; j++)
			{
				Val = img.at<uchar>(j, i);
				if (MaxVal < Val)
					MaxVal = Val;
				if (MinVal > Val)
					MinVal = Val;
			}
		}

		int ApplyVal = std::floor(((MaxVal - MinVal)* 0.9) + (float)MinVal);

		for (int i = 0; i < cols; i++)
		{
			for (int j = 0; j < rows; j++)
			{
				Val = img.at<uchar>(j, i);

				if (Val < 3)
				{
					img.at<uchar>(j, i) = ApplyVal;
				}
			}
		}
	}
}

void  RealityPlus::RemoveBlack(const cv::Mat &ColorImg, cv::Mat &BinaryImage)
{
	int cols = ColorImg.cols;
	int rows = ColorImg.rows;

	if (ColorImg.channels() >= 3)
	{
		std::vector<cv::Mat> channels;
		cv::split(ColorImg, channels);

		int Val;

		for (int i = 0; i < cols; i++)
		{
			for (int j = 0; j < rows; j++)
			{
				Val = channels[0].at<uchar>(j, i) + channels[1].at<uchar>(j, i) + channels[2].at<uchar>(j, i);
				if (Val < 3)
					BinaryImage.at<uchar>(j, i) = 0;
			}
		}
	}
}
void  RealityPlus::RGB_HistEqualization(cv::Mat &img)
{
	if (img.channels() >= 3)
	{
		std::vector<cv::Mat> channels;
		cv::split(img, channels);

		int cols = img.cols;
		int rows = img.rows;

		std::vector<int> Histogram(256, 0);

		for (int i = 0; i < cols; i++)
		{
			for (int j = 0; j < rows; j++)
			{
				Histogram[channels[0].at<uchar>(j, i)]++;
				Histogram[channels[1].at<uchar>(j, i)]++;
				Histogram[channels[2].at<uchar>(j, i)]++;
			}
		}

		int count = 0;
		std::vector<float> CDF(256, 0);
		std::vector<int> Equlized(256, 0);
		int PixelNum = cols * rows;
		for (int i = 0; i < 256; i++)
		{
			Histogram[i] = std::floor(Histogram[i] / 3.0);
			count += Histogram[i];
			CDF[i] = ((float)count / (float)PixelNum);
			Equlized[i] = std::round(CDF[i] * 255.0);
		}

		for (int i = 0; i < cols; i++)
		{
			for (int j = 0; j < rows; j++)
			{
				channels[0].at<uchar>(j, i) = Equlized[channels[0].at<uchar>(j, i)];
				channels[1].at<uchar>(j, i) = Equlized[channels[1].at<uchar>(j, i)];
				channels[2].at<uchar>(j, i) = Equlized[channels[2].at<uchar>(j, i)];
			}
		}

		cv::merge(channels, img);
	}
}

void RealityPlus::equalizeIntensity(cv::Mat& inputImage)
{
	if (inputImage.channels() >= 3)
	{
		cv::Mat ycrcb;
		cv::cvtColor(inputImage, ycrcb, CV_BGR2YCrCb);

		std::vector<cv::Mat> channels;
		cv::split(ycrcb, channels);

		cv::equalizeHist(channels[0], channels[0]);

		cv::merge(channels, ycrcb);
		cv::cvtColor(ycrcb, inputImage, CV_YCrCb2BGR);
	}
}

void RealityPlus::equalizeIntensity_HSV(cv::Mat& inputImage)
{
	if (inputImage.channels() >= 3)
	{
		std::vector<cv::Mat> channels;
		cv::split(inputImage, channels);

		cv::equalizeHist(channels[0], channels[0]);

		cv::merge(channels, inputImage);
	}
}

void RealityPlus::Sharpen(const cv::Mat& myImage, cv::Mat& Result)
{
	CV_Assert(myImage.depth() == CV_8U);  // accept only uchar images
	const int nChannels = myImage.channels();
	Result.create(myImage.size(), myImage.type());
	for (int j = 1; j < myImage.rows - 1; ++j)
	{
		const uchar* previous = myImage.ptr<uchar>(j - 1);
		const uchar* current = myImage.ptr<uchar>(j);
		const uchar* next = myImage.ptr<uchar>(j + 1);
		uchar* output = Result.ptr<uchar>(j);
		for (int i = nChannels; i < nChannels*(myImage.cols - 1); ++i)
		{
			*output++ = cv::saturate_cast<uchar>(5 * current[i]
				- current[i - nChannels] - current[i + nChannels] - previous[i] - next[i]);
		}
	}
	Result.row(0).setTo(cv::Scalar(0));
	Result.row(Result.rows - 1).setTo(cv::Scalar(0));
	Result.col(0).setTo(cv::Scalar(0));
	Result.col(Result.cols - 1).setTo(cv::Scalar(0));
}

void RealityPlus::WhiteBalance(cv::Mat& inputImage)
{
	double discard_ratio = 0.05;
	int hists[3][256];
	memset(hists, 0, 3 * 256 * sizeof(int));

	for (int y = 0; y < inputImage.rows; ++y) {
		uchar* ptr = inputImage.ptr<uchar>(y);
		for (int x = 0; x < inputImage.cols; ++x) {
			for (int j = 0; j < 3; ++j) {
				hists[j][ptr[x * 3 + j]] += 1;
			}
		}
	}

	// cumulative hist
	int total = inputImage.cols*inputImage.rows;
	int vmin[3], vmax[3];
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 255; ++j) {
			hists[i][j + 1] += hists[i][j];
		}
		vmin[i] = 0;
		vmax[i] = 255;
		while (hists[i][vmin[i]] < discard_ratio * total)
			vmin[i] += 1;
		while (hists[i][vmax[i]] > (1 - discard_ratio) * total)
			vmax[i] -= 1;
		if (vmax[i] < 255 - 1)
			vmax[i] += 1;
	}


	for (int y = 0; y < inputImage.rows; ++y) {
		uchar* ptr = inputImage.ptr<uchar>(y);
		for (int x = 0; x < inputImage.cols; ++x) {
			for (int j = 0; j < 3; ++j) {
				int val = ptr[x * 3 + j];
				if (val < vmin[j])
					val = vmin[j];
				if (val > vmax[j])
					val = vmax[j];
				ptr[x * 3 + j] = static_cast<uchar>((val - vmin[j]) * 255.0 / (vmax[j] - vmin[j]));
			}
		}
	}
}