#include "DLT.h"
#include <Eigen/QR>

//#include "PatternSearch.h"

template <typename T> int Sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

//static double CameraMatrixOptimization(std::vector<double> X, void* ObjPtr)
//{
//	return ((RealityPlus::PnP*)ObjPtr)->ProjectionError(X);
//}


void RealityPlus::DLT::CalcDLT()
{
	int PointNum = (int)m_vPoints2D.rows();
	if (PointNum != m_vPoints3D.rows())
		return;

	Eigen::MatrixXd C, B, Bt, BtB_inv, Ct;
	C.resize(PointNum*2, 3);
	B.resize(PointNum*2, 9);

	for (int i = 0; i < PointNum; i++)
	{
		C((2*i), 0)   = -(m_vPoints3D(i, 0) * m_vPoints2D(i, 0));				
		C((2 * i), 1) = -(m_vPoints3D(i ,1) * m_vPoints2D(i, 0));			
		C((2 * i), 2) = -(m_vPoints3D(i, 2) * m_vPoints2D(i, 0));

		C((2 * i) + 1, 0) = -(m_vPoints3D(i, 0) * m_vPoints2D(i, 1));			
		C((2 * i) + 1, 1) = -(m_vPoints3D(i, 1) * m_vPoints2D(i, 1));		
		C((2 * i) + 1, 2) = -(m_vPoints3D(i, 2) * m_vPoints2D(i, 1));

		B((2 * i), 0) = m_vPoints3D(i, 0);	B((2 * i), 1) = m_vPoints3D(i, 1);	B((2 * i), 2) = m_vPoints3D(i, 2);	B((2 * i), 3) = 1.0;
		B((2 * i), 4) = 0.0;	B((2 * i), 5) = 0.0;	B((2 * i), 6) = 0.0;	B((2 * i), 7) = 0.0;	B((2 * i), 8) = -m_vPoints2D(i, 0);

		B((2 * i) + 1, 4) = m_vPoints3D(i, 0);	B((2 * i) + 1, 5) = m_vPoints3D(i, 1);	B((2 * i) + 1, 6) = m_vPoints3D(i, 2);	B((2 * i) + 1, 7) = 1.0;
		B((2 * i) + 1, 0) = 0.0;		B((2 * i) + 1, 1) = 0.0;	B((2 * i) + 1, 2) = 0.0;	B((2 * i) + 1, 3) = 0.0;	B((2 * i) + 1, 8) = -m_vPoints2D(i, 1);
	}

	Bt = B.transpose().eval();
	BtB_inv = (Bt * B).inverse().eval();

	Ct = C.transpose().eval();

	Eigen::MatrixXd D, BtC, BtCt;
	BtC = (Bt*C);
	BtCt = BtC.transpose().eval();

	D = ((Ct*C) - (BtCt*(BtB_inv*BtC)));

	Eigen::EigenSolver<Eigen::MatrixXd> es(D);

	//std::cout << "\nMatrix D:\n" << D << "\n";
	//std::cout << "\nEigenValues:\n" << es.eigenvalues() << "\n";
	//std::cout << "\nEigenVectors:\n" << es.eigenvectors() << "\n";

	double SmallestEignVal = std::fabs(es.eigenvalues()(0).real());
	int SmallestEignIndex = 0;
	for (int i = 1; i < 3; i++)
	{
		if (std::fabs(es.eigenvalues()(i).real()) < SmallestEignVal)
		{
			SmallestEignVal = std::fabs(es.eigenvalues()(i).real());
			SmallestEignIndex = i;
		}
	}	
	Eigen::VectorXd a3 = es.eigenvectors().col(SmallestEignIndex).real();
	Eigen::VectorXd a9 = -(BtB_inv *(Bt*(C*a3)));

	Eigen::MatrixXd A;
	
	Eigen::Matrix3d A1, A2, minA1;
	Eigen::Vector3d A4, T;

	double Delta = 0.0;

	A.resize(3, 4);
	A(2, 0) = a3(0);
	A(2, 1) = a3(1);
	A(2, 2) = a3(2);

	A(0, 0) = a9(0);		
	A(0, 1) = a9(1);
	A(0, 2) = a9(2);
	A(0, 3) = a9(3);
	A(1, 0) = a9(4);
	A(1, 1) = a9(5);
	A(1, 2) = a9(6);
	A(1, 3) = a9(7);
	A(2, 3) = a9(8);

	Delta = std::sqrt(std::pow(A(2, 0), 2) + std::pow(A(2, 1), 2) + std::pow(A(2, 2), 2));

	//std::cout << "\nMatrix A:\n" << A << "\n";
	//std::cout << "Delta: " << Delta << "\n";

	A1(0, 0) = A(0, 0);
	A1(0, 1) = A(0, 1);
	A1(0, 2) = A(0, 2);
	A1(1, 0) = A(1, 0);
	A1(1, 1) = A(1, 1);
	A1(1, 2) = A(1, 2);
	A1(2, 0) = A(2, 0);
	A1(2, 1) = A(2, 1);
	A1(2, 2) = A(2, 2);

	A4(0) = A(0, 3);
	A4(1) = A(1, 3);
	A4(2) = A(2, 3);

	minA1 = -1.0*A1;

	T = minA1.colPivHouseholderQr().solve(A4);	
	A2 = (1.0 / Delta)*A1;

	Eigen::Matrix3d P,revA, Qrev, Rrev, Q,R, J, S, G, M;
	P.setZero();
	P(0, 2) = 1.0;
	P(1, 1) = 1.0;
	P(2, 0) = 1.0;
		
	revA = (P * A2).transpose().eval();

	Eigen::HouseholderQR<Eigen::Matrix3d> qr(revA);
	qr.compute(revA);

	Rrev = qr.matrixQR().template  triangularView<Eigen::Upper>();
	Qrev = qr.householderQ();
	   
	Q = P * (Qrev.transpose().eval());
	R = P * ((Rrev.transpose().eval()) * P);

	J.setIdentity();
	for (int i = 0; i < 3; i++)
		J(i, i) = (double)Sgn<double>(R(i, i));

	Rrev = R * J;

	double Theta = std::atan2(-Rrev(0, 1), (Rrev(0, 0) + Rrev(1, 1)));
	double CosQ = std::cos(Theta), SinQ = std::sin(Theta);

	S.setIdentity();
	S(0, 0) = CosQ;		S(0, 1) = SinQ;
	S(1, 1) = CosQ;		S(1, 0) = -SinQ;

	G = Rrev * S;

	M = (S.inverse().eval())*(J.inverse().eval())*Q;

	double DeltaSign = M.determinant();

	M *= DeltaSign;
	Delta *= DeltaSign;
	
	double Focal, Cx, Cy, b1, b2;

	Eigen::AngleAxisd AA(M);
	//double RotationAngle = AA.angle();
	Eigen::Vector3d RotationAxis = AA.axis();

	Cx = G(0, 2);
	Cy = G(1, 2);
	b1 = -((G(0, 0) - G(1, 1)) / (G(0, 0) + G(1, 1)));
	b2 = -2.0 * (G(0, 1) / (G(0, 0) + G(1, 1)));
	Focal = 2*(((G(0, 0)*G(1, 1)) - std::pow(G(0,1),2.0)) / (G(0, 0) + G(1, 1)));

	//std::cout << "\nMatrix G:\n" << G << "\n";
	//std::cout << "\nMatrix M:\n" << M << "\n\n";
	//std::cout << "Cx:\n" << Cx << "\n";
	//std::cout << "Cy:\n" << Cy << "\n";
	//std::cout << "b1:\n" << b1 << "\n";
	//std::cout << "b2:\n" << b2 << "\n";
	//std::cout << "f:\n" << Focal << "\n";
	//std::cout << "\n\nTranslation: " << T.transpose() << "\n";
	//std::cout << "Rotation Axis: " << RotationAxis.transpose().eval() << "\n";
	//std::cout << "Rotation Angle: " << RotationAngle * 57.295779513082320876798154814105 << "\n";
	   	
	//system("pause");
}

bool RealityPlus::PnP::CalculatePnP()
{
	if ((m_dFocal < 0.0) || (m_vModelPoints.size() < 4) || (m_vModelPoints.size() != m_vImagePoints.size()))
		return false;

	// Camera internals
	cv::Mat camera_matrix = (cv::Mat_<double>(3, 3) << m_dFocal, 0, m_vProjectionCenter.x, 0, m_dFocal, m_vProjectionCenter.y, 0, 0, 1);
	cv::Mat dist_coeffs = cv::Mat::zeros(4, 1, cv::DataType<double>::type); // Assuming no lens distortion

	//std::cout << "Camera Matrix " << std::endl << camera_matrix << std::endl;

	int MethodFlag = (int)m_ePnP_Method;
	if (MethodFlag > 1)
		MethodFlag++;

	// Solve for pose
	if (m_bUseRANSAC)
	{
		cv::solvePnPRansac(m_vModelPoints, m_vImagePoints, camera_matrix, dist_coeffs, m_vRotation, m_vTranslation, m_bUseInitialGuess, m_iIterration, 10.0, 0.98, cv::noArray(), MethodFlag);
	}
	else {
		cv::solvePnP(m_vModelPoints, m_vImagePoints, camera_matrix, dist_coeffs, m_vRotation, m_vTranslation, m_bUseInitialGuess, MethodFlag);
	}

	//std::cout << "\nTranslation:\n" << m_vTranslation << "\n";
	//std::cout << "\nRotation:\n" << m_vRotation << "\n";

	return true;
}

//double RealityPlus::PnP::ProjectionError(std::vector<double> X)
//{
//	double Error = 10000000000.0;
//	if ((m_dFocal < 0.0) || (m_vModelPoints.size() < 4) || (m_vModelPoints.size() != m_vImagePoints.size()))
//		return Error;
//
//	// Camera internals
//	cv::Mat camera_matrix = (cv::Mat_<double>(3, 3) << X[0], 0, X[1], 0, X[0], X[2], 0, 0, 1);
//	cv::Mat dist_coeffs = cv::Mat::zeros(4, 1, cv::DataType<double>::type); // Assuming no lens distortion
//		
//	int MethodFlag = (int)m_ePnP_Method;
//	if (MethodFlag > 1)
//		MethodFlag++;
//
//	// Solve for pose
//	if (m_bUseRANSAC)
//	{
//		cv::solvePnPRansac(m_vModelPoints, m_vImagePoints, camera_matrix, dist_coeffs, m_vRotation, m_vTranslation, m_bUseInitialGuess, m_iIterration, 10.0, 0.98, cv::noArray(), MethodFlag);
//	}
//	else {
//		cv::solvePnP(m_vModelPoints, m_vImagePoints, camera_matrix, dist_coeffs, m_vRotation, m_vTranslation, m_bUseInitialGuess, MethodFlag);
//	}
//
//	std::vector<cv::Point2d> NewImagePoints;
//	cv::projectPoints(m_vModelPoints, m_vRotation, m_vTranslation, camera_matrix, dist_coeffs, NewImagePoints);
//
//	Error = 0.0;
//	for (int i = 0; i < NewImagePoints.size(); i++)
//	{
//		Error += std::sqrt(std::pow((m_vImagePoints[i].x - NewImagePoints[i].x), 2.0) + std::pow((m_vImagePoints[i].y - NewImagePoints[i].y), 2.0));
//	}
//	
//	if (NewImagePoints.size() > 1)
//		Error /= (double)NewImagePoints.size();
//
//	return Error;
//}
//
//bool RealityPlus::PnP::OptimizePnP()
//{
//	if ((m_dFocal < 0.0) || (m_vModelPoints.size() < 4) || (m_vModelPoints.size() != m_vImagePoints.size()))
//		return false;
//
//	std::vector<double> X, StepSize;
//	X.resize(3);
//	X[0] = m_dFocal;
//	X[1] = m_vProjectionCenter.x;
//	X[2] = m_vProjectionCenter.y;
//
//	StepSize.resize(3);
//	StepSize[0] = 1.0;
//	StepSize[1] = 1.0;
//	StepSize[2] = 1.0;
//
//	PatternSearch DHsolver(this, CameraMatrixOptimization);
//	DHsolver.SetDimension((int)X.size());
//	DHsolver.SetMaxIteration(100000);
//	DHsolver.SetOptimizationType(PatternSearch::eMin);
//	DHsolver.SetStepSize(StepSize);
//	DHsolver.SetTollerance(0.0000000001);
//	DHsolver.SetVariables(X);
//	DHsolver.PrintData(true);
//	bool scss = DHsolver.Optimize();
//
//	if (!scss) return false;
//
//	X = DHsolver.GetVariables();
//
//	// Camera internals
//	cv::Mat camera_matrix = (cv::Mat_<double>(3, 3) << X[0], 0, X[1], 0, X[0], X[2], 0, 0, 1);
//	cv::Mat dist_coeffs = cv::Mat::zeros(4, 1, cv::DataType<double>::type); // Assuming no lens distortion
//
//	std::cout << "Camera Matrix " << std::endl << camera_matrix << std::endl;
//
//	int MethodFlag = (int)m_ePnP_Method;
//	if (MethodFlag > 1)
//		MethodFlag++;
//
//	// Solve for pose
//	if (m_bUseRANSAC)
//	{
//		cv::solvePnPRansac(m_vModelPoints, m_vImagePoints, camera_matrix, dist_coeffs, m_vRotation, m_vTranslation, m_bUseInitialGuess, m_iIterration, 10.0, 0.98, cv::noArray(), MethodFlag);
//	}
//	else {
//		cv::solvePnP(m_vModelPoints, m_vImagePoints, camera_matrix, dist_coeffs, m_vRotation, m_vTranslation, m_bUseInitialGuess, MethodFlag);
//	}
//
//	std::cout << "\nTranslation:\n" << m_vRotation << "\n";
//	std::cout << "\nRotation:\n" << m_vTranslation << "\n";
//
//	return true;
//
//}
