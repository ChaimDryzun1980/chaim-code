#pragma once

#include "CommonDefenitions.h"
#include <pcl/features/normal_3d_omp.h>
#include <pcl/surface/mls.h>


namespace PointCloudNormals
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr AddNormals(CloudXYZRGB_Ptr cloud, double RadiusSearch = 1.0);
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr AddNormals(CloudXYZRGB_Ptr cloud, int Neighbours = 50);
	pcl::PointCloud<pcl::Normal>::Ptr CalcNormals(CloudXYZRGB_Ptr cloud, double RadiusSearch = 1.0);
	pcl::PointCloud<pcl::Normal>::Ptr CalcNormals(CloudXYZRGB_Ptr cloud, int Neighbours = 50);
	pcl::PointCloud<pcl::Normal>::Ptr CalcNormalsWithCorrection(CloudXYZRGB_Ptr cloud, int KNN = 4, int Neighbours = 50);
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr CalcMLSNormals(CloudXYZRGB_Ptr cloud, double Radius = 1.0);
}
