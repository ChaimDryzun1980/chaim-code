#include "PointCloudSegmentation.h"
#include "PointCloudVisualization.h"
#include "PointCloudHelperFunction.h"
#include "Conversions.h"
#include "CommonDefenitions.h"

#define WIN32_LEAN_AND_MEAN

#include <winsock2.h>
#include <Ws2tcpip.h>
#include <stdio.h>

// Link with ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")

#include <opencv2/dnn.hpp>
#include <opencv2/tracking.hpp>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/cloud_viewer.h>

#include <json/json.h>
#include <json/writer.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

// Get RGB values based on normals - texcoords, normals value [u v]
std::tuple<uint8_t, uint8_t, uint8_t> get_texcolor(rs2::video_frame texture, rs2::texture_coordinate texcoords)
{
	const int w = texture.get_width(), h = texture.get_height();

	// convert normals [u v] to basic coords [x y]
	int x = std::min(std::max(int(texcoords.u*w + .5f), 0), w - 1);
	int y = std::min(std::max(int(texcoords.v*h + .5f), 0), h - 1);

	int idx = x * texture.get_bytes_per_pixel() + y * texture.get_stride_in_bytes();
	const auto texture_data = reinterpret_cast<const uint8_t*>(texture.get_data());
	return std::tuple<uint8_t, uint8_t, uint8_t>(texture_data[idx], texture_data[idx + 1], texture_data[idx + 2]);
}


CloudXYZRGB_Ptr points_to_pcl(const rs2::points& points, const rs2::video_frame& color) {

	//// OpenCV Mat for showing the rgb color image, just as part of processing
	//Mat colorr(Size(640, 480), CV_8UC3, (void*)color.get_data(), Mat::AUTO_STEP);
	//namedWindow("Display Image", WINDOW_AUTOSIZE);
	//imshow("Display Image", colorr);

	auto sp = points.get_profile().as<rs2::video_stream_profile>();
	CloudXYZRGB_Ptr cloud(new CloudXYZRGB);

	// Config of PCL Cloud object
	cloud->width = static_cast<uint32_t>(sp.width());
	cloud->height = static_cast<uint32_t>(sp.height());
	cloud->is_dense = false;
	cloud->points.resize(points.size());

	auto tex_coords = points.get_texture_coordinates();
	auto vertices = points.get_vertices();

	// Iterating through all points and setting XYZ coordinates
	// and RGB values
	for (int i = 0; i < points.size(); ++i)
	{
		cloud->points[i].x = vertices[i].x;
		cloud->points[i].y = -vertices[i].y;
		cloud->points[i].z = vertices[i].z;

		std::tuple<uint8_t, uint8_t, uint8_t> current_color;
		current_color = get_texcolor(color, tex_coords[i]);

		// Reversed order- 2-1-0 because of BGR model used in camera
		cloud->points[i].r = std::get<2>(current_color);
		cloud->points[i].g = std::get<1>(current_color);
		cloud->points[i].b = std::get<0>(current_color);
	}

	return cloud;
}

float confThreshold, nmsThreshold;
std::vector<cv::Rect> postprocess(cv::Mat& frame, const std::vector<cv::Mat>& out, cv::dnn::Net& net);

std::vector<cv::String> getOutputsNames(const cv::dnn::Net& net);

pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);


int main(int argc, char* argv[])
{
	std::string InputJsonFile = "InputJSON.json";

	if (argc > 1)
		InputJsonFile = argv[1];

	Json::Value jsonData;
	std::ifstream test(InputJsonFile, std::ifstream::binary);
	Json::CharReaderBuilder jsonReader;
	JSONCPP_STRING errs;

	if (!Json::parseFromStream(jsonReader, test, &jsonData, &errs))
	{
		std::cout << "Could not parse input JSON" << std::endl;
		std::cout << errs << std::endl;
		system("pause");
		return 1;
	}

	int m_iPort = (unsigned short)jsonData.get("PORT", 2000).asInt();
	int m_iTrackMethod = (unsigned short)jsonData.get("TRACKING_METHOD", 6).asInt();
	int AvrageNum = jsonData.get("AVERAGE", 5).asInt();
	int inpWidth = jsonData.get("DETECTION_WIDTH", 416).asInt();
	int inpHeight = jsonData.get("DETECTION_HIGHT", 416).asInt(); 
	int Warmup = jsonData.get("INITIAL_WARMUP", 10).asInt(); 
	int m_iMinPointsPerPlane = jsonData.get("MIN_POINTS_PER_PLANE", 12).asInt();
	double _max_distance = jsonData.get("RANSAC_DISTANCE", 0.2).asDouble();
	double Z_MAX = jsonData.get("Z_MAX", 85.0).asDouble();
	double Z_MIN = jsonData.get("Z_MIN", 10.0).asDouble();
	confThreshold = jsonData.get("DETECTION_CONFIDANCE_LEVEL", 0.5).asDouble();
	nmsThreshold = jsonData.get("DETECTION_NMS_THRESHOLD", 0.4).asDouble(); // non-maximum suppression threshold
	float scale = jsonData.get("DETECTION_SCALE", 0.00392).asDouble();
	bool m_bVerbose = jsonData.get("VERBOSE", true).asBool();
	bool m_bDisplay = jsonData.get("SHOW_IMAGE", false).asBool();
	std::string m_sLog = jsonData.get("LOG_FILE", "Out.json").asString();
	std::string m_sAddress = jsonData.get("ADDRESS", "192.168.1.136").asString();
	std::string classesfile = jsonData.get("DETECTION_CLASSES", "D:\\Code\\MyCode\\CppCode\\YOLO3\\coco.names").asString();
	std::string modelconfiguration = jsonData.get("DETECTION_CONFIG", "D:\\Code\\MyCode\\CppCode\\YOLO3\\yolov3.cfg").asString();	
	std::string modelweights = jsonData.get("DETECTION_WEIGHTS", "D:\\Code\\MyCode\\CppCode\\YOLO3\\yolov3.weights").asString();

	if (m_iMinPointsPerPlane < 4)
		m_iMinPointsPerPlane = 4;

	_max_distance = std::fabs(_max_distance / 100.0);
	if (_max_distance < ZEPS)
		_max_distance = 0.001;

	Z_MIN = std::fabs(Z_MIN / 100.0);
	if (Z_MIN < ZEPS)
		Z_MIN = 0.001;
	if (Z_MIN > 4.0)
		Z_MIN = 4.0;

	Z_MAX = std::fabs(Z_MAX / 100.0);
	if (Z_MAX < ZEPS)
		Z_MAX = 0.001;
	if (Z_MAX > 4.0)
		Z_MAX = 4.0;

	if (Z_MAX < Z_MIN)
	{
		double TmpZ = Z_MAX;
		Z_MAX = Z_MIN;
		Z_MIN = TmpZ;
	}

	if (std::fabs(Z_MAX - Z_MIN) < 0.001)
	{
		Z_MIN = 0.05;
		Z_MAX = 0.85;
	}

	Warmup = abs(Warmup);
	if (Warmup > 100)
		Warmup = 100;
	if (Warmup < 1)
		Warmup = 1;

	std::vector<MyEulerAngles> LastAngles;
	MyEulerAngles GivenAngles;

	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane(new pcl::PointCloud<pcl::PointXYZRGB>);
	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	pcl::SACSegmentation<pcl::PointXYZRGB> seg;
	pcl::ExtractIndices<pcl::PointXYZRGB> extract;
	seg.setOptimizeCoefficients(true);
	seg.setModelType(pcl::SACMODEL_PLANE);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setDistanceThreshold(_max_distance);
		
	cv::Scalar mean(0.0, 0.0, 0.0, 0.0);
	bool swapRB = true;
	std::vector<std::string> classes;

	ifstream ifs(classesfile.c_str());
	std::string line;
	while (getline(ifs, line)) classes.push_back(line);

	cv::dnn::Net net = cv::dnn::readNetFromDarknet(modelconfiguration, modelweights);
	net.setPreferableBackend(cv::dnn::DNN_BACKEND_DEFAULT);
	net.setPreferableTarget(cv::dnn::DNN_TARGET_OPENCL_FP16);

	// List of tracker types in OpenCV
	// NOTE : GOTURN implementation is buggy and does not work.
	std::string trackerTypes[7] = { "BOOSTING", "MIL", "KCF", "TLD","MEDIANFLOW", "GOTURN", "CSRT" };
	// vector <string> trackerTypes(types, std::end(types));

	// Create a tracker
	std::string trackerType = trackerTypes[m_iTrackMethod];

	cv::Ptr<cv::Tracker> tracker;
	{
		if (trackerType == "BOOSTING")
			tracker = cv::TrackerBoosting::create();
		else if (trackerType == "MIL")
			tracker = cv::TrackerMIL::create();
		else if (trackerType == "KCF")
			tracker = cv::TrackerKCF::create();
		else if (trackerType == "TLD")
			tracker = cv::TrackerTLD::create();
		else if (trackerType == "MEDIANFLOW")
			tracker = cv::TrackerMedianFlow::create();
		else if (trackerType == "GOTURN")
			tracker = cv::TrackerGOTURN::create();
		else if (trackerType == "CSRT")
			tracker = cv::TrackerCSRT::create();
	}
	cv::Rect2d BoundingBox;

	pcl::PassThrough<pcl::PointXYZRGB> passZ;
	passZ.setFilterFieldName("z");
	passZ.setFilterLimits(Z_MIN, Z_MAX);

	pcl::PassThrough<pcl::PointXYZRGB> passX;
	passX.setFilterFieldName("x");

	pcl::PassThrough<pcl::PointXYZRGB> passY;
	passY.setFilterFieldName("y");

	//// Create the filtering object
	//pcl::VoxelGrid<pcl::PointXYZRGB> sor;
	//sor.setLeafSize(0.002f, 0.002f, 0.002f);
	
	std::vector<pcl::PointIndices> cluster_indices;

	std::vector<cv::Rect> boxes;
	cv::Mat blob;
	cv::Rect FinalBox;

	bool FoundBox = false;

	Json::Value JsonValues;

	int iResult;
	WSADATA wsaData;

	SOCKET SendSocket = INVALID_SOCKET;
	sockaddr_in RecvAddr;

	char SendBuf[1024];
	int BufLen = 1024;

	std::ofstream file_id;
	file_id.open(m_sLog, std::ofstream::out | std::ofstream::trunc);

	//----------------------
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		wprintf(L"WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	//---------------------------------------------
	// Create a socket for sending data
	SendSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (SendSocket == INVALID_SOCKET) {
		wprintf(L"socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Set up the RecvAddr structure with the IP address of
	// the receiver (in this example case "192.168.1.1")
	// and the specified port number.
	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_port = htons(m_iPort);
	RecvAddr.sin_addr.s_addr = inet_addr(m_sAddress.c_str());

	Eigen::Vector3d Zvect(0.0, 0.0, 1.0);

	clock_t tm;
	
	if (m_bDisplay)
	{
		cv::namedWindow("Color", cv::WINDOW_NORMAL);
		cv::namedWindow("Depth", cv::WINDOW_NORMAL);
	}

	// Declare pointcloud object, for calculating pointclouds and texture mappings
	rs2::pointcloud pc;

	rs2::align align_to_color(RS2_STREAM_COLOR);
	rs2::align align_to_depth(RS2_STREAM_DEPTH);

	rs2::decimation_filter dec;
	// If the demo is too slow, make sure you run in Release (-DCMAKE_BUILD_TYPE=Release)
	// but you can also increase the following parameter to decimate depth more (reducing quality)
	dec.set_option(RS2_OPTION_FILTER_MAGNITUDE, 2);
	// Define transformations from and to Disparity domain
	rs2::disparity_transform depth2disparity;
	rs2::disparity_transform disparity2depth(false);
	// Define spatial filter (edge-preserving)
	rs2::spatial_filter spat;
	// Enable hole-filling
	// Hole filling is an agressive heuristic and it gets the depth wrong many times
	// However, this demo is not built to handle holes
	// (the shortest-path will always prefer to "cut" through the holes since they have zero 3D distance)
	spat.set_option(RS2_OPTION_HOLES_FILL, 5); // 5 = fill all the zero pixels
	// Define temporal filter
	rs2::temporal_filter temp;
	
	// Declare RealSense pipeline, encapsulating the actual device and sensors
	rs2::pipeline pipe;

	rs2::config cfg;
	cfg.enable_stream(RS2_STREAM_INFRARED, 640, 480, RS2_FORMAT_Y8, 30);
	cfg.enable_stream(RS2_STREAM_DEPTH, 640, 480, RS2_FORMAT_Z16, 30);
	cfg.enable_stream(RS2_STREAM_COLOR, 640, 480, RS2_FORMAT_BGR8, 30);
	  
	auto profile = pipe.start(cfg);

	auto sensor = profile.get_device().first<rs2::depth_sensor>();

	// Set the device to High Accuracy preset of the D400 stereoscopic cameras
	if (sensor && sensor.is<rs2::depth_stereo_sensor>())
	{
		//sensor.set_option(RS2_OPTION_VISUAL_PRESET, RS2_RS400_VISUAL_PRESET_HIGH_ACCURACY);
		sensor.set_option(RS2_OPTION_VISUAL_PRESET, RS2_RS400_VISUAL_PRESET_HIGH_DENSITY);
	}

	auto stream = profile.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();
	auto intr = stream.get_intrinsics();

	// Camera warmup - dropping several first frames to let auto-exposure stabilize
	for (int i = 0; i < Warmup; i++)
	{
		// Wait for all configured streams to produce a frame
		auto frames = pipe.wait_for_frames();
	}
	
	for (;;)
	{
		tm = clock();

		rs2::frameset data = pipe.wait_for_frames();
		//rs2::frameset aligned_set = align_to_depth .process(data);
		rs2::frameset aligned_set = align_to_color.process(data);

		// Decimation will reduce the resultion of the depth image,
			// closing small holes and speeding-up the algorithm
		//aligned_set = aligned_set.apply_filter(dec);

		// To make sure far-away objects are filtered proportionally
		// we try to switch to disparity domain
		aligned_set = aligned_set.apply_filter(depth2disparity);

		// Apply spatial filtering
		aligned_set = aligned_set.apply_filter(spat);

		// Apply temporal filtering
		aligned_set = aligned_set.apply_filter(temp);

		// If we are in disparity domain, switch back to depth
		aligned_set = aligned_set.apply_filter(disparity2depth);
		
		auto depth = aligned_set.get_depth_frame();
		auto colored_frame = aligned_set.get_color_frame();


		// If we only received new depth frame, 
		// but the color did not update, continue
		static int last_frame_number = 0;
		if (colored_frame.get_frame_number() == last_frame_number) continue;
		last_frame_number = colored_frame.get_frame_number();

		// Creating OpenCV Matrix from a color image
		auto color_image = Conversions::frame_to_mat(colored_frame);
		auto depth_mat = Conversions::depth_frame_to_meters(pipe, depth);

		if (!FoundBox)
		{
			tracker.release();

			// Create a 4D blob from a frame.
			cv::Size inpSize(inpWidth > 0 ? inpWidth : color_image.cols, inpHeight > 0 ? inpHeight : color_image.rows);
			cv::dnn::blobFromImage(color_image, blob, scale, inpSize, mean, swapRB, false);

			// Run a model.
			net.setInput(blob);
			if (net.getLayer(0)->outputNameToIndex("im_info") != -1)  // Faster-RCNN or R-FCN
			{
				resize(color_image, color_image, inpSize);
				cv::Mat imInfo = (cv::Mat_<float>(1, 3) << inpSize.height, inpSize.width, 1.6f);
				net.setInput(imInfo, "im_info");
			}
			std::vector<cv::Mat> outs;
			net.forward(outs, getOutputsNames(net));

			boxes.clear();
			boxes = postprocess(color_image, outs, net);

			if (boxes.size() == 1)
			{
				FinalBox = boxes[0];
				FoundBox = true;
				BoundingBox = cv::Rect2d(FinalBox.x - 1, FinalBox.y - 3, FinalBox.width + 1, FinalBox.height+3);

				if (trackerType == "BOOSTING")
					tracker = cv::TrackerBoosting::create();
				else if (trackerType == "MIL")
					tracker = cv::TrackerMIL::create();
				else if (trackerType == "KCF")
					tracker = cv::TrackerKCF::create();
				else if (trackerType == "TLD")
					tracker = cv::TrackerTLD::create();
				else if (trackerType == "MEDIANFLOW")
					tracker = cv::TrackerMedianFlow::create();
				else if (trackerType == "GOTURN")
					tracker = cv::TrackerGOTURN::create();
				else if (trackerType == "CSRT")
					tracker = cv::TrackerCSRT::create();

				if (!tracker->init(color_image, BoundingBox))
					FoundBox = false;
			}
			else if (boxes.size() > 1)
			{
				int BestIndx = -1;
				double MaxSize = -1.0, TmpSize;
				for (int indx = 0; indx < boxes.size(); indx++)
				{
					TmpSize = fabs((((double)boxes[indx].width) * ((double)boxes[indx].height)));
					if (MaxSize < TmpSize)
					{
						MaxSize = TmpSize;
						BestIndx = indx;
					}
				}
				if (BestIndx >= 0)
				{
					FinalBox = boxes[BestIndx];
					FoundBox = true;
					BoundingBox = cv::Rect2d(FinalBox.x - 1, FinalBox.y - 3, FinalBox.width + 1, FinalBox.height +3);

					if (trackerType == "BOOSTING")
						tracker = cv::TrackerBoosting::create();
					else if (trackerType == "MIL")
						tracker = cv::TrackerMIL::create();
					else if (trackerType == "KCF")
						tracker = cv::TrackerKCF::create();
					else if (trackerType == "TLD")
						tracker = cv::TrackerTLD::create();
					else if (trackerType == "MEDIANFLOW")
						tracker = cv::TrackerMedianFlow::create();
					else if (trackerType == "GOTURN")
						tracker = cv::TrackerGOTURN::create();
					else if (trackerType == "CSRT")
						tracker = cv::TrackerCSRT::create();
					
					if (!tracker->init(color_image, BoundingBox))
						FoundBox = false;
				}
				else
				{
					FoundBox = false;
					tracker.release();
				}
			}
			else
			{
				FoundBox = false;
				tracker.release();
			}			
		}
		else
		{
			if (tracker->empty())
			{
				if (trackerType == "BOOSTING")
					tracker = cv::TrackerBoosting::create();
				else if (trackerType == "MIL")
					tracker = cv::TrackerMIL::create();
				else if (trackerType == "KCF")
					tracker = cv::TrackerKCF::create();
				else if (trackerType == "TLD")
					tracker = cv::TrackerTLD::create();
				else if (trackerType == "MEDIANFLOW")
					tracker = cv::TrackerMedianFlow::create();
				else if (trackerType == "GOTURN")
					tracker = cv::TrackerGOTURN::create();
				else if (trackerType == "CSRT")
					tracker = cv::TrackerCSRT::create();
			}

			// Update the tracking result
			bool ok = tracker->update(color_image, BoundingBox);

			if (!ok)
			{
				FoundBox = false;
				tracker.release();
			}
			else
			{
				// Order here is crucial! 
		// map_to() color frame has to be done befor point calculation
		// otherwise texture won't be mapped
				pc.map_to(colored_frame);
				auto points = pc.calculate(depth);

				// Actual calling of conversion and saving XYZRGB cloud to file
				cloud = points_to_pcl(points, colored_frame);

				if (cloud->size() < m_iMinPointsPerPlane)
				{
					FoundBox = false;
					tracker.release();
				}
				else {

					passZ.setInputCloud(cloud);
					passZ.filter(*cloud);

					cv::rectangle(color_image, cv::Point(BoundingBox.x, BoundingBox.y), cv::Point(BoundingBox.x + BoundingBox.width, BoundingBox.y + BoundingBox.height), cv::Scalar(0, 255, 0));

					// Deproject from pixel to point in 3D
					float upixel[2]; // From pixel
					float upoint[3]; // From point (in 3D)

					float vpixel[2]; // To pixel
					float vpoint[3]; // To point (in 3D)

					// Copy pixels into the arrays (to match rsutil signatures)
					upixel[0] = BoundingBox.x;
					upixel[1] = BoundingBox.y;
					vpixel[0] = BoundingBox.x + BoundingBox.width;
					vpixel[1] = BoundingBox.y + BoundingBox.height;

					// Query the frame for distance
					// Note: this can be optimized
					// It is not recommended to issue an API call for each pixel
					// (since the compiler can't inline these)
					// However, in this example it is not one of the bottlenecks

					std::cout << intr.fx << " " << intr.fy << " " << intr.ppx << " " << intr.ppy << " ";

					auto udist = depth.get_distance(upixel[0], upixel[1]);
					auto vdist = depth.get_distance(vpixel[0], vpixel[1]);

					if ((std::fabs(udist) < ZEPS) || (std::fabs(vdist) < ZEPS))
					{
						FoundBox = false;
						tracker.release();
					}
					else
					{

						// Deproject from pixel to point in 3D
						rs2_deproject_pixel_to_point(upoint, &intr, upixel, udist);
						rs2_deproject_pixel_to_point(vpoint, &intr, vpixel, vdist);

						if (m_bVerbose)
						{
							std::cout << "\n\t" << BoundingBox.x << ", " << BoundingBox.y << " --> " << upoint[0] << ", " << upoint[1] << ", " << upoint[2] << "\n";
							std::cout << "\n\t" << BoundingBox.x + BoundingBox.width << ", " << BoundingBox.y + BoundingBox.height << " --> " << vpoint[0] << ", " << vpoint[1] << ", " << vpoint[2] << "\n";
						}

						passX.setInputCloud(cloud);
						passX.setFilterLimits(upoint[0], vpoint[0]);
						passX.filter(*cloud);

						passY.setInputCloud(cloud);
						passY.setFilterLimits(upoint[1], vpoint[1]);
						passY.filter(*cloud);

						if (cloud->size() < m_iMinPointsPerPlane)
						{
							FoundBox = false;
							tracker.release();
						}
						else
						{

							// Fit a plane
							seg.setInputCloud(cloud);
							seg.segment(*inliers, *coefficients);

							// Check result
							if (inliers->indices.size() < m_iMinPointsPerPlane)
							{
								FoundBox = false;
								tracker.release();
							}
							else
							{
								// Extract inliers
								extract.setInputCloud(cloud);
								extract.setIndices(inliers);
								extract.setNegative(false);
								extract.filter(*cloud_plane);										

								Eigen::Matrix3d Q = PointCloudHelperFunctions::planePCA(cloud_plane);
								pcl::PointXYZ PntXYZ = PointCloudHelperFunctions::CalcPoistion(cloud_plane, Q);

								double X = PntXYZ.x;
								double Y = PntXYZ.y;
								double Z = PntXYZ.z;

								Eigen::Vector3d ea = Q.eulerAngles(2, 1, 0);
								GivenAngles.m_dYaw = (double)ea(0);
								GivenAngles.m_dPitch = (double)ea(1);
								GivenAngles.m_dRoll = (double)ea(2);

								LastAngles.push_back(GivenAngles);
								GivenAngles.m_dYaw = 0.0;
								GivenAngles.m_dPitch = 0.0;
								GivenAngles.m_dRoll = 0.0;

								int LastAnglesSize = (int)LastAngles.size();
								for (int i = 0; i < LastAnglesSize; i++)
								{
									GivenAngles.m_dYaw += LastAngles[i].m_dYaw / (double)LastAnglesSize;
									GivenAngles.m_dPitch += LastAngles[i].m_dPitch / (double)LastAnglesSize;
									GivenAngles.m_dRoll += LastAngles[i].m_dRoll / (double)LastAnglesSize;
								}

								if (LastAnglesSize > AvrageNum)
								{
									LastAngles.erase(LastAngles.begin());
								}

								if (m_bVerbose)
								{
									std::cout << "Euler angles:\n" << (double)GivenAngles.m_dYaw << " " << (double)GivenAngles.m_dPitch << " " << (double)GivenAngles.m_dRoll << "\n";
									std::cout << "\nTranslation:\n\t" << X << ", " << Y << ", " << Z << "\n\n";
								}

								JsonValues["SENDER"] = "PHONEDETECTOR";
								JsonValues["X"] = (double)X;
								JsonValues["Y"] = (double)Y;
								JsonValues["Z"] = (double)Z;
								JsonValues["YAW"] = (double)GivenAngles.m_dYaw;
								JsonValues["PITCH"] = (double)GivenAngles.m_dPitch;
								JsonValues["ROLL"] = (double)GivenAngles.m_dRoll;

								Json::StreamWriterBuilder wbuilder;
								wbuilder.settings_["commentStyle"] = "None";
								wbuilder.settings_["indentation"] = "";
								std::string document = Json::writeString(wbuilder, JsonValues);

								file_id << document << "\n";

								//---------------------------------------------
							// Send a datagram to the receiver
								iResult = sendto(SendSocket,
									document.c_str(), document.length(), 0, (SOCKADDR *)& RecvAddr, sizeof(RecvAddr));
								if (iResult == SOCKET_ERROR) {
									wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
									closesocket(SendSocket);
									WSACleanup();
									return 1;
								}
							}
						}
					}
				}
			}
		}			
		

		tm = clock() - tm;
		
		if (m_bVerbose)
		{
			printf("It took me %d clicks (%f seconds).\t", tm, ((float)tm) / CLOCKS_PER_SEC);
			if (FoundBox)
				printf("Phone Found!!\n");
			else
				printf("No phone\n");
		}

		if (m_bDisplay)
		{
			cv::imshow("Color", color_image);
			cv::imshow("Depth", depth_mat);
			char c = (char)cv::waitKey(1);
		}

		if (GetKeyState(VK_CANCEL) & 0x8000)
			break;
	}

	if (m_bDisplay)
	{
		cv::destroyAllWindows();
	}

	file_id.close();

	//---------------------------------------------
	// When the application is finished sending, close the socket.
	iResult = closesocket(SendSocket);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"closesocket failed with error: %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Clean up and quit.
	WSACleanup();

	return EXIT_SUCCESS;
}


std::vector<cv::Rect> postprocess(cv::Mat& frame, const std::vector<cv::Mat>& outs, cv::dnn::Net& net)
{
	static std::vector<int> outLayers = net.getUnconnectedOutLayers();
	static std::string outLayerType = net.getLayer(outLayers[0])->type;

	std::vector<int> classIds;
	std::vector<float> confidences;
	std::vector<cv::Rect> boxes;
	if (net.getLayer(0)->outputNameToIndex("im_info") != -1)  // Faster-RCNN or R-FCN
	{
		// Network produces output blob with a shape 1x1xNx7 where N is a number of
		// detections and an every detection is a vector of values
		// [batchId, classId, confidence, left, top, right, bottom]
		CV_Assert(outs.size() == 1);
		float* data = (float*)outs[0].data;
		for (size_t i = 0; i < outs[0].total(); i += 7)
		{
			int TmpID = (data[i + 1]) - 1;
			if (TmpID != 67)
				continue;

			float confidence = data[i + 2];
			if (confidence > confThreshold)
			{
				int left = (int)data[i + 3];
				int top = (int)data[i + 4];
				int right = (int)data[i + 5];
				int bottom = (int)data[i + 6];
				int width = right - left + 1;
				int height = bottom - top + 1;
				classIds.push_back((int)(data[i + 1]) - 1);  // Skip 0th background class id.
				boxes.push_back(cv::Rect(left, top, width, height));
				confidences.push_back(confidence);
			}
		}
	}
	else if (outLayerType == "DetectionOutput")
	{
		// Network produces output blob with a shape 1x1xNx7 where N is a number of
		// detections and an every detection is a vector of values
		// [batchId, classId, confidence, left, top, right, bottom]
		CV_Assert(outs.size() == 1);
		float* data = (float*)outs[0].data;
		for (size_t i = 0; i < outs[0].total(); i += 7)
		{
			int TmpID = (data[i + 1]) - 1;
			if (TmpID != 67)
				continue;

			float confidence = data[i + 2];
			if (confidence > confThreshold)
			{
				int left = (int)(data[i + 3] * frame.cols);
				int top = (int)(data[i + 4] * frame.rows);
				int right = (int)(data[i + 5] * frame.cols);
				int bottom = (int)(data[i + 6] * frame.rows);
				int width = right - left + 1;
				int height = bottom - top + 1;
				classIds.push_back((int)(data[i + 1]) - 1);  // Skip 0th background class id.
				boxes.push_back(cv::Rect(left, top, width, height));
				confidences.push_back(confidence);
			}
		}
	}
	else if (outLayerType == "Region")
	{
		for (size_t i = 0; i < outs.size(); ++i)
		{
			// Network produces output blob with a shape NxC where N is a number of
			// detected objects and C is a number of classes + 4 where the first 4
			// numbers are [center_x, center_y, width, height]
			float* data = (float*)outs[i].data;
			for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
			{
				cv::Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
				cv::Point classIdPoint;
				double confidence;
				minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);

				if (classIdPoint.x != 67)
					continue;

				if (confidence > confThreshold)
				{
					int centerX = (int)(data[0] * frame.cols);
					int centerY = (int)(data[1] * frame.rows);
					int width = (int)(data[2] * frame.cols);
					int height = (int)(data[3] * frame.rows);
					int left = centerX - width / 2;
					int top = centerY - height / 2;

					classIds.push_back(classIdPoint.x);
					confidences.push_back((float)confidence);
					boxes.push_back(cv::Rect(left, top, width, height));
				}
			}
		}
	}
	
	std::vector<int> indices;
	cv::dnn::NMSBoxes(boxes, confidences, confThreshold, nmsThreshold, indices);
	
	return boxes;
}

std::vector<cv::String> getOutputsNames(const cv::dnn::Net& net)
{
	static std::vector<cv::String> names;
	if (names.empty())
	{
		std::vector<int> outLayers = net.getUnconnectedOutLayers();
		std::vector<cv::String> layersNames = net.getLayerNames();
		names.resize(outLayers.size());
		for (size_t i = 0; i < outLayers.size(); ++i)
			names[i] = layersNames[outLayers[i] - 1];
	}
	return names;
}
