#include "PointCloudHelperFunction.h"

double PointCloudHelperFunctions::ComputeBoundingBox(const CloudXYZRGB_Ptr pcl_cloud, Eigen::Vector3d& upper_right, Eigen::Vector3d& bottom_left, double factor)
{
	if (pcl_cloud->empty())
		return -1.;
	
	double min_x, min_y, min_z, max_x, max_y, max_z;
	min_x = max_x = pcl_cloud->points[0].x;
	min_y = max_y = pcl_cloud->points[0].y;
	min_z = max_z = pcl_cloud->points[0].z;

	//#pragma omp parallel for 
	for (int i = 0; i < pcl_cloud->points.size(); ++i)
	{
		// Check if the point is invalid

		if (pcl_cloud->points[i].x < min_x)
			min_x = pcl_cloud->points[i].x;
		if (pcl_cloud->points[i].y < min_y)
			min_y = pcl_cloud->points[i].y;
		if (pcl_cloud->points[i].z < min_z)
			min_z = pcl_cloud->points[i].z;
		if (pcl_cloud->points[i].x > max_x)
			max_x = pcl_cloud->points[i].x;
		if (pcl_cloud->points[i].y > max_y)
			max_y = pcl_cloud->points[i].y;
		if (pcl_cloud->points[i].z > max_z)
			max_z = pcl_cloud->points[i].z;
	}
	upper_right = Eigen::Vector3d(max_x, max_y, max_z);
	bottom_left = Eigen::Vector3d(min_x, min_y, min_z);

	Eigen::Vector3d TmpAv = (upper_right + bottom_left) * 0.5;
	Eigen::Vector3d TmpDiff = (upper_right - bottom_left) * 0.5;
	bottom_left = TmpAv - (factor * TmpDiff);
	upper_right = TmpAv + (factor * TmpDiff);

	return (upper_right - bottom_left).norm();
}

double PointCloudHelperFunctions::MinimalZ(const CloudXYZRGB_Ptr pcl_cloud)
{
	if (pcl_cloud->empty())
		return -1.;
	double min_z;
	min_z = pcl_cloud->points[0].z;

	//#pragma omp parallel for 
	for (int i = 0; i < pcl_cloud->points.size(); ++i)
	{
		// Check if the point is invalid

		if (pcl_cloud->points[i].z < min_z)
			min_z = pcl_cloud->points[i].z;
	}
	return min_z;
}

double PointCloudHelperFunctions::computeCloudResolution(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud)
{
	double res = 0.0;
	int n_points = 0;
	pcl::search::KdTree<pcl::PointXYZRGB> tree;
	tree.setInputCloud(cloud);

	std::vector<int> indices(2);
	std::vector<float> sqr_distances(2);

	const int CloudSize = (int)cloud->size();

	for (int i = 0; i < CloudSize; i++)
	{
		if (!pcl_isfinite((*cloud)[i].x))
		{
			continue;
		}

		//Considering the second neighbor since the first is the point itself.
		int nres = tree.nearestKSearch(i, 2, indices, sqr_distances);
		if (nres == 2)
		{
			res += sqrt(sqr_distances[1]);
			n_points += 1;
		}
	}

	if (n_points != 0)
	{
		res /= n_points;
	}
	return res;
}

Eigen::Matrix3d PointCloudHelperFunctions::planePCA(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud)
{
	Eigen::Matrix3d Q;

	int DataSize = (int)cloud->size();

	Eigen::MatrixX3d data;
	data.resize(DataSize, 3);

	for (int i = 0; i < DataSize; i++)
	{
		data(i, 0) = cloud->points[i].x;
		data(i, 1) = cloud->points[i].y;
		data(i, 2) = cloud->points[i].z;
	}

	Eigen::Vector3d featureMeans = data.colwise().mean();

	for (int i = 0; i < DataSize; i++)
	{
		data.row(i) = data.row(i) - (featureMeans.transpose().eval());
	}

	//Eigen::MatrixXd standarsized = data.rowwise() - featureMeans;

	// Compute the covariance matrix.
	Eigen::MatrixXd cov = (data.adjoint().eval()) * data;
	//cov = cov / (DataSize - 1);

	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eig(cov);
	Eigen::Matrix3d Qinit = eig.eigenvectors();
	Eigen::Vector3d EV = eig.eigenvalues();

	double MaxVal = EV.maxCoeff() * 1.1;

	int perm[3] = { 2, 0, 1 };

	for (int j = 0; j < 3; j++)
	{
		double MinVal = MaxVal;
		int Index = -1;
		for (int i = 0; i < 3; i++)
		{
			if (EV(i) < MinVal)
			{
				Index = i;
				MinVal = EV(i);
			}
		}
		Q.col(perm[j]) = Qinit.col(Index);
		EV(Index) = MaxVal;
	}	

	for (int i = 0; i < 3; i++)
	{
		if (Q(i, i) < 0.0)
			Q.col(i) *= -1.0;
	}


	return Q;
}


Eigen::Matrix3d PointCloudHelperFunctions::Kabsch(const Eigen::Matrix3d &Q)
{
	Eigen::Matrix3d R;
	Eigen::Matrix3d in;
	in.setIdentity();

	Eigen::Matrix3d out = Q.transpose().eval();
	
	Eigen::Vector3d in_ctr = in.colwise().mean();
	Eigen::Vector3d out_ctr = out.colwise().mean();
	   
	for (int i = 0; i < 3; i++)
	{
		in.row(i) = in.row(i) - (in_ctr.transpose().eval());
		out.row(i) = out.row(i) - (out_ctr.transpose().eval());
	}	

	// SVD
	Eigen::MatrixXd Cov = (in.transpose().eval()) * out;
	Eigen::JacobiSVD<Eigen::MatrixXd> svd(Cov, Eigen::ComputeThinU | Eigen::ComputeThinV);

	// Find the rotation
	double d = (svd.matrixV() * svd.matrixU().transpose()).determinant();
	if (d > 0)
		d = 1.0;
	else
		d = -1.0;

	Eigen::Matrix3d I = Eigen::Matrix3d::Identity(3, 3);
	I(2, 2) = d;
	R = svd.matrixV() * (I * (svd.matrixU().transpose().eval()));
	   
	return R;
}

pcl::PointXYZ PointCloudHelperFunctions::CalcPoistion(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud, const Eigen::Matrix3d &Q)
{
	pcl::PointXYZ FinalPosition;

	Eigen::Vector3d Yaxis = Q.col(1);

	int DataSize = (int)cloud->size();

	Eigen::MatrixX3d data;
	data.resize(DataSize, 3);

	Eigen::VectorXd ProjectedData;
	ProjectedData.resize(DataSize);

	for (int i = 0; i < DataSize; i++)
	{
		data(i, 0) = cloud->points[i].x;
		data(i, 1) = cloud->points[i].y;
		data(i, 2) = cloud->points[i].z;
	}

	Eigen::Vector3d featureMeans = data.colwise().mean();

	for (int i = 0; i < DataSize; i++)
	{
		data.row(i) = data.row(i) - (featureMeans.transpose().eval());
		ProjectedData(i) = data.row(i).dot(Yaxis);
	}

	double MAxVal = ProjectedData.maxCoeff();

	Eigen::Vector3d TmpPi(0.0, MAxVal, 0.0);

	Eigen::Vector3d Pi;
	Pi = (Q.transpose().eval()) *(TmpPi);
	
	FinalPosition.x = Pi(0);
	FinalPosition.y = Pi(1);
	FinalPosition.z = Pi(2);

	return FinalPosition;
}