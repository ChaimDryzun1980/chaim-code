#pragma once
#include "CommonDefenitions.h"

namespace PointCloudHelperFunctions
{
	double ComputeBoundingBox(const CloudXYZRGB_Ptr pcl_cloud, Eigen::Vector3d& upper_right, Eigen::Vector3d& bottom_left, double factor = 1.0);
	double MinimalZ(const CloudXYZRGB_Ptr pcl_cloud);
	double computeCloudResolution(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud);

	Eigen::Matrix3d planePCA(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud);
	Eigen::Matrix3d Kabsch(const Eigen::Matrix3d &Q);

	pcl::PointXYZ CalcPoistion(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud, const Eigen::Matrix3d &Q);


	inline bool isnan(double x) {
		return x != x;
	}
}


