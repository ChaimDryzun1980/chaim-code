#pragma once
#include "CommonDefenitions.h"

namespace Conversions
{
	cv::Mat frame_to_mat(const rs2::frame& f);
	cv::Mat depth_frame_to_meters(const rs2::pipeline& pipe, const rs2::depth_frame& f);
}