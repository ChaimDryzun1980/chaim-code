#pragma once

#include <pcl/point_types.h>
#include <pcl/point_types.h>
#include <pcl/search/kdtree.h>
#include <pcl/common/io.h>

#include <vector>
#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <math.h>
#include <time.h>

#include <omp.h>

#include <opencv2/opencv.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>

#include <Eigen/Dense>

#include <librealsense2/rs.hpp>
#include <librealsense2/rsutil.h>

typedef pcl::PointCloud<pcl::PointXYZ> CloudXYZ;
typedef CloudXYZ::Ptr CloudXYZ_Ptr;

typedef pcl::PointCloud<pcl::PointXYZRGB> CloudXYZRGB;
typedef CloudXYZRGB::Ptr CloudXYZRGB_Ptr;

typedef pcl::PointCloud<pcl::PointXYZRGBA> CloudXYZRGBA;
typedef CloudXYZRGBA::Ptr CloudXYZRGBA_Ptr;

typedef pcl::PointCloud<pcl::PointNormal> CloudXYZNormal;
typedef CloudXYZNormal::Ptr CloudXYZNormal_Ptr;

typedef pcl::PointCloud<pcl::Normal> CloudNormal;
typedef CloudNormal::Ptr CloudNormal_Ptr;

typedef pcl::PointCloud<pcl::PointXYZRGBNormal> CloudXYZRGBNormal;
typedef CloudXYZRGBNormal::Ptr CloudXYZRGBNormal_Ptr;

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.283185307179586476925286766559
#define HALF_PI 1.5707963267948966192313216916398
#define RAD_TO_DEGREE 57.295779513082320876798154814105
#define DEGREE_TO_RAD 0.01745329251994329576923690768489
#define ZEPS   1.0e-10
#define INFTY  1.0e+10
#define CENTIMETER_PER_INCH	2.54
#define INCH_PER_CENTIMETER	0.3937007874015748031496062992126

struct MyEulerAngles
{
	double m_dPitch, m_dYaw, m_dRoll;
	MyEulerAngles()
	{
		m_dPitch = m_dYaw = m_dRoll = 0.0;
	}
};

