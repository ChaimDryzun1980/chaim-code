#include "PointCloudNormals.h"
#include "PointCloudHelperFunction.h"

pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr PointCloudNormals::AddNormals(CloudXYZRGB_Ptr cloud, double RadiusSearch) {
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointXYZRGBNormal>);

	if (cloud->size() == 0)
		return cloud_with_normals;

	pcl::PointCloud<pcl::Normal>::Ptr Norms(new pcl::PointCloud<pcl::Normal>);
	Norms = CalcNormals(cloud, RadiusSearch);
	pcl::concatenateFields(*cloud, *Norms, *cloud_with_normals);

	return cloud_with_normals;
}

pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr PointCloudNormals::AddNormals(CloudXYZRGB_Ptr cloud, int Neighbours /*= 50*/)
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointXYZRGBNormal>);

	if (cloud->size() == 0)
		return cloud_with_normals;

	pcl::PointCloud<pcl::Normal>::Ptr Norms(new pcl::PointCloud<pcl::Normal>);
	Norms = CalcNormals(cloud, Neighbours);
	pcl::concatenateFields(*cloud, *Norms, *cloud_with_normals);

	return cloud_with_normals;

}

pcl::PointCloud<pcl::Normal>::Ptr PointCloudNormals::CalcNormals(CloudXYZRGB_Ptr cloud, double RadiusSearch) {
	pcl::NormalEstimationOMP<pcl::PointXYZRGB, pcl::Normal> ne;
	//pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
	ne.setNumberOfThreads(omp_get_num_procs());
	//std::cout << "\tomp_get_num_procs() = " << omp_get_num_procs() << "\n";
	ne.setInputCloud(cloud);
	ne.setRadiusSearch(RadiusSearch);
	ne.setViewPoint(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
	
	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>());
	ne.compute(*cloud_normals);
	int cloudSize = (int)cloud_normals->size();

	for (int indx = 0; indx < cloudSize; indx++) {
		double Nrm = fabs((cloud_normals->points[indx].normal_x * cloud_normals->points[indx].normal_x) +
			(cloud_normals->points[indx].normal_y * cloud_normals->points[indx].normal_y) +
			(cloud_normals->points[indx].normal_z * cloud_normals->points[indx].normal_z));
		if (Nrm < ZEPS) Nrm = 1.0;
		else Nrm = (1.0) / Nrm;
		cloud_normals->points[indx].normal_x = cloud_normals->points[indx].normal_x * (Nrm);
		cloud_normals->points[indx].normal_y = cloud_normals->points[indx].normal_y * (Nrm);
		cloud_normals->points[indx].normal_z = cloud_normals->points[indx].normal_z * (Nrm);
	}

	return cloud_normals;
}

pcl::PointCloud<pcl::Normal>::Ptr PointCloudNormals::CalcNormals(CloudXYZRGB_Ptr cloud, int Neighbours /*= 50*/)
{
	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGB>());
	pcl::NormalEstimationOMP<pcl::PointXYZRGB, pcl::Normal> ne;
	ne.setSearchMethod(tree);
	ne.setKSearch(Neighbours);
	ne.setNumberOfThreads(omp_get_num_procs());
	ne.setInputCloud(cloud);
	ne.setViewPoint(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>());
	ne.compute(*cloud_normals);

	int cloudSize = (int)cloud_normals->size();

	pcl::PointXYZ Center(0.0, 0.0, 0.0);

	for (int indx = 0; indx < cloudSize; indx++) {
		double Nrm = fabs((cloud_normals->points[indx].normal_x * cloud_normals->points[indx].normal_x) +
			(cloud_normals->points[indx].normal_y * cloud_normals->points[indx].normal_y) +
			(cloud_normals->points[indx].normal_z * cloud_normals->points[indx].normal_z));
		if (Nrm < ZEPS) Nrm = 1.0;
		else Nrm = (1.0) / Nrm;
		cloud_normals->points[indx].normal_x = cloud_normals->points[indx].normal_x * (Nrm);
		cloud_normals->points[indx].normal_y = cloud_normals->points[indx].normal_y * (Nrm);
		cloud_normals->points[indx].normal_z = cloud_normals->points[indx].normal_z * (Nrm);

		Center.x += (cloud->points[indx].x * (1.0 / (double)cloudSize));
		Center.y += (cloud->points[indx].y * (1.0 / (double)cloudSize));
		Center.z += (cloud->points[indx].z * (1.0 / (double)cloudSize));
	}

	Eigen::Vector3d	VectA, VectB;
	for (int indx = 0; indx < cloudSize; indx++) {
		VectA = Eigen::Vector3d(cloud->points[indx].x - Center.x, cloud->points[indx].y - Center.y, cloud->points[indx].z - Center.z);
		VectA.normalize();
		VectB = Eigen::Vector3d(cloud_normals->points[indx].normal_x, cloud_normals->points[indx].normal_y, cloud_normals->points[indx].normal_z);
		VectB.normalize();
				
		if ((VectA.dot(VectB)) < 0) {
			cloud_normals->points[indx].normal_x = cloud_normals->points[indx].normal_x * (-1.0);
			cloud_normals->points[indx].normal_y = cloud_normals->points[indx].normal_y * (-1.0);
			cloud_normals->points[indx].normal_z = cloud_normals->points[indx].normal_z * (-1.0);
		}
	}

	return cloud_normals;
}

pcl::PointCloud<pcl::Normal>::Ptr PointCloudNormals::CalcNormalsWithCorrection(CloudXYZRGB_Ptr cloud, int KNN /* = 4 */, int Neighbours /*= 50*/)
{
	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>());
	cloud_normals = CalcNormals(cloud, Neighbours);

	Eigen::Vector3d Minbox, MaxBox, VectA, VectB;
	PointCloudHelperFunctions::ComputeBoundingBox(cloud, MaxBox, Minbox, 5.0);

	pcl::search::KdTree<pcl::PointXYZRGB> tree;
	tree.setInputCloud(cloud);

	std::vector<int> indices(KNN);
	std::vector<float> sqr_distances(KNN);
	pcl::PointXYZRGB Pi; // (MaxBox.x, MaxBox.y, MaxBox.z);
	Pi.x = MaxBox(0);	Pi.y = MaxBox(1);	Pi.z = MaxBox(2);
	Pi.r = 0;		Pi.g = 0;		Pi.b = 0;

	int nres = tree.nearestKSearch(Pi, 1, indices, sqr_distances);

	if (nres < 1) return cloud_normals;

	VectA = Eigen::Vector3d(MaxBox(0) - cloud->points[indices[0]].x, MaxBox(1) - cloud->points[indices[0]].y, MaxBox(2) - cloud->points[indices[0]].z);
	VectA.normalize();
	VectB = Eigen::Vector3d(cloud_normals->points[indices[0]].normal_x, cloud_normals->points[indices[0]].normal_y, cloud_normals->points[indices[0]].normal_z);
	VectB.normalize();

	if ((VectA.dot(VectB)) < 0) {
		cloud_normals->points[indices[0]].normal_x = cloud_normals->points[indices[0]].normal_x * (-1.0);
		cloud_normals->points[indices[0]].normal_y = cloud_normals->points[indices[0]].normal_y * (-1.0);
		cloud_normals->points[indices[0]].normal_z = cloud_normals->points[indices[0]].normal_z * (-1.0);
	}

	int CloudSize = (int)cloud->size();
	std::vector<bool> Seen;
	Seen.resize(CloudSize);
	std::queue<int> IndexQueue;
	for (int i = 0; i < CloudSize; i++) {
		Seen[i] = false;
	}

	IndexQueue.push(indices[0]);
	Seen[indices[0]] = true;

	while (!IndexQueue.empty()) {
		int Indx = IndexQueue.front();
		IndexQueue.pop();
		if (Seen[Indx]) continue;
		nres = tree.nearestKSearch(Pi, KNN, indices, sqr_distances);

		VectA = Eigen::Vector3d(cloud_normals->points[Indx].normal_x, cloud_normals->points[Indx].normal_y, cloud_normals->points[Indx].normal_z);
		VectA.normalize();

		for (int Nindex = 0; Nindex < nres; Nindex++) {
			if (Seen[indices[Nindex]]) continue;

			IndexQueue.push(indices[Nindex]);

			VectB = Eigen::Vector3d(cloud_normals->points[indices[Nindex]].normal_x, cloud_normals->points[indices[Nindex]].normal_y, cloud_normals->points[indices[Nindex]].normal_z);
			VectB.normalize();

			if ((VectA.dot(VectB)) < 0) {
				cloud_normals->points[indices[Nindex]].normal_x = cloud_normals->points[indices[Nindex]].normal_x * (-1.0);
				cloud_normals->points[indices[Nindex]].normal_y = cloud_normals->points[indices[Nindex]].normal_y * (-1.0);
				cloud_normals->points[indices[Nindex]].normal_z = cloud_normals->points[indices[Nindex]].normal_z * (-1.0);
			}

		}
	}

	return cloud_normals;
}

pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr PointCloudNormals::CalcMLSNormals(CloudXYZRGB_Ptr cloud, double Radius)
{
	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree;

	// Output has the PointNormal type in order to store the normals calculated by MLS
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr mls_points(new pcl::PointCloud<pcl::PointXYZRGBNormal>);

	// Init object (second point type is for the normals, even if unused)
	pcl::MovingLeastSquaresOMP<pcl::PointXYZRGB, pcl::PointXYZRGBNormal> mls;
	mls.setNumberOfThreads(omp_get_num_procs());
	mls.setComputeNormals(true);
	mls.setSearchMethod(tree);
	mls.setInputCloud(cloud);
	mls.setSearchRadius(Radius);
	mls.setPolynomialOrder(2);

	// Reconstruct
	mls.process(*mls_points);

	return mls_points;
}
