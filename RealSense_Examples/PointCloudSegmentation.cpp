#include "PointCloudSegmentation.h"
#include "PointCloudNormals.h"

std::vector<pcl::PointIndices> PointCloudSegmentation::RegionGrowingSegmentation(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, int Neighbours, int MinClusterSize, int MaxClusterSize, double SmoothnessThreshold, double CurvatureThreshold)
{
	pcl::search::Search<pcl::PointXYZRGB>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZRGB> >(new pcl::search::KdTree<pcl::PointXYZRGB>);
	pcl::PointCloud <pcl::Normal>::Ptr normals(new pcl::PointCloud <pcl::Normal>);
	//normals = PointCloudNormals::CalcNormalsWithCorrection(cloud, 4, Neighbours);
	normals = PointCloudNormals::CalcNormals(cloud, Neighbours);

	pcl::RegionGrowing<pcl::PointXYZRGB, pcl::Normal> reg;
	reg.setMinClusterSize(MinClusterSize);
	reg.setMaxClusterSize(MaxClusterSize);
	reg.setSearchMethod(tree);
	reg.setNumberOfNeighbours(Neighbours);
	reg.setInputCloud(cloud);
	reg.setInputNormals(normals);
	reg.setSmoothnessThreshold((SmoothnessThreshold / 180.0) * PI);
	reg.setCurvatureThreshold(CurvatureThreshold);

	std::vector <pcl::PointIndices> clusters;
	reg.extract(clusters);

	return clusters;
}

pcl::PointCloud <pcl::PointXYZRGB>::Ptr PointCloudSegmentation::BuildCloudFromIndices(pcl::PointCloud <pcl::PointXYZRGB>::Ptr cloud, std::vector<pcl::PointIndices> cluster_indices, int Indx)
{
	pcl::PointCloud <pcl::PointXYZRGB>::Ptr NewCloud(new pcl::PointCloud <pcl::PointXYZRGB>());

	int VectorSize = (int)cluster_indices.size();

	for (int i = 0; i < VectorSize; i++) {
		
		if ((Indx >= 0) && (Indx != i))
			continue;

		int IndicesNum = (int)cluster_indices[i].indices.size();
		for (int j = 0; j < IndicesNum; j++) {
			NewCloud->push_back(cloud->points[cluster_indices[i].indices[j]]);
		}
	}
	return NewCloud;
}

pcl::PointCloud<pcl::PointXYZRGB>::Ptr PointCloudSegmentation::BuildCloudFromIndicesColors(pcl::PointCloud <pcl::PointXYZRGB>::Ptr cloud, std::vector<pcl::PointIndices> cluster_indices)
{
	pcl::PointCloud <pcl::PointXYZRGB>::Ptr NewCloud(new pcl::PointCloud <pcl::PointXYZRGB>());
	int VectorSize = (int)cluster_indices.size();
	int Step = std::floor(255.0 / (double)VectorSize);
	int R, G, B;
	pcl::PointXYZRGB Pi;
	for (int i = 0; i < VectorSize; i++) {
		R = ((i + 1) % 2 == 0) ? ((i - 1)*Step) : (255 - (i * Step));
		B = ((i + 1) % 2 == 0) ?  (255 - ((i-1)*Step)) : (i * Step);
		G = ((i + 1) % 3 == 0) ? 128 : 0;
		int IndicesNum = (int)cluster_indices[i].indices.size();
		for (int j = 0; j < IndicesNum; j++) {
			Pi = cloud->points[cluster_indices[i].indices[j]];
			Pi.r = R;
			Pi.g = G;
			Pi.b = B;
			NewCloud->push_back(Pi);
		}
	}
	return NewCloud;
}

bool PointCloudSegmentation::SegmentPlane(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, pcl::ModelCoefficients::Ptr coeffcients, double DistanceThreshold, int ItterNum)
{
	// All the objects needed
	//pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGB>());

	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);

	// Create the segmentation object for plane segmentation and set all the parameters
	pcl::SACSegmentation<pcl::PointXYZRGB> seg;

	seg.setModelType(pcl::SACMODEL_PLANE);
	seg.setMethodType(pcl::SAC_RANSAC);
		seg.setMaxIterations(ItterNum);
	seg.setDistanceThreshold(DistanceThreshold);
	seg.setOptimizeCoefficients(true);
	seg.setInputCloud(cloud);

	// Obtain the plane inliers and coefficients
	seg.segment(*inliers, *coeffcients);

	if (inliers->indices.size() < 3) return false;

	return true;
}

pcl::PointCloud<pcl::PointXYZRGB>::Ptr PointCloudSegmentation::FindPlane(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, int Neighbours, int MinClusterSize, int MaxClusterSize, double SmoothnessThreshold, double CurvatureThreshold, double DistanceThreshold, int ItterNum)
{
	std::vector<pcl::PointIndices> cluster_indices;
	cluster_indices = PointCloudSegmentation::RegionGrowingSegmentation(cloud, Neighbours, MinClusterSize, MaxClusterSize, SmoothnessThreshold, CurvatureThreshold);

	pcl::PointCloud <pcl::PointXYZRGB>::Ptr NewCloud(new pcl::PointCloud <pcl::PointXYZRGB>());
	int VectorSize = (int)cluster_indices.size();

	// All the objects needed
	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGB>());

	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	pcl::ModelCoefficients::Ptr coeffcients(new pcl::ModelCoefficients);

	// Create the segmentation object for plane segmentation and set all the parameters
	pcl::SACSegmentation<pcl::PointXYZRGB> seg;

	seg.setModelType(pcl::SACMODEL_PLANE);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setMaxIterations(ItterNum);
	seg.setDistanceThreshold(DistanceThreshold);
	seg.setOptimizeCoefficients(true);
		
	int Step = std::floor(255.0 / (double)VectorSize);
	int R, G, B;
	pcl::PointXYZRGB Pi;

	for (int i = 0; i < VectorSize; i++) {
		pcl::PointCloud <pcl::PointXYZRGB>::Ptr Cloudi(new pcl::PointCloud <pcl::PointXYZRGB>());
		Cloudi = PointCloudSegmentation::BuildCloudFromIndices(cloud, cluster_indices, i);
		
		seg.setInputCloud(Cloudi);

		// Obtain the plane inliers and coefficients
		seg.segment(*inliers, *coeffcients);

		if (inliers->indices.size() < (0.3*Cloudi->size()))
			continue;

		R = ((i + 1) % 2 == 0) ? ((i - 1)*Step) : (255 - (i * Step));
		B = ((i + 1) % 2 == 0) ? (255 - ((i - 1)*Step)) : (i * Step);
		G = ((i + 1) % 3 == 0) ? 128 : 0;

		for (int j = 0; j < inliers->indices.size(); j++)
		{
			Pi = Cloudi->points[inliers->indices[j]];
			Pi.r = R;
			Pi.g = G;
			Pi.b = B;
			NewCloud->push_back(Pi);
		}
	}
	return NewCloud;
}
