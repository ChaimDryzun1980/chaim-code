#pragma once

#include "CommonDefenitions.h"

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/organized_multi_plane_segmentation.h>

namespace PointCloudSegmentation
{
	std::vector<pcl::PointIndices> RegionGrowingSegmentation(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, int Neighbours = 50, int MinClusterSize = 1, int MaxClusterSize = 100000000, double SmoothnessThreshold = 7.0, double CurvatureThreshold = 1.0);
	pcl::PointCloud <pcl::PointXYZRGB>::Ptr BuildCloudFromIndices(pcl::PointCloud <pcl::PointXYZRGB>::Ptr cloud, std::vector<pcl::PointIndices> cluster_indices, int Indx = -1);
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr BuildCloudFromIndicesColors(pcl::PointCloud <pcl::PointXYZRGB>::Ptr cloud, std::vector<pcl::PointIndices> cluster_indices);
	bool SegmentPlane(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, pcl::ModelCoefficients::Ptr coefficients, double DistanceThreshold = 0.15, int ItterNum = 1000);

	pcl::PointCloud<pcl::PointXYZRGB>::Ptr FindPlane(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, int Neighbours = 50, int MinClusterSize = 1, int MaxClusterSize = 100000000, double SmoothnessThreshold = 7.0, double CurvatureThreshold = 1.0, double DistanceThreshold = 0.01, int ItterNum = 1000);
}
