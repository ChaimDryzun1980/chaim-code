#pragma once

#include "CommonDefenitions.h"
#include <pcl/visualization/cloud_viewer.h>

namespace PointCloudVisualization
{
	// Simple point cloud viewer
	void ShowCloud(const CloudXYZRGB_Ptr pcl_cloud);

	// Single point clouds viewer
	void Visualize(const CloudXYZRGB_Ptr pcl_cloud);
	
	void Visualize(const CloudXYZRGB_Ptr Target, const CloudXYZRGB_Ptr NewCloud, std::string TextA = "First", std::string TextB = "Second");
}
