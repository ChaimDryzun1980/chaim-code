#include "PointCloudVisualization.h"

void PointCloudVisualization::ShowCloud(const CloudXYZRGB_Ptr pcl_cloud)
{
	pcl::visualization::CloudViewer viewer("Simple Cloud Viewer");
	viewer.showCloud(pcl_cloud);
	while (!viewer.wasStopped()) {
	}
}

void PointCloudVisualization::Visualize(const CloudXYZRGB_Ptr pcl_cloud)
{
	pcl::visualization::PCLVisualizer viewer("Point Cloud Visualization");

	// The color we will be using
	float bckgr_gray_level = 0.0;  // Black
	
	// Original point cloud is white
	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(pcl_cloud);
	viewer.addPointCloud<pcl::PointXYZRGB>(pcl_cloud, rgb, "cloud_in_v1");
	viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "cloud_in_v1");

	// Set background color
	viewer.setBackgroundColor(bckgr_gray_level, bckgr_gray_level, bckgr_gray_level);

	// Display the visualiser
	while (!viewer.wasStopped())
	{
		viewer.spinOnce();
	}
	viewer.close();
}

void PointCloudVisualization::Visualize(const CloudXYZRGB_Ptr Target, const CloudXYZRGB_Ptr NewCloud, std::string TextA, std::string TextB)
{
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("Point Cloud Visualization"));
	//pcl::visualization::PCLVisualizer viewer("Point Cloud Visualization");

	// Set background color
	viewer->setBackgroundColor(0, 0, 0);

	int v1(0);
	viewer->createViewPort(0.0, 0.0, 0.5, 1.0, v1);
	viewer->setBackgroundColor(0, 0, 0, v1);
	viewer->addText(TextA, 10, 10, "v1 text", v1);

	int v2(0);
	viewer->createViewPort(0.5, 0.0, 1.0, 1.0, v2);
	viewer->setBackgroundColor(0.0, 0.0, 0.0, v2);
	viewer->addText(TextB, 10, 10, "v2 text", v2);

	int colorA = 255;
	int colorB = 0;
	int colorC = 0;

	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgbA(Target);
	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgbB(NewCloud);
	viewer->addPointCloud<>(Target, rgbA, "Target cloud", v1);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "Target cloud", v1);
	viewer->addPointCloud<>(NewCloud, rgbB, "Scene cloud", v2);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "Scene cloud", v2);
	viewer->resetCamera();

	// Display the visualizer
	while (!viewer->wasStopped())
	{
		viewer->spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}
	viewer->close();
}