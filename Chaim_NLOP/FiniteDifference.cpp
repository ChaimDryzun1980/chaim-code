#include "FiniteDifference.h"

FiniteDiffernece::FiniteDiffernece(void* objp, double(*func)(std::vector<double>, void*))
{
	m_Function = func;
	m_ObjPtr = objp;
	m_DerivationType = eCentral;
}

FiniteDiffernece::~FiniteDiffernece()
{

}

std::vector<double> FiniteDiffernece::CalcGradient(std::vector<double> X, std::vector<double> Step)
{
	int Dim = X.size();

	std::vector<double> Gradient;
	Gradient.resize(Dim);

	if (Step.size() != Dim) return Gradient;

	for (int i = 0; i < Dim; i++) {
		double h = fabs(Step[i]);
		double F1, F2;
		if (h < FDZeps) h = FDZeps;
		if ((m_DerivationType == eCentral) || (m_DerivationType == eForward)) {
			X[i] += h;
		}
		F1 = m_Function(X, m_ObjPtr);

		if (m_DerivationType == eCentral) {
			X[i] -= (2.0 * h);
		}
		else {
			X[i] -= h;
		}
		F2 = m_Function(X, m_ObjPtr);
		if ((m_DerivationType == eCentral) || (m_DerivationType == eBackward)) {
			X[i] += h;
		}
		Gradient[i] = (F1 - F2);
		if (m_DerivationType == eCentral) {
			Gradient[i] /= (2.0 * h);
		}
		else {
			Gradient[i] /= h;
		}
		if (fabs(Gradient[i]) < FDZeps) { Gradient[i] = 0.0; }
	}
	return Gradient;
}

std::vector<std::vector<double>> FiniteDiffernece::CalcHessian(std::vector<double> X, std::vector<double> Step)
{
	int Dim = X.size();

	std::vector<std::vector<double>> Hessian;
	Hessian.resize(Dim);

	if (Step.size() != Dim) return Hessian;

	for (int i = 0; i < Dim; i++) {
		Hessian[i].resize(Dim);

		double hi = fabs(Step[i]);
		if (hi < FDZeps) hi = FDZeps;

		double F, Fp1, Fp2, Fm1, Fm2;

		F = m_Function(X, m_ObjPtr);

		X[i] += 2.0 * hi;
		Fp2 = m_Function(X, m_ObjPtr);

		X[i] -= hi;
		Fp1 = m_Function(X, m_ObjPtr);

		X[i] -= 3.0 * hi;
		Fm2 = m_Function(X, m_ObjPtr);

		X[i] += hi;
		Fm1 = m_Function(X, m_ObjPtr);

		X[i] += hi;

		Hessian[i][i] = (-Fp2 + (16.0 * Fp1) - (30.0 * F) + (16.0 * Fm1) - Fm2) / (12.0 * hi * hi);

		if (fabs(Hessian[i][i]) < FDZeps) { Hessian[i][i] = 0.0; }

		for (int j = i + 1; j < Dim; j++) {
			double hj = fabs(Step[i]);
			if (hj < FDZeps) hj = FDZeps;
			X[i] += hi;
			X[j] += hj;
			Fp2 = m_Function(X, m_ObjPtr);

			X[j] -= (2.0 * hj);
			Fp1 = m_Function(X, m_ObjPtr);

			X[i] -= (2.0 * hi);
			Fm2 = m_Function(X, m_ObjPtr);

			X[j] += (2.0 * hj);
			Fm1 = m_Function(X, m_ObjPtr);

			X[i] += hi;
			X[j] -= hj;

			Hessian[i][j] = (Fp2 - Fp1 - Fm1 + Fm2) / (4.0 * hi * hj);
			Hessian[j][i] = Hessian[i][j];

			if (fabs(Hessian[i][j]) < FDZeps) { Hessian[i][j] = 0.0; Hessian[j][i] = 0.0; }
		}
	}
	return Hessian;
}


