#pragma once
#include <vector>

#define FireFlysZeps 0.0000000001

class FireFlysOptimization
{
public:

	enum OptimizationType { eMin, eMax };

	enum BoundaryType
	{
		ePeriodic, eRefractive, eRandom, eTrim, eNone
	};

	FireFlysOptimization(void* objp, double(*func)(std::vector<double>, void*));
	virtual ~FireFlysOptimization() { }

	virtual void SetDimension(const int Dimension){ m_Dim = Dimension; }
	virtual int GetDimension() { return m_Dim; }

	virtual void SetVariables(const std::vector<double> Variables) { m_Variables = Variables; }
	virtual std::vector<double> GetVariables() { return m_Variables; }

	virtual void SetVariablesMin(const std::vector<double> VariablesMin) { m_MinVar = VariablesMin; }
	virtual void SetVariablesMax(const std::vector<double> VariablesMax) { m_MaxVar = VariablesMax; }

	virtual double GetSolution() { return m_Solution; }

	virtual std::vector<std::vector<double>> GetAllVariables() { return m_FireFlys; }
	virtual std::vector<double> GetAllSolutions();

	virtual void SetMaxIteration(const int MaxIteration) { m_MaxIteration = MaxIteration; }

	virtual int GetIteration() { return m_iter; }

	virtual void SetTollerance(const double Epsilon) { m_Epsilon = Epsilon; }

	virtual void SetOptimizationType(OptimizationType OT) { m_OptimizationType = OT; }

	virtual void SetBoundaryType(BoundaryType BT) { m_BoundaryType = BT; }

	virtual void SetFireFlysNumber(int FireFlysNum) { m_FireFlysNum = FireFlysNum; }

	virtual void SetAlpha(double Alpha0) { m_Alpha0 = Alpha0; }
	virtual void SetBeta(double Beta0) { m_Beta0 = Beta0; }
	virtual void SetGamma(double Gamma) { m_Gamma = Gamma; }
	virtual void SetDelta(double Delta) { m_Delta = Delta; }
	virtual void SetScale(double L)	{ m_Alpha0 = 0.01 * fabs(L); m_Gamma = 1.0 / sqrt(fabs(L)); }

	virtual bool Optimize();

	virtual void GetResults(std::vector<std::vector<double>>& vResults);
	virtual void GetParams(double& Alpha, double& Beta0, double& Gamma, double& Delta);
	virtual void GetFireFlys(std::vector<std::vector<double>>& vFireFlys);

	virtual void SetMinimalSolutionValue(const double MinSolVal) { m_MinSolVal = MinSolVal; }

private:

	int						m_Dim;
	int						m_MaxIteration;
	int						m_iter;
	int						m_FireFlysNum;
	std::vector<double>		m_Variables;
	std::vector<double>		m_MinVar;
	std::vector<double>		m_MaxVar;
	std::vector<std::vector<double>>	m_FireFlys;
	std::vector<double>			m_LightIntensities;
	double			m_Solution;
	double			m_Epsilon;
	double			m_MinSolVal;
	double			m_Alpha0, m_Beta0, m_Gamma, m_Delta;
	OptimizationType m_OptimizationType;
	BoundaryType	m_BoundaryType;

	void* m_ObjPtr;
	double(*m_Function)(std::vector<double>, void*);

	void Sort();
	double Norm2(std::vector<double> Vect);
};
