#include "DownHillSimplex.h"
#include <iostream>

DownhillSimplex::DownhillSimplex(void* objp, double(*func)(std::vector<double>, void*))
{
	m_Function = func;
	m_ObjPtr = objp;
	m_Dim = 2;
	m_Variables.resize(m_Dim);
	m_StepSize.resize(m_Dim);
	for (int i = 0; i < m_Dim; i++) {
		m_Variables[i] = 0.0;
		m_StepSize[i] = DownhillSimplexZeps;
	}
	m_Solution = 0.0;
	m_MaxIteration = 100 * m_Dim;
	m_Epsilon = DownhillSimplexZeps;
	m_Alpha = 1.0;
	m_Gamma = 2.0;
	m_Rho = 0.5;
	m_Sigma = 0.5;
	m_OptimizationType = eMin;
	m_BoundaryType = eTrim;
	m_Iter = 0;
}

DownhillSimplex::~DownhillSimplex()
{

}

bool DownhillSimplex::Optimize()
{
	// variables declaration
	std::vector<double> X0, Xr, Xe, Xc;
	X0.resize(m_Dim);
	Xr.resize(m_Dim);
	Xe.resize(m_Dim);
	Xc.resize(m_Dim);

	double Fr, Fe, Fc, DS_STD = 0.0;
	int SimplexSize = m_Dim + 1;
	m_Iter = 0;

	//clock_t NewTime = clock();

	std::vector<std::vector<double>> X;
	std::vector<double> Fx;

	X.resize(SimplexSize);
	Fx.resize(SimplexSize);


	// Setting the initial simplex
	for (int i = 0; i < SimplexSize; i++)
	{
		X[i] = m_Variables;

		if (i > 0){
			X[i][i - 1] += m_StepSize[i - 1];
		}
		Fx[i] = m_Function(X[i], m_ObjPtr);
		if (m_OptimizationType == eMax) { Fx[i] *= -1.0; }
	}

	//std::cout << m_Iter << " " << DS_STD << " " << Fx[0] << " " << Fx[m_Dim] << "\n";

	// Optimization
	do {

		for (int i = 0; i < m_Dim; i++) {
			X0[i] = 0.0;
		}

		//Sorting the Simplex results
		for (int i = 0; i < m_Dim; i++) {
			for (int j = i + 1; j < SimplexSize; j++) {
				if (Fx[j] < Fx[i]) {
					double TmpFx = Fx[j];
					Fx[j] = Fx[i];
					Fx[i] = TmpFx;
					std::vector<double> TmpX = X[j];
					X[j] = X[i];
					X[i] = TmpX;
				}
			}
			//X0 += X[i];
			X0 = VectorsAddition(X0, X[i]);
		}

		//X0 *= 1.0 / (double)m_Dim;
		X0 = VectorScale(X0, 1.0 / (double)m_Dim);

		Xr = DownHillSimplexOperation(X0, (1 + m_Alpha), X[m_Dim], -m_Alpha);
		Fr = m_Function(Xr, m_ObjPtr);
		if (m_OptimizationType == eMax) { Fr *= -1.0; }

		if ((Fr >= Fx[0]) && (Fr < Fx[m_Dim - 1])) {
			X[m_Dim] = Xr;
			Fx[m_Dim] = Fr;
		}
		else if (Fr < Fx[0]) {
			Xe = DownHillSimplexOperation(X0, (1 + m_Gamma), Xr, -m_Gamma);
			Fe = m_Function(Xe, m_ObjPtr);
			if (m_OptimizationType == eMax) { Fe *= -1.0; }

			if (Fe < Fr) {
				X[m_Dim] = Xe;
				Fx[m_Dim] = Fe;
			}
			else {
				X[m_Dim] = Xr;
				Fx[m_Dim] = Fr;
			}
		}
		else  {
			Xc = DownHillSimplexOperation(X0, (1.0 - m_Rho), X[m_Dim], m_Rho);
			Fc = m_Function(Xc, m_ObjPtr);
			if (m_OptimizationType == eMax) { Fc *= -1.0; }

			if (Fc < Fx[m_Dim]) {
				X[m_Dim] = Xc;
				Fx[m_Dim] = Fc;
			}
			else {
				for (int i = 1; i < SimplexSize; i++) {
					X[i] = DownHillSimplexOperation(X[0], (1.0 - m_Sigma), X[i], m_Sigma);
					Fx[i] = m_Function(X[i], m_ObjPtr);
					if (m_OptimizationType == eMax) { Fx[i] *= -1.0; }
				}
			}
		}
		m_Iter++;
		DS_STD = 0.0;
		double Avg = 0.0;
		for (int i = 0; i < SimplexSize; i++) {
			Avg += Fx[i];
		}
		Avg /= (double)SimplexSize;
		for (int i = 0; i < SimplexSize; i++) {
			DS_STD += ((Fx[i] - Avg) * (Fx[i] - Avg));
		}
		DS_STD /= ((double)SimplexSize + 1.0);
		DS_STD = sqrt(DS_STD);
	
		//std::cout << m_Iter << " " << DS_STD << " " << Fx[0] << " " << Fx[m_Dim] << "\n";

	} while ((m_Iter < m_MaxIteration) && (DS_STD > m_Epsilon));

	for (int i = 0; i < m_Dim; i++) {
		for (int j = i + 1; j < SimplexSize; j++) {
			if (Fx[j] < Fx[i]) {
				double TmpFx = Fx[j];
				Fx[j] = Fx[i];
				Fx[i] = TmpFx;
				std::vector<double> TmpX = X[j];
				X[j] = X[i];
				X[i] = TmpX;
			}
		}
	}

	//NewTime = clock() - NewTime;
	//std::cout << "\nComputation Time = " << (std::to_string(((float)NewTime) / CLOCKS_PER_SEC)) << "   Itr = " << m_Iter << "\n";


	m_Solution = Fx[0];
	if (m_OptimizationType == eMax) { m_Solution *= -1.0; }
	m_Variables = X[0];

	return true;
}

std::vector<double> DownhillSimplex::DownHillSimplexOperation(std::vector<double> A, double w1, std::vector<double> B, double w2)
{
	std::vector<double> C;
	int VectSize = A.size();
	C.resize(VectSize);

	int RunSize = VectSize > B.size() ? B.size() : VectSize;

	for (int i = 0; i < RunSize; i++) {
		C[i] = (w1 * A[i]) + (w2 * B[i]);
	}

	return C;
}

std::vector<double> DownhillSimplex::VectorsAddition(std::vector<double> A, std::vector<double> B)
{
	std::vector<double> C;
	int VectSize = A.size();
	C.resize(VectSize);

	int RunSize = VectSize > B.size() ? B.size() : VectSize;

	for (int i = 0; i < RunSize; i++) {
		C[i] = A[i] + B[i];
	}
	return C;
}

std::vector<double> DownhillSimplex::VectorScale(std::vector<double> A, double Scale)
{
	std::vector<double> C;
	int VectSize = A.size();
	C.resize(VectSize);

	for (int i = 0; i < VectSize; i++) {
		C[i] = Scale* A[i];
	}
	return C;

}
