#include "BrentsMethod.h"
#include <cmath>

BrentsMethod::BrentsMethod(void* objp, double(*func)(double, void*))
{
	m_Function = func;
	m_ObjPtr = objp;
	m_Variable = 0.0;
	m_LowerLimit = 0.0;
	m_UpperLimit = 0.0;
	m_Solution = 0.0;
	m_MaxIteration = 1000;
	m_Epsilon = BrentZeps;
	m_OptimizationType = eMin;
	m_Iter = 0;
}

BrentsMethod::~BrentsMethod()
{

}

bool BrentsMethod::Optimize()
{

	double x, a = m_LowerLimit, c = m_UpperLimit, b = c + ((a - c) / Brent_Half_PHI), d = a + ((c - a) / Brent_Half_PHI);
	double fx, fd, fa = m_Function(a, m_ObjPtr), fb = m_Function(b, m_ObjPtr), fc = m_Function(c, m_ObjPtr);


	while ((fa > fb) && (fb > fc) && (fabs((c - a) / 2.0) > m_Epsilon)) {
		b = c;
		c = (b - (a / Brent_Half_PHI)) / (1.0 - (1.0 / Brent_Half_PHI));
		d = a + ((c - a) / Brent_Half_PHI);
		fb = m_Function(b, m_ObjPtr);
		fc = m_Function(c, m_ObjPtr);
	}


	while ((fa < fb) && (fabs((c - a) / 2.0) > m_Epsilon)) {
		c = b;
		b = c + ((a - c) / Brent_Half_PHI);
		d = a + ((c - a) / Brent_Half_PHI);
		fb = m_Function(b, m_ObjPtr);
		fc = m_Function(c, m_ObjPtr);
	}

	while ((fc < fb) && (fabs((c - a) / 2.0) > m_Epsilon)) {
		a = b;
		b = c + ((a - c) / Brent_Half_PHI);
		d = a + ((c - a) / Brent_Half_PHI);
		fb = m_Function(b, m_ObjPtr);
		fa = m_Function(a, m_ObjPtr);
	}

	if (m_OptimizationType == eMax) {
		fa *= -1.0;
		fb *= -1.0;
		fc *= -1.0;
	}

	if (fabs(c - a) < BrentZeps) return false;

	while ((fabs((c - a) / 2.0) > m_Epsilon) && (m_Iter < m_MaxIteration)) {
		bool ParabolicInter = true;
		double ma = ((fb - fa) / (b - a)), mb = ((fc - fa) / (c - a));
		if (fabs(ma - mb) > BrentZeps)  {

			double ba = b - a, bc = b - c, fbc = fb - fc, fba = fb - fa;
			x = b - (((ba * ba * fbc) - (bc * bc * fba)) / (2.0 * ((ba * fbc) - (bc * fba))));
			fx = m_Function(x, m_ObjPtr);

			if ((x >= a) && (x <= c) && (fx < fb)){
				if (fabs(x - b) < m_Epsilon) {
					a = x - (m_Epsilon / 4.0);
					c = x + (m_Epsilon / 4.0);
				}
				else if (x < b) {
					c = b;
					fc = fb;
				}
				else {
					a = b;
					fa = fb;
				}
				b = x;
				fb = fx;
				if (m_OptimizationType == eMax) {
					fb *= -1.0;
				}
			}
			else
			{
				ParabolicInter = false;
			}
		}
		else {
			ParabolicInter = false;
		}

		if (!ParabolicInter) {
			b = c + ((a - c) / Brent_Half_PHI);
			d = a + ((c - a) / Brent_Half_PHI);

			fb = m_Function(b, m_ObjPtr);
			fd = m_Function(d, m_ObjPtr);

			if (m_OptimizationType == eMax) {
				fb *= -1.0;
				fd *= -1.0;
			}

			if (fb < fd) {
				c = d;
				fc = fd;
			}
			else
			{
				a = b;
				fa = fb;
			}

			b = c + ((a - c) / Brent_Half_PHI);
			d = a + ((c - a) / Brent_Half_PHI);

			fb = m_Function(b, m_ObjPtr);
		}

		if (m_OptimizationType == eMax) {
			fb *= -1.0;
		}

		m_Iter++;
	}

	m_Variable = b;
	m_Solution = fb;

	if (fa < m_Solution) {
		m_Variable = a;
		m_Solution = fa;
	}

	if (fc < m_Solution) {
		m_Variable = c;
		m_Solution = fc;
	}


	return true;
}
