#pragma once

#include <vector>

#define DownhillSimplexZeps 0.0000000001

class DownhillSimplex
{
public:
	enum OptimizationType { eMin, eMax };

	enum BoundaryType
	{
		ePeriodic, eRefractive, eRandom, eTrim, eNone
	};

	DownhillSimplex(void* objp, double(*func)(std::vector<double>, void*));
	virtual ~DownhillSimplex();

	virtual void SetDimension(const int Dimension){ m_Dim = Dimension; }
	virtual int GetDimension() { return m_Dim; }

	virtual void SetVariables(const std::vector<double> Variables) { m_Variables = Variables; }
	virtual std::vector<double> GetVariables() { return m_Variables; }

	virtual double GetSolution() { return m_Solution; }

	virtual void SetStepSize(const std::vector<double> StepSize) { m_StepSize = StepSize; }

	virtual void SetMaxIteration(const int MaxIteration) { m_MaxIteration = MaxIteration; }

	virtual int GetIteration() { return m_Iter; }

	virtual void SetTollerance(const double Epsilon) { m_Epsilon = Epsilon; }

	virtual void SetOptimizationType(OptimizationType OT) { m_OptimizationType = OT; }

	virtual void SetBoundaryType(BoundaryType BT) { m_BoundaryType = BT; }
	
	virtual void SetAlpha(double Alpha) { m_Alpha = Alpha; }
	virtual void SetGamma(double Gamma) { m_Gamma = Gamma; }
	virtual void SetRho(double Rho) { m_Rho = Rho; }
	virtual void SetSigma(double Sigma) { m_Sigma = Sigma; }

	virtual bool Optimize();

private:

	int				m_Dim;
	int				m_MaxIteration;
	int				m_Iter;
	std::vector<double>			m_Variables;
	std::vector<double>			m_StepSize;
	double			m_Solution;
	double			m_Epsilon;
	double			m_Alpha, m_Rho, m_Gamma, m_Sigma;
	OptimizationType m_OptimizationType;
	BoundaryType	m_BoundaryType;
	void* m_ObjPtr;
	double(*m_Function)(std::vector<double>, void*);

	std::vector<double> DownHillSimplexOperation(std::vector<double> A, double w1, std::vector<double> B, double w2);
	std::vector<double> VectorsAddition(std::vector<double> A, std::vector<double> B);
	std::vector<double> VectorScale(std::vector<double> A, double Scale);

};
