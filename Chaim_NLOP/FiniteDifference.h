#pragma once
#include <vector>
#include <string>

#define FDZeps 0.0000000001

class FiniteDiffernece {
public:

	enum DerivationType{ eCentral, eForward, eBackward };

	FiniteDiffernece(void* objp, double(*func)(std::vector<double>, void*));
	virtual ~FiniteDiffernece();

	void SetDerivationType(DerivationType DT) { m_DerivationType = DT; }

	std::vector<double> CalcGradient(std::vector<double> X, std::vector<double> Step);
	std::vector<std::vector<double>> CalcHessian(std::vector<double> X, std::vector<double> Step);

private:
	double(*m_Function)(std::vector<double>, void*);
	void* m_ObjPtr;
	DerivationType	m_DerivationType;
};
