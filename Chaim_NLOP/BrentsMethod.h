#pragma once

#define BrentZeps 0.0000000001
#define  Brent_Half_PHI     1.6180339887498948482045868343656

class BrentsMethod
{
public:
	enum OptimizationType { eMin, eMax };

	BrentsMethod(void* objp, double(*func)(double, void*));
	virtual ~BrentsMethod();

	virtual double GetVariable() { return m_Variable; }

	virtual void SetLowerLimit(const double LowerLimit) { m_LowerLimit = LowerLimit; }
	virtual void setUpperLimit(const double UpperLimit) { m_UpperLimit = UpperLimit; }

	virtual double GetSolution() { return m_Solution; }

	virtual void SetMaxIteration(const int MaxIteration) { m_MaxIteration = MaxIteration; }

	virtual int GetIteration() { return m_Iter; }

	virtual void SetTollerance(const double Epsilon) { m_Epsilon = Epsilon; }

	virtual void SetOptimizationType(OptimizationType OT) { m_OptimizationType = OT; }

	virtual bool Optimize();

private:

	int				m_MaxIteration;
	int				m_Iter;
	double			m_Variable;
	double			m_LowerLimit;
	double			m_UpperLimit;
	double			m_Solution;
	double			m_Epsilon;
	void* m_ObjPtr;
	double(*m_Function)(double, void*);
	OptimizationType m_OptimizationType;
};
