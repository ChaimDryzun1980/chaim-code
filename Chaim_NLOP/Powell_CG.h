#pragma once
#include <vector>

#define PowellCGZeps 0.0000000001

class PowellConjugatedGradient
{
public:

	enum ConjugatedGradientType { eFR, ePR, eHS, eDY, eSD };
	enum CGLineSearchType { eCGGoldenRatio, eCGParabolicInterpolation, eCGBrent };
	enum OptimizationType { eMin, eMax };

	enum BoundaryType
	{
		ePeriodic, eRefractive, eRandom, eTrim, eNone
	};

	PowellConjugatedGradient(void* objp, double(*func)(std::vector<double>, void*));
	virtual ~PowellConjugatedGradient();

	virtual void SetDimension(const int Dimension){ m_Dim = Dimension; }
	virtual int GetDimension() { return m_Dim; }

	virtual void SetVariables(const std::vector<double> Variables) { m_Variables = Variables; }
	virtual std::vector<double> GetVariables() { return m_Variables; }

	virtual double GetSolution() { return m_Solution; }

	virtual void SetStepSize(const std::vector<double> StepSize) { m_StepSize = StepSize; }

	virtual void SetMaxIteration(const int MaxIteration) { m_MaxIteration = MaxIteration; }

	virtual int GetIteration() { return m_Iter; }

	virtual void SetTollerance(const double Epsilon) { m_Epsilon = Epsilon; }

	virtual void SetOptimizationType(OptimizationType OT) { m_OptimizationType = OT; }

	virtual void SetBoundaryType(BoundaryType BT) { m_BoundaryType = BT; }

	virtual void SetConjugatedGradientType(ConjugatedGradientType NT) { m_ConjugatedGradientType = NT; }


	virtual void AllowNegativeBeta(bool AllowNegativeBeta) { m_bAllowNegativeBeta = AllowNegativeBeta; }

	virtual bool Optimize();

	double CalcAlpha(double X);

private:

	int				m_Dim;
	int				m_MaxIteration;
	int				m_Iter;
	std::vector<double>	m_Variables;
	std::vector<double>	m_StepSize;
	std::vector<double> m_Sn;
	double			m_Solution;
	double			m_Epsilon;
	void* m_ObjPtr;
	double(*m_Function)(std::vector<double>, void*);
	OptimizationType m_OptimizationType;
	ConjugatedGradientType		m_ConjugatedGradientType;
	BoundaryType	m_BoundaryType;
	bool			m_bAllowNegativeBeta;

	std::vector<double> CalcGradient();

	std::vector<double> VectorScale(std::vector<double> X, double Scale);
	std::vector<double> VectorAddition(std::vector<double> A, std::vector<double> B, double weightA = 1.0, double weightB = 1.0);
	std::vector<double> VectorSub(std::vector<double> A, std::vector<double> B, double weightA = 1.0, double weightB = 1.0);
	double VectorMult(std::vector<double> A, std::vector<double> B, double weightA = 1.0, double weightB = 1.0);
	double VectorNorm(std::vector<double> A, double weightA = 1.0);
	double VectorNorm2(std::vector<double> A, double weightA = 1.0);

};
