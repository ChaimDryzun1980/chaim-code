#include "Powell_CG.h"
#include "BrentsMethod.h"
#include <iostream>

static double EvaluateGDoneD(double X, void* ObjPtr)
{
	return ((PowellConjugatedGradient*)ObjPtr)->CalcAlpha(X);
}

PowellConjugatedGradient::PowellConjugatedGradient(void* objp, double(*func)(std::vector<double>, void*))
{
	m_Function = func;
	m_ObjPtr = objp;
	m_Dim = 2;
	m_Variables.resize(m_Dim);
	m_Sn.resize(m_Dim);
	m_StepSize.resize(m_Dim);
	for (int i = 0; i < m_Dim; i++) {
		m_Variables[i] = 0.0;
		m_StepSize[i] = PowellCGZeps;
	}
	m_Solution = 0.0;
	m_MaxIteration = 100 * m_Dim;
	m_Epsilon = PowellCGZeps;
	m_OptimizationType = eMin;
	m_BoundaryType = eTrim;
	m_ConjugatedGradientType = ePR;
	m_bAllowNegativeBeta = false;
	m_Iter = 0;
}

PowellConjugatedGradient::~PowellConjugatedGradient()
{

}

bool PowellConjugatedGradient::Optimize()
{
	std::vector<double> Gradient;
	Gradient.resize(m_Dim);

	std::vector<double> PrevGradient;
	PrevGradient.resize(m_Dim);

	m_Sn.resize(m_Dim);
	m_Sn = m_Variables;

	std::vector<double> TmpVectA;
	TmpVectA.resize(m_Dim);

	double Err = 1.0, Beta = 0.0, alpha;
	int Cnt = 0, zeroCount = 0;
	m_Iter = 0;

	do
	{
		Gradient = CalcGradient();
		if (m_OptimizationType == eMin) {
			Gradient = VectorScale(Gradient, -1.0);
		}

		//m_Solution = m_Function(m_Variables, m_ObjPtr);

		Beta = 0.0;

		double PrevGradient2 = VectorNorm2(PrevGradient);
		double Gradient2 = VectorNorm2(Gradient);
		TmpVectA = VectorSub(Gradient, PrevGradient);
		double GradientPrevGradient = VectorMult(Gradient, TmpVectA);
		double SnPrevGradient = VectorMult(m_Sn, TmpVectA);

		if ((Cnt % m_Dim) != 0) {
			switch (m_ConjugatedGradientType)
			{
			case eFR:
				if (fabs(PrevGradient2) > PowellCGZeps) {
					Beta = (Gradient2) / (PrevGradient2);
				}
				break;
			case ePR:
				if (fabs(PrevGradient2) > PowellCGZeps) {
					Beta = (GradientPrevGradient) / (PrevGradient2);
				}
				break;
			case eHS:
				if (fabs(SnPrevGradient) > PowellCGZeps) {
					Beta = (GradientPrevGradient) / (SnPrevGradient);
				}
				break;
			case eDY:
				if (fabs(SnPrevGradient) > PowellCGZeps) {
					Beta = (Gradient2) / (SnPrevGradient);
				}
				break;
			case eSD:
				Beta = 0.0;
				break;
			default:
				if (fabs(PrevGradient2) > PowellCGZeps) {
					Beta = (GradientPrevGradient) / (PrevGradient2);
				}
				break;
			}
		}

		if ((!m_bAllowNegativeBeta) && (Beta < 0.0)) Beta = 0.0;

		//m_Sn = Gradient + (Beta * m_Sn);
		m_Sn = VectorAddition(Gradient, m_Sn, 1.0, Beta);

		//double gradSize = Gradient.Norm();
		double gradSize = VectorNorm(Gradient);

		BrentsMethod OneDSolver(this, EvaluateGDoneD);
		OneDSolver.SetLowerLimit(0.0);
		OneDSolver.setUpperLimit(gradSize);
		OneDSolver.SetMaxIteration(1000);
		OneDSolver.SetOptimizationType(BrentsMethod::eMin);
		OneDSolver.SetTollerance(BrentZeps);
		OneDSolver.Optimize();
		alpha = OneDSolver.GetVariable();

		std::cout << m_Iter << ": ";
		//for (int indx = 0; indx < m_Dim; indx++) {
		//std::cout << m_Variables[indx] << " ";
		//}
		//std::cout << "\n";

		//m_Variables += (alpha * m_Sn);
		m_Variables = VectorAddition(m_Variables, m_Sn, 1.0, alpha);

		//Err = (alpha * m_Sn).Norm();
		Err = VectorNorm(m_Sn, alpha);
		if (Err < m_Epsilon) {
			zeroCount++;
		}
		else {
			zeroCount = 0;
		}

		m_Solution = m_Function(m_Variables, m_ObjPtr);
		std::cout << m_Solution << "  (" << Beta << ", " << gradSize << ", " << alpha << ", " << Err << ")\n";

		PrevGradient = Gradient;
		Cnt++;
		m_Iter++;
	} while ((m_Iter < m_MaxIteration) && ((Err > m_Epsilon) || (zeroCount < m_Dim)));


	m_Solution = m_Function(m_Variables, m_ObjPtr);

	return true;
}

double PowellConjugatedGradient::CalcAlpha(double X)
{
	std::vector<double> tmpSn;
	tmpSn.resize(m_Dim);

	//tmpSn = m_Variables + (X * m_Sn);
	tmpSn = VectorAddition(m_Variables, m_Sn, 1.0, X);

	return m_Function(tmpSn, m_ObjPtr);
}

std::vector<double> PowellConjugatedGradient::CalcGradient()
{
	std::vector<double> Gradient;
	Gradient.resize(m_Dim);

	int Indx = (m_Iter % m_Dim);

	for (int i = 0; i < m_Dim; i++)
		Gradient[i] = 0.0;

	Gradient[Indx] = fabs(m_StepSize[Indx]);
	
	return Gradient;
}

std::vector<double> PowellConjugatedGradient::VectorScale(std::vector<double> X, double Scale)
{
	std::vector<double> C;
	int VectSize = X.size();
	C.resize(VectSize);

	for (int i = 0; i < VectSize; i++) {
		C[i] = Scale* X[i];
	}
	return C;
}

std::vector<double> PowellConjugatedGradient::VectorAddition(std::vector<double> A, std::vector<double> B, double weightA /*= 1.0*/, double weightB /*= 1.0*/)
{
	std::vector<double> C;
	int VectSize = A.size();
	C.resize(VectSize);

	int TrueSize = (B.size() < VectSize) ? B.size() : VectSize;

	for (int i = 0; i < TrueSize; i++) {
		C[i] = (weightA* A[i]) + (weightB* B[i]);
	}
	return C;
}

std::vector<double> PowellConjugatedGradient::VectorSub(std::vector<double> A, std::vector<double> B, double weightA /*= 1.0*/, double weightB /*= 1.0*/)
{
	std::vector<double> C;
	int VectSize = A.size();
	C.resize(VectSize);

	int TrueSize = (B.size() < VectSize) ? B.size() : VectSize;

	for (int i = 0; i < TrueSize; i++) {
		C[i] = (weightA* A[i]) - (weightB* B[i]);
	}
	return C;
}

double PowellConjugatedGradient::VectorMult(std::vector<double> A, std::vector<double> B, double weightA /*= 1.0*/, double weightB /*= 1.0*/)
{
	double C = 0.0;
	int TrueSize = (B.size() < A.size()) ? B.size() : A.size();
	for (int i = 0; i < TrueSize; i++) {
		C += (weightA* A[i]) * (weightB* B[i]);
	}
	return C;
}

double PowellConjugatedGradient::VectorNorm(std::vector<double> A, double weightA /*= 1.0*/)
{
	return sqrt(VectorNorm2(A, weightA));
}

double PowellConjugatedGradient::VectorNorm2(std::vector<double> A, double weightA /*= 1.0*/)
{
	double C = 0.0;
	for (int i = 0; i < A.size(); i++) {
		C += (weightA* A[i]) * (weightA* A[i]);
	}
	return C;
}

