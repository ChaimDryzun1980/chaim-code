#pragma once
#include <vector>
#include <functional>

#define PatternSearchZeps 0.0000000001

class PatternSearch
{
public:


	enum OptimizationType { eMin, eMax };

	enum BoundaryType
	{
		ePeriodic, eRefractive, eRandom, eTrim, eNone
	};

	PatternSearch(void* objp, double(*func)(std::vector<double>, void*));
	virtual ~PatternSearch();

	virtual void SetDimension(const int Dimension){ m_Dim = Dimension; }
	virtual int GetDimension() { return m_Dim; }

	virtual void SetVariables(const std::vector<double> Variables) { m_Variables = Variables; }
	virtual std::vector<double> GetVariables() { return m_Variables; }

	virtual double GetSolution() { return m_Solution; }

	virtual void SetStepSize(const std::vector<double> StepSize) { m_StepSize = StepSize; }

	virtual void SetMaxIteration(const int MaxIteration) { m_MaxIteration = MaxIteration; }

	virtual int GetIteration() { return m_iter; }

	virtual void SetTollerance(const double Epsilon) { m_Epsilon = Epsilon; }

	virtual void SetMinimalSolutionValue(const double MinSolVal) { m_MinSolVal = MinSolVal; }

	virtual void SetOptimizationType(OptimizationType OT) { m_OptimizationType = OT; }

	virtual void SetBoundaryType(BoundaryType BT) { m_BoundaryType = BT; }

	virtual void PrintData(bool PrintData) { m_bPrint = PrintData; }

	virtual bool Optimize();

	virtual void SetOptimizefunction(std::function<void(int, float)> status) {
		m_fOptimizeFunction = status;
		m_bOptimizeStatus = true;
	}

private:

	int				m_Dim;
	int				m_MaxIteration;
	int				m_iter;
	std::vector<double>	m_Variables;
	std::vector<double>	m_StepSize;
	double			m_Solution;
	double			m_Epsilon;
	double			m_MinSolVal;
	OptimizationType m_OptimizationType;
	BoundaryType	m_BoundaryType;
	void* m_ObjPtr;
	double(*m_Function)(std::vector<double>, void*);
	bool m_bPrint;

	std::function<void(int, float)> m_fOptimizeFunction;
	bool m_bOptimizeStatus;

};
