#include "PatternSearch.h"
#include <iostream>
#include <time.h> 
#include <string>

PatternSearch::PatternSearch(void* objp, double(*func)(std::vector<double>, void*))
{
	m_Function = func;
	m_ObjPtr = objp;
	m_Dim = 2;
	m_Variables.resize(m_Dim);
	m_StepSize.resize(m_Dim);
	for (int i = 0; i < m_Dim; i++) {
		m_Variables[i] = 0.0;
		m_StepSize[i] = PatternSearchZeps;
	}
	m_Solution = 0.0;
	m_MaxIteration = 100 * m_Dim;
	m_Epsilon = PatternSearchZeps;
	m_MinSolVal = -1000000.0;
	m_OptimizationType = eMin;
	m_BoundaryType = eTrim;
	m_iter = 0;
	m_bPrint = true;
	m_bOptimizeStatus = false;
}

PatternSearch::~PatternSearch()
{

}

bool PatternSearch::Optimize()
{
	int VarIndex = 0;
	bool Dir = 0;

	double GoldenRatioFactor = 2.0 / (1.0 + sqrt(5));

	m_Solution = m_Function(m_Variables, m_ObjPtr);
	double NewSol = m_Solution;

	std::vector<double> X = m_Variables;
	std::vector<double> tmpStep = m_StepSize;
	int i;
	m_iter = 0;

	//clock_t NewTime = clock();

	double AvrgStpSz = 0.0;
	for (i = 0; (i < m_Dim); i++) {
		AvrgStpSz += (tmpStep[i] / ((double)m_Dim));
	}

	int InnerItr = 0;

	if (m_Epsilon <= PatternSearchZeps) {
		m_Epsilon = PatternSearchZeps * 1.5;
	}

	if (m_bPrint)
	{
		std::cout << "\tItr = " << m_iter << "\tVal = " << m_Solution << "\tAvg Step Size = " << AvrgStpSz << " ";
		for (int Ki = 0; Ki < m_Dim; Ki++)
			std::cout << X[Ki] << " ";
		std::cout << "\n";
	}
	else if (m_bOptimizeStatus)
		m_fOptimizeFunction(m_iter, m_Solution);


	while ((m_Solution > m_MinSolVal) && (AvrgStpSz > m_Epsilon) && (m_iter < m_MaxIteration)) {

		for (i = 0; ((NewSol >= m_Solution) && (i < m_Dim)); i++) {
			if (Dir == 0) {
				X[(i + VarIndex) % m_Dim] -= tmpStep[(i + VarIndex) % m_Dim];
			}
			else {
				X[(i + VarIndex) % m_Dim] += tmpStep[(i + VarIndex) % m_Dim];
			}

			NewSol = m_Function(X, m_ObjPtr);
			if (m_OptimizationType == eMax) NewSol *= -1.0;

			if (Dir == 0) {
				X[(i + VarIndex) % m_Dim] += tmpStep[(i + VarIndex) % m_Dim];
			}
			else {
				X[(i + VarIndex) % m_Dim] -= tmpStep[(i + VarIndex) % m_Dim];
			}
		}

		if (NewSol >= m_Solution) {
			InnerItr = 0;
			Dir = (Dir + 1) % 2;
			for (i = 0; ((NewSol >= m_Solution) && (i < m_Dim)); i++) {
				if (Dir == 0) {
					X[(i + VarIndex) % m_Dim] -= tmpStep[(i + VarIndex) % m_Dim];
				}
				else {
					X[(i + VarIndex) % m_Dim] += tmpStep[(i + VarIndex) % m_Dim];
				}

				NewSol = m_Function(X, m_ObjPtr);
				if (m_OptimizationType == eMax) NewSol *= -1.0;

				if (Dir == 0) {
					X[(i + VarIndex) % m_Dim] += tmpStep[(i + VarIndex) % m_Dim];
				}
				else {
					X[(i + VarIndex) % m_Dim] -= tmpStep[(i + VarIndex) % m_Dim];
				}
			}

			if (NewSol >= m_Solution) {
				InnerItr = 0;
				VarIndex = 0;
				Dir = 0;
				AvrgStpSz = 0.0;
				for (i = 0; (i < m_Dim); i++) {
					tmpStep[i] *= GoldenRatioFactor;
					AvrgStpSz += (tmpStep[i] / ((double)m_Dim));
					if (tmpStep[i] < PatternSearchZeps) {
						tmpStep[i] = PatternSearchZeps;
					}
				}
			}
			else {
				InnerItr = 0;
				VarIndex = (i - 1 + VarIndex) % m_Dim;
				if (Dir == 0) {
					X[VarIndex] -= tmpStep[VarIndex];
				}
				else {
					X[VarIndex] += tmpStep[VarIndex];
				}
				m_Variables = X;
				m_Solution = NewSol;
				if (m_OptimizationType == eMax) m_Solution *= -1.0;
			}
		}
		else {
			InnerItr=0;
			VarIndex = (i - 1 + VarIndex) % m_Dim;
			if (Dir == 0) {
				X[VarIndex] -= tmpStep[VarIndex];
			}
			else {
				X[VarIndex] += tmpStep[VarIndex];
			}
			m_Variables = X;
			m_Solution = NewSol;
			if (m_OptimizationType == eMax) m_Solution *= -1.0;

			if (InnerItr == 2) {
				InnerItr = 0;
				tmpStep[VarIndex] *= (1.0 / GoldenRatioFactor);
			}

		}
		
		if (m_bPrint)
		{
			std::cout << "\tItr = " << m_iter << "\tVal = " << m_Solution << "\tAvg Step Size = " << AvrgStpSz << " ";
			for (int Ki = 0; Ki < m_Dim; Ki++)
				std::cout << X[Ki] << " ";
			std::cout << "\n";
		}
		else if (m_bOptimizeStatus)
			m_fOptimizeFunction(m_iter+1, m_Solution);
		
		m_iter++;
	}

	//NewTime = clock() - NewTime;
	//std::cout << "\nComputation Time = " << (std::to_string(((float)NewTime) / CLOCKS_PER_SEC)) << "   Itr = " << m_iter << "\n";

	return true;
}
