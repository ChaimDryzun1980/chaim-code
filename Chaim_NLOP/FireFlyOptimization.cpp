#include "FireFlyOptimization.h"
#include <iostream>
#include <random>
#include <ctime>
#include <math.h>

FireFlysOptimization::FireFlysOptimization(void* objp, double(*func)(std::vector<double>, void*))
{
	m_Function = func;
	m_ObjPtr = objp;
	m_Dim = 2;
	m_Variables.resize(m_Dim);
	m_MinVar.resize(m_Dim);
	m_MaxVar.resize(m_Dim);
	for (int i = 0; i < m_Dim; i++) {
		m_Variables[i] = 0.0;
		m_MinVar[i] = std::numeric_limits<double>::min();
		m_MaxVar[i] = std::numeric_limits<double>::max();
	}
	m_Solution = 0.0;
	m_FireFlysNum = 50;
	m_MaxIteration = 100;
	m_Epsilon = FireFlysZeps;
	m_OptimizationType = eMax;
	m_BoundaryType = eTrim;
	m_iter = 0;
	m_Beta0 = 1.0;
	m_Alpha0 = 0.05;
	m_Delta = 0.96;
	m_Gamma = 43.0;
	m_MinSolVal = std::numeric_limits<double>::max();
	m_FireFlys.resize(m_FireFlysNum);
	m_LightIntensities.resize(m_FireFlysNum);
}

std::vector<double> FireFlysOptimization::GetAllSolutions()
{
	if (m_OptimizationType == eMin) {
		for (int i = 0; i < m_FireFlysNum; i++) {
			m_LightIntensities[i] = (fabs(m_LightIntensities[i]) >= FireFlysZeps) ? 1.0 / m_LightIntensities[i] : 1.0 / (m_LightIntensities[i] + FireFlysZeps);
		}
	}
	return m_LightIntensities;
}

bool FireFlysOptimization::Optimize()
{
	srand(time(NULL));

	m_FireFlys.resize(m_FireFlysNum);
	m_LightIntensities.resize(m_FireFlysNum);

	double Tmpvl, BestVal;

	m_FireFlys[0].resize(m_Dim);
	for (int j = 0; j < m_Dim; j++) {
		Tmpvl = m_Variables[j];
		if ((Tmpvl < m_MinVar[j]) || (Tmpvl > m_MaxVar[j])) {
			int vl = rand() % RAND_MAX;
			Tmpvl = ((double)vl / (double)RAND_MAX);
			Tmpvl *= (m_MaxVar[j] - m_MinVar[j]);
			Tmpvl += m_MinVar[j];
		}
		m_FireFlys[0][j] = Tmpvl;
	}
	Tmpvl = m_Function(m_FireFlys[0], m_ObjPtr);
	m_LightIntensities[0] = Tmpvl;
	if (m_OptimizationType == eMin) {
		m_LightIntensities[0] = (fabs(m_LightIntensities[0]) >= FireFlysZeps) ? 1.0 / m_LightIntensities[0] : 1.0 / (m_LightIntensities[0] + FireFlysZeps);
	}

	for (int i = 1; i < m_FireFlysNum; i++) {
		do {
			m_FireFlys[i].resize(m_Dim);
			for (int j = 0; j < m_Dim; j++) {
				int vl = rand() % RAND_MAX;
				Tmpvl = ((double)vl / (double)RAND_MAX);
				Tmpvl *= (m_MaxVar[j] - m_MinVar[j]);
				Tmpvl += m_MinVar[j];
				m_FireFlys[i][j] = Tmpvl;
			}
			Tmpvl = m_Function(m_FireFlys[i], m_ObjPtr);
		} while ((Tmpvl > 1000000.0) || (!std::isfinite(Tmpvl)));
		m_LightIntensities[i] = Tmpvl;
		if (m_OptimizationType == eMin) {
			m_LightIntensities[i] = (fabs(m_LightIntensities[i]) >= FireFlysZeps) ? 1.0 / m_LightIntensities[i] : 1.0 / (m_LightIntensities[i] + FireFlysZeps);
		}
	}

	Sort();

	BestVal = m_LightIntensities[m_FireFlysNum - 1];
	m_Variables = m_FireFlys[m_FireFlysNum - 1];
	if (m_OptimizationType == eMin) {
		BestVal = (fabs(BestVal) > FireFlysZeps) ? 1.0 / BestVal : 1.0 / FireFlysZeps;
	}

	double Distance, Attractivness, GammaFactor = 1.0, RandomizationFactor = m_Alpha0;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0, 1.0);
	//std::normal_distribution<double> distribution(0.0, 1.0);
	m_iter = 0;

	if (m_OptimizationType == eMin) {
		m_MinSolVal = (fabs(m_MinSolVal) > FireFlysZeps) ? 1.0 / m_MinSolVal : 1.0 / FireFlysZeps;
	}

	std::cout << "\tFireflys optimization --> Iter #" << m_iter << "\tBest result: " << BestVal << "\n";

	std::vector<double> TmpVect;
	TmpVect.resize(m_Dim);
	do {

		for (int i = m_FireFlysNum - 1; i > 0; i--) {
			for (int j = i + 1; j < m_FireFlysNum; j++) {

				for (int k = 0; k < m_Dim; k++) {
					TmpVect[k] = m_FireFlys[i][k] - m_FireFlys[j][k];
				}

				Distance = Norm2(TmpVect);
				if (!std::isfinite(Distance)) {
					Distance = 1.0;
				}

				GammaFactor = pow((fabs(m_LightIntensities[i]) / (fabs(m_LightIntensities[j]) + FireFlysZeps)), (1.0 / (m_iter + 1)));
				if (!std::isfinite(GammaFactor)) {
					GammaFactor = 1.0;
				}

				Attractivness = m_Beta0 * exp(-m_Gamma*GammaFactor*Distance);
				for (int k = 0; k < m_Dim; k++) {

					m_FireFlys[i][k] += Attractivness * (m_FireFlys[j][k] - m_FireFlys[i][k]);
					//m_FireFlys[i][k] = Attractivness * m_FireFlys[j][k];
					//m_FireFlys[i][k] += (1.0 - Attractivness) * m_FireFlys[i][k];
					double chk = (distribution(generator));
					m_FireFlys[i][k] += (RandomizationFactor * ((distribution(generator) - 0.5)) * (m_MaxVar[k] - m_MinVar[k]) * 0.01);

					if (m_FireFlys[i][k] < m_MinVar[k]) {
						switch (m_BoundaryType)
						{
						case ePeriodic:
							m_FireFlys[i][k] = m_MaxVar[k] - fabs(m_MinVar[k] - m_FireFlys[i][k]);
							break;
						case eRefractive:
							m_FireFlys[i][k] = m_MinVar[k] + fabs(m_MinVar[k] - m_FireFlys[i][k]);
							break;
						case eRandom:
							m_FireFlys[i][k] = m_MinVar[k] + (distribution(generator) * (m_MaxVar[k] - m_MinVar[k]));
							break;
						case eTrim:
							m_FireFlys[i][k] = m_MinVar[k];
							break;
						case eNone:
							break;
						default:
							m_FireFlys[i][k] = m_MinVar[k] + (distribution(generator) * (m_MaxVar[k] - m_MinVar[k]));
							break;
						}
					}
					if (m_FireFlys[i][k] > m_MaxVar[k]) {
						switch (m_BoundaryType)
						{
						case ePeriodic:
							m_FireFlys[i][k] = m_MinVar[k] + fabs(m_FireFlys[i][k] - m_MaxVar[k]);
							break;
						case eRefractive:
							m_FireFlys[i][k] = m_MaxVar[k] - fabs(m_FireFlys[i][k] - m_MaxVar[k]);
							break;
						case eRandom:
							m_FireFlys[i][k] = m_MinVar[k] + (distribution(generator) * (m_MaxVar[k] - m_MinVar[k]));
							break;
						case eTrim:
							m_FireFlys[i][k] = m_MaxVar[k];
							break;
						case eNone:
							break;
						default:
							m_FireFlys[i][k] = m_MinVar[k] + (distribution(generator) * (m_MaxVar[k] - m_MinVar[k]));
							break;
						}
					}
				}

				if (m_OptimizationType == eMin) {
					m_LightIntensities[i] = (fabs(m_LightIntensities[i]) >= FireFlysZeps) ? 1.0 / m_LightIntensities[i] : 1.0 / (m_LightIntensities[i] + FireFlysZeps);
				}
			}
		}

		for (int k = 0; k < m_Dim; k++) {
			double chk = (distribution(generator));
			m_FireFlys[m_FireFlysNum - 1][k] += RandomizationFactor * ((distribution(generator) - 0.5)) * (m_MaxVar[k] - m_MinVar[k]) * 0.01;

			if (m_FireFlys[m_FireFlysNum - 1][k] < m_MinVar[k]) {
				switch (m_BoundaryType)
				{
				case ePeriodic:
					m_FireFlys[m_FireFlysNum - 1][k] = m_MaxVar[k] - fabs(m_MinVar[k] - m_FireFlys[m_FireFlysNum - 1][k]);
					break;
				case eRefractive:
					m_FireFlys[m_FireFlysNum - 1][k] = m_MinVar[k] + fabs(m_MinVar[k] - m_FireFlys[m_FireFlysNum - 1][k]);
					break;
				case eRandom:
					m_FireFlys[m_FireFlysNum - 1][k] = m_MinVar[k] + (distribution(generator) * (m_MaxVar[k] - m_MinVar[k]));
					break;
				case eTrim:
					m_FireFlys[m_FireFlysNum - 1][k] = m_MinVar[k];
					break;
				case eNone:
					break;
				default:
					m_FireFlys[m_FireFlysNum - 1][k] = m_MinVar[k] + (distribution(generator) * (m_MaxVar[k] - m_MinVar[k]));
					break;
				}
			}
			if (m_FireFlys[m_FireFlysNum - 1][k] > m_MaxVar[k]) {
				switch (m_BoundaryType)
				{
				case ePeriodic:
					m_FireFlys[m_FireFlysNum - 1][k] = m_MinVar[k] + fabs(m_FireFlys[m_FireFlysNum - 1][k] - m_MaxVar[k]);
					break;
				case eRefractive:
					m_FireFlys[m_FireFlysNum - 1][k] = m_MaxVar[k] - fabs(m_FireFlys[m_FireFlysNum - 1][k] - m_MaxVar[k]);
					break;
				case eRandom:
					m_FireFlys[m_FireFlysNum - 1][k] = m_MinVar[k] + (distribution(generator) * (m_MaxVar[k] - m_MinVar[k]));
					break;
				case eTrim:
					m_FireFlys[m_FireFlysNum - 1][k] = m_MaxVar[k];
					break;
				case eNone:
					break;
				default:
					m_FireFlys[m_FireFlysNum - 1][k] = m_MinVar[k] + (distribution(generator) * (m_MaxVar[k] - m_MinVar[k]));
					break;
				}
			}
		}

		for (int i = 0; i < m_FireFlysNum; i++) {
			Tmpvl = m_Function(m_FireFlys[i], m_ObjPtr);
			if (Tmpvl < 1.0) {
				Tmpvl *= 1.0;
			}
			if (!std::isfinite(Tmpvl)) {
				Tmpvl = 0.0;
			}
			if (m_OptimizationType == eMin) {
				m_LightIntensities[i] = (fabs(Tmpvl) >= FireFlysZeps) ? 1.0 / Tmpvl : 1.0 / (Tmpvl + FireFlysZeps);
			}
			else {
				m_LightIntensities[i] = Tmpvl;
			}
			if (!std::isfinite(m_LightIntensities[i])) {
				m_LightIntensities[i] = 1.0 / FireFlysZeps;
			}
		}

		Sort();

		BestVal = m_LightIntensities[m_FireFlysNum - 1];
		m_Variables = m_FireFlys[m_FireFlysNum - 1];
		if (m_OptimizationType == eMin) {
			BestVal = (fabs(BestVal) > FireFlysZeps) ? 1.0 / BestVal : 1.0 / FireFlysZeps;
		}

		RandomizationFactor *= m_Delta;
		m_iter++;

		std::cout << "\tFireflys optimization --> Iter #" << m_iter << "\tBest result: " << BestVal << "\n";
	} while ((m_LightIntensities[m_FireFlysNum - 1] < m_MinSolVal) && (m_iter < m_MaxIteration));

	if (m_OptimizationType == eMin) {
		m_MinSolVal = (fabs(m_MinSolVal) > FireFlysZeps) ? 1.0 / m_MinSolVal : 1.0 / FireFlysZeps;
	}

	m_Solution = BestVal;

	return true;
}

void FireFlysOptimization::GetResults(std::vector<std::vector<double>>& vResults)
{
	vResults.resize(m_FireFlysNum);
	for (int i = 0; i < m_FireFlysNum; ++i){
		/*vResults[i] = (m_FireFlys[i], m_Solution[i]); */
		vResults[i].resize(m_Dim + 1);
		for (int d = 0; d < m_Dim; ++d){
			vResults[i][d] = m_FireFlys[i][d];
		}
		vResults[i][m_Dim] = m_LightIntensities[i];
	}
}

void FireFlysOptimization::GetParams(double& Alpha, double& Beta0, double& Gamma, double& Delta)
{
	Alpha = m_Alpha0 * pow(m_Delta, m_iter);;
	Beta0 = m_Beta0;
	Gamma = m_Gamma;
	Delta = m_Delta;
}

void FireFlysOptimization::GetFireFlys(std::vector<std::vector<double>>& vFireFlys)
{
	vFireFlys = m_FireFlys;
}

void FireFlysOptimization::Sort()
{
	for (int i = 0; i < (m_FireFlysNum - 1); i++) {
		for (int j = i + 1; j < m_FireFlysNum; j++) {
			if (m_LightIntensities[j] < m_LightIntensities[i]) {
				double TmpVal = m_LightIntensities[j];
				m_LightIntensities[j] = m_LightIntensities[i];
				m_LightIntensities[i] = TmpVal;

				std::vector<double> TmpVect = m_FireFlys[j];
				m_FireFlys[j] = m_FireFlys[i];
				m_FireFlys[i] = TmpVect;
			}
		}
	}

}

double FireFlysOptimization::Norm2(std::vector<double> Vect)
{
	double VectNorm2 = 0.0;
	for (int i = 0; i < Vect.size(); i++)
		VectNorm2 += Vect[i] * Vect[i];

	return VectNorm2;
}
