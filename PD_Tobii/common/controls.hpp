#pragma once 

void computeMatricesFromInputs();
glm::mat4 getModelMatrix();
glm::mat4 getViewMatrix();
glm::mat4 getProjectionMatrix();

void setModelMatrix(const glm::mat4 &inputMatrix);
void setViewMatrix(const glm::mat4 &inputMatrix);
void setProjectionMatrix(const glm::mat4 &inputMatrix);

