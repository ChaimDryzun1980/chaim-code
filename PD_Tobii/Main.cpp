// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <math.h>
#include <time.h>
#include <windows.h>
#include <mutex>

#define WIN32_LEAN_AND_MEAN

//#include <winsock2.h>
//#include <Ws2tcpip.h>

// Link with ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")

#include <json/json.h>

// Include tobii headers
#include <tobii/tobii.h>
#include <tobii/tobii_streams.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>
using namespace glm;

#include "common/shader.hpp"
#include "common/texture.hpp"
#include "common/controls.hpp"
#include "common/objloader.hpp"
#include "common/vboindexer.hpp"
#include "common/text2D.hpp"

void gaze_origin_callback(tobii_gaze_origin_t const* gaze_origin, void* user_data)
{
	double* buffer = (double*)user_data;
	double PD = 0.0;
	if ((gaze_origin->left_validity == TOBII_VALIDITY_VALID) && (gaze_origin->right_validity == TOBII_VALIDITY_VALID))
	{
		double N_X_L, N_Y_L, N_Z_L, N_X_R, N_Y_R, N_Z_R;
		N_X_L = gaze_origin->left_xyz[0];
		N_Y_L = gaze_origin->left_xyz[1];
		N_Z_L = gaze_origin->left_xyz[2];

		N_X_R = gaze_origin->right_xyz[0];
		N_Y_R = gaze_origin->right_xyz[1];
		N_Z_R = gaze_origin->right_xyz[2];

		PD = sqrt(pow(N_X_R - N_X_L, 2.0) + pow(N_Y_R - N_Y_L, 2.0) + pow(N_Z_R - N_Z_L, 2.0));
	}
	buffer[0] = PD;
}


static void url_receiver(char const* url, void* user_data)
{
	char* buffer = (char*)user_data;
	if (*buffer != '\0') return; // only keep first value

	if (strlen(url) < 256)
		strcpy(buffer, url);
}

int main(int argc, char* argv[])
{
	printf("\n");
	printf("Reality+ Pupillary Distance Measurement\n");
	printf("Using Tobii Eye Tracker 4C\n");
	printf("Version 0.1.0 - 08.08.2019\n");
	printf("\n\n");

	// Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 16);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(1024, 768, "Reality+ Pupillary Distance Measurement", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	std::string InputJsonFile = "InputJSON.json";

	if (argc > 1)
		InputJsonFile = argv[1];

	Json::Value jsonData;
	std::ifstream test(InputJsonFile, std::ifstream::binary);
	Json::CharReaderBuilder jsonReader;
	JSONCPP_STRING errs;

	if (!Json::parseFromStream(jsonReader, test, &jsonData, &errs))
	{
		std::cout << "Could not parse input JSON" << std::endl;
		std::cout << errs << std::endl;
		system("pause");
		return 1;
	}
	else {
		std::cout << "finished parsing input JSON !!\n";
	}

	int Warmup = jsonData.get("INITIAL_WARMUP", 500).asInt();
	int Avrage = jsonData.get("INITIAL_AVERAGE", 500).asInt();

	int Br = jsonData.get("BACKGROUND_R", 0).asInt();
	int Bg = jsonData.get("BACKGROUND_G", 0).asInt();
	int Bb = jsonData.get("BACKGROUND_B", 102).asInt();

	int Ar = jsonData.get("AMBIANT_R", 76).asInt();
	int Ag = jsonData.get("AMBIANT_G", 76).asInt();
	int Ab = jsonData.get("AMBIANT_B", 25).asInt();

	int Sr = jsonData.get("SPECULAR_R", 128).asInt();
	int Sg = jsonData.get("SPECULAR_G", 128).asInt();
	int Sb = jsonData.get("SPECULAR_B", 128).asInt();

	int Dr = jsonData.get("DIFFUSE_R", 163).asInt();
	int Dg = jsonData.get("DIFFUSE_G", 122).asInt();
	int Db = jsonData.get("DIFFUSE_B", 82).asInt();

	int Lr = jsonData.get("LIGHT_R", 255).asInt();
	int Lg = jsonData.get("LIGHT_G", 255).asInt();
	int Lb = jsonData.get("LIGHT_B", 255).asInt();

	double Shns = jsonData.get("SHINNINESS", 96.0).asDouble();
	double Pwr = jsonData.get("LIGHT_POWER", 65.0).asDouble();

	double Mx_T = jsonData.get("MAX_TOLLERANCE", 10.0).asDouble();
	double Mn_T = jsonData.get("MIN_TOLLERANCE", 0.3).asDouble();
	double D_R = jsonData.get("DECAY_RATE", 0.0025).asDouble();
	bool m_bVerbose = jsonData.get("VERBOSE", true).asBool();
	
	int m_iPort = (unsigned short)jsonData.get("PORT", 2000).asInt();
	std::string m_sAddress = "192.168.1.136";

	std::string m_sLog = "Out.json";
	std::string m_sFragmentShader = "StandardShading.fragmentshader";
	std::string m_sVertexShader = "StandardShading.vertexshader";
	std::string m_sObject = "suzanne.obj";
	std::string m_sTexture = "uvmap.dds";

#ifndef _DEBUG
	{
		m_sLog = jsonData.get("LOG_FILE", "Out.json").asString();
		m_sFragmentShader = jsonData.get("FRAGMENT_SHADER", "StandardShading.fragmentshader").asString();
		m_sVertexShader = jsonData.get("VERTEX_SHADER", "StandardShading.vertexshader").asString();
		m_sObject = jsonData.get("OBJ_FILE", "suzanne.obj").asString();
		m_sTexture = jsonData.get("DDS_FILE", "uvmap.dds").asString();
		m_sAddress = jsonData.get("ADDRESS", "192.168.1.136").asString();
	}
#endif

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	// Hide the mouse and enable unlimited mouvement
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set the mouse at the center of the screen
	glfwPollEvents();
	glfwSetCursorPos(window, 1024 / 2, 768 / 2);

	float BckRed = ((float)(min(abs(Br), 255)) / 255.0);
	float BckBlu = ((float)(min(abs(Bb), 255)) / 255.0);
	float BckGrn = ((float)(min(abs(Bg), 255)) / 255.0);

	// Dark blue background
	glClearColor(BckRed, BckGrn, BckBlu, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	glEnable(GL_MULTISAMPLE);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders(m_sVertexShader.c_str(), m_sFragmentShader.c_str());

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");

	// Load the texture
	GLuint Texture = loadDDS(m_sTexture.c_str());

	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID = glGetUniformLocation(programID, "myTextureSampler");

	GLuint AlphaID = glGetUniformLocation(programID, "myAlpha");
	GLuint ShininessID = glGetUniformLocation(programID, "MaterialShininess");
	GLuint LightPowerID = glGetUniformLocation(programID, "LightPower");

	// Read our .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	bool res = loadOBJ(m_sObject.c_str(), vertices, uvs, normals);

	std::vector<unsigned short> indices;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;
	indexVBO(vertices, uvs, normals, indices, indexed_vertices, indexed_uvs, indexed_normals);

	// Load it into a VBO

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

	GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);

	GLuint normalbuffer;
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

	// Get a handle for our "LightPosition" uniform
	glUseProgram(programID);
	GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");

	GLuint LightColorID = glGetUniformLocation(programID, "LightColor");

	GLuint MaterialAmbientID = glGetUniformLocation(programID, "MaterialAmbient");
	GLuint MaterialSpecularID = glGetUniformLocation(programID, "MaterialSpecular");
	GLuint MaterialDiffuseID = glGetUniformLocation(programID, "MaterialDiffuse");

	float AmRed = ((float)(min(abs(Ar), 255)) / 255.0);
	float AmBlu = ((float)(min(abs(Ab), 255)) / 255.0);
	float AmGrn = ((float)(min(abs(Ag), 255)) / 255.0);
	glm::vec3 MatAmbt = glm::vec3(AmRed, AmGrn, AmBlu);

	float SpRed = ((float)(min(abs(Ar), 255)) / 255.0);
	float SpBlu = ((float)(min(abs(Ab), 255)) / 255.0);
	float SpGrn = ((float)(min(abs(Ag), 255)) / 255.0);
	glm::vec3 MatSpclr = glm::vec3(SpRed, SpGrn, SpBlu);

	float DfRed = ((float)(min(abs(Dr), 255)) / 255.0);
	float DfBlu = ((float)(min(abs(Db), 255)) / 255.0);
	float DfGrn = ((float)(min(abs(Dg), 255)) / 255.0);
	glm::vec3 MatDffs = glm::vec3(DfRed, DfGrn, DfBlu);

	float LgRed = ((float)(min(abs(Lr), 255)) / 255.0);
	float LgBlu = ((float)(min(abs(Lb), 255)) / 255.0);
	float LgGrn = ((float)(min(abs(Lg), 255)) / 255.0);
	glm::vec3 lightClr = glm::vec3(LgRed, LgGrn, LgBlu);
	glm::vec3 lightPos = glm::vec3(4.0, 4.0, 4.0);

	// For speed computation
	double lastTime = glfwGetTime();
	int nbFrames = 0;

	// Initialize our little text library with the Holstein font
	initText2D("Holstein.DDS");

	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	tobii_api_t* api;
	tobii_error_t error = tobii_api_create(&api, NULL, NULL);
	assert(error == TOBII_ERROR_NO_ERROR);

	char url[256] = { 0 };
	error = tobii_enumerate_local_device_urls(api, url_receiver, url);
	assert(error == TOBII_ERROR_NO_ERROR && *url != '\0');

	tobii_device_t* device;
	error = tobii_device_create(api, url, &device);
	assert(error == TOBII_ERROR_NO_ERROR);

	double GazePD_Data[1];
	error = tobii_gaze_origin_subscribe(device, gaze_origin_callback, GazePD_Data);
	assert(error == TOBII_ERROR_NO_ERROR);

	double GazePD;

	int Counter = 0, StartIndex = max(abs(Warmup), 250), AverageNumber = max(abs(Avrage), 250), EndCount = 2100000000;
	double AvGazePD = 0.0, PrevGazePD = 0.0, MaxTollerance = fabs(Mx_T), MinTollerance = fabs(Mn_T), DecayRate = fabs(D_R);

	std::string Msg = "Follow the monkey";

	glm::vec3 position = glm::vec3(0, 0, 0);
	glm::vec3 direction = glm::vec3(0, 0, -1.0);
	glm::vec3 up = glm::vec3(0, 1.0, 0.0);


	glm::vec3 MonkeyPositions[10] = {
		glm::vec3(0.0f, 0.0f, -5.0f),
		glm::vec3(0.0f, 1.7f, -5.0f),
		glm::vec3(-2.1f, -1.7f, -5.0f),
		glm::vec3(2.1f, -1.7f, -5.0f),
		glm::vec3(2.1f, 1.7f, -5.0f),
		glm::vec3(-2.1f, 1.7f, -5.0f),
		glm::vec3(0.0f, -1.7f, -5.0f),
		glm::vec3(2.1f, 0.0f, -5.0f),
		glm::vec3(-2.1f, 0.0f, -5.0f),
		glm::vec3(0.0f, 0.0f, -5.0f),
	};

	int MonkeyIndex = 0;
	float MonkeyWeight = 1.0;

	glm::vec3 gCntrlPosition1 = MonkeyPositions[MonkeyIndex];
	glm::vec3 gCntrlOrientation1(0.0, 0.0, 0.0);
	glm::vec3 gCntrlScale1(0.2f, 0.2f, 0.22f);

	float FoV = 45.0f;
	int width = 1024;
	int height = 768;
	glfwGetWindowSize(window, &width, &height);

	// Build the model matrix
	glm::mat4 ScalingMatrix = scale(mat4(), gCntrlScale1);

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 1000 units
	glm::mat4 ProjectionMatrix = glm::perspective(glm::radians(FoV), (float)width / (float)height, 0.05f, 1000.0f);

	// Camera matrix
	glm::mat4 ViewMatrix = glm::lookAt(
		position,           // Camera is here
		position + direction, // and looks here : at the same position, plus "direction"
		up                  // Head is up (set to 0,-1,0 to look upside-down)
	);

	float Angle = 0.0;
	float Sign = 1.0;

	Json::Value JsonValues;
	
	std::string TmpFile = "Tmp.txt";
	std::string document;
	std::ofstream file_id;
	file_id.open(TmpFile, std::ofstream::out | std::ofstream::trunc);

	int iResult;
	WSADATA wsaData;

	SOCKET SendSocket = INVALID_SOCKET;
	sockaddr_in RecvAddr;

	//char SendBuf[1024];
	int BufLen = 1024;

	//----------------------
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		wprintf(L"WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	//---------------------------------------------
	// Create a socket for sending data
	SendSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (SendSocket == INVALID_SOCKET) {
		wprintf(L"socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Set up the RecvAddr structure with the IP address of
	// the receiver (in this example case "192.168.1.1")
	// and the specified port number.
	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_port = htons(m_iPort);
	RecvAddr.sin_addr.s_addr = inet_addr(m_sAddress.c_str());

	bool EndCalibration = false;

	do {
		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		// Compute the MVP matrix
		float InvMonkW = 1.0 - MonkeyWeight;
		gCntrlPosition1.x = (MonkeyWeight * MonkeyPositions[MonkeyIndex].x)+ (InvMonkW * MonkeyPositions[(MonkeyIndex + 1)%10].x);
		gCntrlPosition1.y = (MonkeyWeight * MonkeyPositions[MonkeyIndex].y) + (InvMonkW * MonkeyPositions[(MonkeyIndex + 1) % 10].y);
		gCntrlPosition1.z = (MonkeyWeight * MonkeyPositions[MonkeyIndex].z) + (InvMonkW * MonkeyPositions[(MonkeyIndex + 1) % 10].z);
		gCntrlOrientation1.y = Angle;
		glm::mat4 TranslationMatrix = translate(mat4(), gCntrlPosition1); // A bit to the left
		glm::mat4 RotationMatrix = eulerAngleYXZ(gCntrlOrientation1.y, gCntrlOrientation1.x, gCntrlOrientation1.z);
		glm::mat4 ModelMatrix = TranslationMatrix * RotationMatrix * ScalingMatrix;
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);

		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);
		glUniform3f(LightColorID, lightClr.x, lightClr.y, lightClr.z);
		glUniform1f(LightPowerID, (float)(fabs(Pwr)));

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture);
		// Set our "myTextureSampler" sampler to use Texture Unit 0
		glUniform1i(TextureID, 0);

		glUniform1f(AlphaID, 1.0);
				
		glUniform3f(MaterialAmbientID, MatAmbt.x, MatAmbt.y, MatAmbt.z);
		glUniform3f(MaterialSpecularID, MatSpclr.x, MatSpclr.y, MatSpclr.z);
		glUniform3f(MaterialDiffuseID, MatDffs.x, MatDffs.y, MatDffs.z);
		glUniform1f(ShininessID, (float)(fabs(Shns)));

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			indices.size(),    // count
			GL_UNSIGNED_SHORT,   // type
			(void*)0           // element array buffer offset
		);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);

		int MsgSize = floor((int)Msg.length() / 2);
		int Start = 400 - (MsgSize * 30);
		if (Start <= 0)
			Start = 10;

		printText2D(Msg.c_str(), Start, 395, 30);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

		error = tobii_wait_for_callbacks(NULL, 1, &device);
		assert(error == TOBII_ERROR_NO_ERROR || error == TOBII_ERROR_TIMED_OUT);

		error = tobii_device_process_callbacks(device);
		assert(error == TOBII_ERROR_NO_ERROR);

		GazePD = GazePD_Data[0];

		if (GazePD > 1.0)
		{
			document = std::to_string(GazePD);
			file_id << document << "\n";

			if (Counter == EndCount)
			{
				Counter = (StartIndex + AverageNumber + 1);
			}

			double Prec = ((double)Counter / (double)(StartIndex + AverageNumber)) * 100.0;

			MonkeyIndex = floor(Prec / 10.0);
			MonkeyWeight = 1.0 - ((Prec - ((double)MonkeyIndex *10.0)) / 10.0);


			if (Counter < StartIndex)
			{
				printf("Calculating PD - Please wait: %3.2f%% Ready        \r", Prec);
				char TmpTxt[256];
				sprintf(TmpTxt, "%3.2f%% Ready", Prec);
				Msg = TmpTxt;				
			}
			else
			{
				double Tollerance = ((MaxTollerance - MinTollerance) * exp(-DecayRate * (double)(Counter - StartIndex))) + MinTollerance;

				if ((fabs(AvGazePD - GazePD) < Tollerance) || (AvGazePD < 20.0))
				{
					AvGazePD = (AvGazePD *((double)(Counter - StartIndex)) + GazePD) / ((double)(Counter - StartIndex + 1));
				}
				else
					Counter--;

				PrevGazePD = AvGazePD;

				if (Counter < (StartIndex + AverageNumber))
				{
					printf("Calculating PD - Please wait: %3.2f%% Ready               \r", Prec);
					char TmpTxt[256];
					sprintf(TmpTxt, "%3.2f%% Ready", Prec);
					Msg = TmpTxt;					
				}
				else
				{
					printf("PD : %f                                                        \r", AvGazePD);
					char TmpTxt[256];
					sprintf(TmpTxt, "PD: %.2f", AvGazePD);
					Msg = TmpTxt;					
					EndCalibration = true;
				}
			}
		}
		else
		{
			if (PrevGazePD < 20.0)
			{
				Counter = 0;
				printf("ERROR: PD could not be calculated !                                \r");
				Msg = "Follow the monkey";
			}
			else
			{
				printf("PD : %f                                                          \r", PrevGazePD);
				char TmpTxt[256];
				sprintf(TmpTxt, "PD: %.2f", PrevGazePD);
				Msg = TmpTxt;
			}

			AvGazePD = PrevGazePD;
		}

		Angle += Sign*0.05;
		Counter++;


	} // Check if the ESC key was pressed or the window was closed
	while (!EndCalibration && glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0);

	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteBuffers(1, &normalbuffer);
	glDeleteBuffers(1, &elementbuffer);
	glDeleteProgram(programID);
	glDeleteTextures(1, &Texture);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Delete the text's VBO, the shader and the texture
	cleanupText2D();

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	file_id.close();

	JsonValues["opacity_PD_for_lcd_cm"] = (double)(AvGazePD * 0.1);

	Json::StreamWriterBuilder wbuilder;
	wbuilder.settings_["commentStyle"] = "None";
	wbuilder.settings_["indentation"] = "";

#ifdef _DEBUG
	{
		document = "{\"opacity_PD_for_lcd_cm\":" + std::to_string(AvGazePD.x) + "}";
	}
#endif
#ifndef _DEBUG
	{
		document = Json::writeString(wbuilder, JsonValues);
	}
#endif

	//---------------------------------------------
	// Send a datagram to the receiver
	iResult = sendto(SendSocket,
		document.c_str(), (int)document.length(), 0, (SOCKADDR *)& RecvAddr, sizeof(RecvAddr));
	if (iResult == SOCKET_ERROR) {
		wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
		closesocket(SendSocket);
		WSACleanup();
		return 1;
	}

	//---------------------------------------------
	// When the application is finished sending, close the socket.
	iResult = closesocket(SendSocket);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"closesocket failed with error: %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Clean up and quit.
	WSACleanup();

	std::ofstream file_idA;
	file_idA.open(m_sLog, std::ofstream::out | std::ofstream::trunc);
	file_idA << document << "\n";
	file_idA.close();


	return 0;
}

