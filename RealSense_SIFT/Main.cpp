#include "CommonDefenitions.h"
#include "Conversions.h"
#include "example.h"

#define WIN32_LEAN_AND_MEAN

#include <winsock2.h>
#include <Ws2tcpip.h>

// Link with ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")

#include <stdio.h>
#include <windows.h>
#include <mutex>
#include <algorithm>

#include <json/json.h>

#include <Eigen/Dense>

#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

// Get RGB values based on normals - texcoords, normals value [u v]
std::tuple<uint8_t, uint8_t, uint8_t> get_texcolor(rs2::video_frame texture, rs2::texture_coordinate texcoords)
{
	const int w = texture.get_width(), h = texture.get_height();

	// convert normals [u v] to basic coords [x y]
	int x = std::min(std::max(int(texcoords.u*w + .5f), 0), w - 1);
	int y = std::min(std::max(int(texcoords.v*h + .5f), 0), h - 1);

	int idx = x * texture.get_bytes_per_pixel() + y * texture.get_stride_in_bytes();
	const auto texture_data = reinterpret_cast<const uint8_t*>(texture.get_data());
	return std::tuple<uint8_t, uint8_t, uint8_t>(texture_data[idx], texture_data[idx + 1], texture_data[idx + 2]);
}


CloudXYZRGB_Ptr points_to_pcl(const rs2::points& points, const rs2::video_frame& color) {

	//// OpenCV Mat for showing the rgb color image, just as part of processing
	//Mat colorr(Size(640, 480), CV_8UC3, (void*)color.get_data(), Mat::AUTO_STEP);
	//namedWindow("Display Image", WINDOW_AUTOSIZE);
	//imshow("Display Image", colorr);

	auto sp = points.get_profile().as<rs2::video_stream_profile>();
	CloudXYZRGB_Ptr cloud(new CloudXYZRGB);

	// Config of PCL Cloud object
	cloud->width = static_cast<uint32_t>(sp.width());
	cloud->height = static_cast<uint32_t>(sp.height());
	cloud->is_dense = false;
	cloud->points.resize(points.size());

	auto tex_coords = points.get_texture_coordinates();
	auto vertices = points.get_vertices();

	// Iterating through all points and setting XYZ coordinates
	// and RGB values
	for (int i = 0; i < points.size(); ++i)
	{
		cloud->points[i].x = vertices[i].x;
		cloud->points[i].y = -vertices[i].y;
		cloud->points[i].z = vertices[i].z;

		std::tuple<uint8_t, uint8_t, uint8_t> current_color;
		current_color = get_texcolor(color, tex_coords[i]);

		// Reversed order- 2-1-0 because of BGR model used in camera
		cloud->points[i].r = std::get<2>(current_color);
		cloud->points[i].g = std::get<1>(current_color);
		cloud->points[i].b = std::get<0>(current_color);
	}

	return cloud;
}

class rotation_estimator
{
	// theta is the angle of camera rotation in x, y and z components
	float3 theta;
	std::mutex theta_mtx;
	
	/* alpha indicates the part that gyro and accelerometer take in computation of theta; higher alpha gives more weight to gyro, but too high
	values cause drift; lower alpha gives more weight to accelerometer, which is more sensitive to disturbances */
	float alpha = 0.98;
	bool first = true;
	// Keeps the arrival time of previous gyro frame
	double last_ts_gyro = 0;
public:
	// Function to calculate the change in angle of motion based on data from gyro
	void process_gyro(rs2_vector gyro_data, double ts)
	{
		if (first) // On the first iteration, use only data from accelerometer to set the camera's initial position
		{
			last_ts_gyro = ts;
			return;
		}
		// Holds the change in angle, as calculated from gyro
		float3 gyro_angle;

		// Initialize gyro_angle with data from gyro
		gyro_angle.x = gyro_data.x; // Pitch
		gyro_angle.y = gyro_data.y; // Yaw
		gyro_angle.z = gyro_data.z; // Roll

		// Compute the difference between arrival times of previous and current gyro frames
		double dt_gyro = (ts - last_ts_gyro) / 1000.0;
		last_ts_gyro = ts;

		// Change in angle equals gyro measures * time passed since last measurement
		gyro_angle = gyro_angle * dt_gyro;

		// Apply the calculated change of angle to the current angle (theta)
		std::lock_guard<std::mutex> lock(theta_mtx);
		theta.add(-gyro_angle.z, -gyro_angle.y, gyro_angle.x);
	}

	void process_accel(rs2_vector accel_data)
	{
		// Holds the angle as calculated from accelerometer data
		float3 accel_angle;

		// Calculate rotation angle from accelerometer data
		accel_angle.z = atan2(accel_data.y, accel_data.z);
		accel_angle.x = atan2(accel_data.x, sqrt(accel_data.y * accel_data.y + accel_data.z * accel_data.z));

		// If it is the first iteration, set initial pose of camera according to accelerometer data (note the different handling for Y axis)
		std::lock_guard<std::mutex> lock(theta_mtx);
		if (first)
		{
			first = false;
			theta = accel_angle;
			// Since we can't infer the angle around Y axis using accelerometer data, we'll use PI as a convetion for the initial pose
			theta.y = PI;
		}
		else
		{
			/*
			Apply Complementary Filter:
				- high-pass filter = theta * alpha:  allows short-duration signals to pass through while filtering out signals
				  that are steady over time, is used to cancel out drift.
				- low-pass filter = accel * (1- alpha): lets through long term changes, filtering out short term fluctuations
			*/
			theta.x = theta.x * alpha + accel_angle.x * (1 - alpha);
			theta.z = theta.z * alpha + accel_angle.z * (1 - alpha);
		}
	}

	// Returns the current rotation angle
	float3 get_theta()
	{
		std::lock_guard<std::mutex> lock(theta_mtx);
		return theta;
	}
};

bool check_imu_is_supported()
{
	bool found_gyro = false;
	bool found_accel = false;
	rs2::context ctx;
	for (auto dev : ctx.query_devices())
	{
		// The same device should support gyro and accel
		found_gyro = false;
		found_accel = false;
		for (auto sensor : dev.query_sensors())
		{
			for (auto profile : sensor.get_stream_profiles())
			{
				if (profile.stream_type() == RS2_STREAM_GYRO)
					found_gyro = true;

				if (profile.stream_type() == RS2_STREAM_ACCEL)
					found_accel = true;
			}
		}
		if (found_gyro && found_accel)
			break;
	}
	return found_gyro && found_accel;
}

int main(int argc, char* argv[])
{

	if (!check_imu_is_supported())
	{
		std::cerr << "Device supporting IMU (D435i) not found";
		return EXIT_FAILURE;
	}
	else {
		std::cout << "Device supporting IMU(D435i) found !!\n";
	}

	std::string InputJsonFile = "InputJSON.json";

	if (argc > 1)
		InputJsonFile = argv[1];

	Json::Value jsonData;
	std::ifstream test(InputJsonFile, std::ifstream::binary);
	Json::CharReaderBuilder jsonReader;
	JSONCPP_STRING errs;

	if (!Json::parseFromStream(jsonReader, test, &jsonData, &errs))
	{
		std::cout << "Could not parse input JSON" << std::endl;
		std::cout << errs << std::endl;
		system("pause");
		return 1;
	}
	else {
		std::cout << "finished parsing input JSON !!\n";
	}


	//int m_iTrackMethod = (unsigned short)jsonData.get("TRACKING_METHOD", 6).asInt();
	int Warmup = jsonData.get("INITIAL_WARMUP", 10).asInt();
	double Match_Max = jsonData.get("MATCH_MAX", 0.05).asDouble();
	bool m_bVerbose = jsonData.get("VERBOSE", true).asBool();
	bool m_bDisplay = jsonData.get("SHOW_IMAGE", true).asBool();
	int m_iPort = (unsigned short)jsonData.get("PORT", 2000).asInt();
	std::string m_sLog = "Out.json";
	std::string m_sAddress = "192.168.1.136";

#ifndef _DEBUG
	{
		m_sLog = jsonData.get("LOG_FILE", "Out.json").asString();
		m_sAddress = jsonData.get("ADDRESS", "192.168.1.136").asString();
	}
#endif
	
	Warmup = abs(Warmup);
	if (Warmup > 100)
		Warmup = 100;
	if (Warmup < 1)
		Warmup = 1;

	if ((Match_Max < 0.00001) || (Match_Max > 0.9999999))
	{
		Match_Max = 0.05;
	}
	
	// --Step 1: Detect the keypoints using SURF Detector
	int minHessian = 400;

	cv::Ptr<cv::xfeatures2d::SURF> detector = cv::xfeatures2d::SURF::create(minHessian);
	//cv::Ptr<cv::xfeatures2d::SIFT> detector = cv::xfeatures2d::SIFT::create();
	std::vector<cv::KeyPoint> keypoints, keypoints_Prev;
	cv::Mat descriptors, descriptors_Prev;
	cv::Mat Output, color_image_Prev;

	Eigen::Vector3d DeltaPosition, DeltaPosition_Prev;
	DeltaPosition = Eigen::Vector3d(0.0, 0.0, 0.0);
	DeltaPosition_Prev = Eigen::Vector3d(0.0, 0.0, 0.0);
	
	Eigen::Matrix3d Rotation, Rotation_Prev;
	Rotation.setIdentity();
	Rotation_Prev.setIdentity();

	cv::FlannBasedMatcher matcher;
	std::vector< cv::DMatch > matches;
	
	Json::Value JsonValues;
	
	int iResult;
	WSADATA wsaData;

	SOCKET SendSocket = INVALID_SOCKET;
	sockaddr_in RecvAddr;

	//char SendBuf[1024];
	int BufLen = 1024;

	std::ofstream file_id;
	file_id.open(m_sLog, std::ofstream::out | std::ofstream::trunc);

	//----------------------
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		wprintf(L"WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	//---------------------------------------------
	// Create a socket for sending data
	SendSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (SendSocket == INVALID_SOCKET) {
		wprintf(L"socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Set up the RecvAddr structure with the IP address of
	// the receiver (in this example case "192.168.1.1")
	// and the specified port number.
	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_port = htons(m_iPort);
	RecvAddr.sin_addr.s_addr = inet_addr(m_sAddress.c_str());

	clock_t tm;

	if (m_bDisplay)
	{
		cv::namedWindow("Color", cv::WINDOW_NORMAL);
		cv::namedWindow("Depth", cv::WINDOW_NORMAL);
	}

	// Declare pointcloud object, for calculating pointclouds and texture mappings
	rs2::pointcloud pc;

	rs2::align align_to_color(RS2_STREAM_COLOR);
	rs2::align align_to_depth(RS2_STREAM_DEPTH);

	rs2::decimation_filter dec;
	// If the demo is too slow, make sure you run in Release (-DCMAKE_BUILD_TYPE=Release)
	// but you can also increase the following parameter to decimate depth more (reducing quality)
	dec.set_option(RS2_OPTION_FILTER_MAGNITUDE, 2);
	// Define transformations from and to Disparity domain
	rs2::disparity_transform depth2disparity;
	rs2::disparity_transform disparity2depth(false);
	// Define spatial filter (edge-preserving)
	rs2::spatial_filter spat;
	// Enable hole-filling
	// Hole filling is an agressive heuristic and it gets the depth wrong many times
	// However, this demo is not built to handle holes
	// (the shortest-path will always prefer to "cut" through the holes since they have zero 3D distance)
	spat.set_option(RS2_OPTION_HOLES_FILL, 5); // 5 = fill all the zero pixels
	// Define temporal filter
	rs2::temporal_filter temp;

	// Declare RealSense pipeline, encapsulating the actual device and sensors
	rs2::pipeline pipe;

	rs2::config cfg;
	cfg.enable_stream(RS2_STREAM_INFRARED, 640, 480, RS2_FORMAT_Y8, 30);
	cfg.enable_stream(RS2_STREAM_DEPTH, 640, 480, RS2_FORMAT_Z16, 30);
	cfg.enable_stream(RS2_STREAM_COLOR, 640, 480, RS2_FORMAT_BGR8, 30);
	cfg.enable_stream(RS2_STREAM_ACCEL, RS2_FORMAT_MOTION_XYZ32F);
	cfg.enable_stream(RS2_STREAM_GYRO, RS2_FORMAT_MOTION_XYZ32F);

	auto profile = pipe.start(cfg);

	auto sensor = profile.get_device().first<rs2::depth_sensor>();

	// Set the device to High Accuracy preset of the D400 stereoscopic cameras
	if (sensor && sensor.is<rs2::depth_stereo_sensor>())
	{
		//sensor.set_option(RS2_OPTION_VISUAL_PRESET, RS2_RS400_VISUAL_PRESET_HIGH_ACCURACY);
		sensor.set_option(RS2_OPTION_VISUAL_PRESET, RS2_RS400_VISUAL_PRESET_HIGH_DENSITY);
	}

	auto stream = profile.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();
	auto intr = stream.get_intrinsics();

	// Declare object that handles camera pose calculations
	rotation_estimator algo;
	float3 theta, thetaPrev, DeltaTheta;
	
	// Camera warmup - dropping several first frames to let auto-exposure stabilize
	for (int i = 0; i < Warmup; i++)
	{
		// Wait for all configured streams to produce a frame
		auto frames = pipe.wait_for_frames();
	}

	bool IsFirst = true;
	
	for (;;)
	{
		tm = clock();

		rs2::frameset data = pipe.wait_for_frames();
		//rs2::frameset aligned_set = align_to_depth .process(data);
		rs2::frameset aligned_set = align_to_color.process(data);

		// Decimation will reduce the resultion of the depth image,
			// closing small holes and speeding-up the algorithm
		//aligned_set = aligned_set.apply_filter(dec);

		// To make sure far-away objects are filtered proportionally
		// we try to switch to disparity domain
		aligned_set = aligned_set.apply_filter(depth2disparity);

		// Apply spatial filtering
		aligned_set = aligned_set.apply_filter(spat);

		// Apply temporal filtering
		aligned_set = aligned_set.apply_filter(temp);

		// If we are in disparity domain, switch back to depth
		aligned_set = aligned_set.apply_filter(disparity2depth);

		auto depth = aligned_set.get_depth_frame();
		auto colored_frame = aligned_set.get_color_frame();

		// Cast the frame that arrived to motion frame
		//auto motion = aligned_set.as<rs2::motion_frame>();
		//auto motion = data.as<rs2::motion_frame>();
		
		// If casting succeeded and the arrived frame is from gyro stream
		//if (motion && motion.get_profile().stream_type() == RS2_STREAM_GYRO && motion.get_profile().format() == RS2_FORMAT_MOTION_XYZ32F)
		if (rs2::motion_frame gyro_frame = data.first_or_default(RS2_STREAM_GYRO))
		{
			//std::cout << "\tGyro\n";

			// Get the timestamp of the current frame
			//double ts = motion.get_timestamp();
			double ts = gyro_frame.get_timestamp();
			
			// Get gyro measures
			//rs2_vector gyro_data = motion.get_motion_data();
			rs2_vector gyro_data = gyro_frame.get_motion_data();

			// Call function that computes the angle of motion based on the retrieved measures
			algo.process_gyro(gyro_data, ts);
		}
		// If casting succeeded and the arrived frame is from accelerometer stream
		//if (motion && motion.get_profile().stream_type() == RS2_STREAM_ACCEL && motion.get_profile().format() == RS2_FORMAT_MOTION_XYZ32F)
		if (rs2::motion_frame accel_frame = data.first_or_default(RS2_STREAM_ACCEL))
		{
			//std::cout << "\tAccelometer\n";
			
			// Get accelerometer measures
			//rs2_vector accel_data = motion.get_motion_data();
			rs2_vector accel_data = accel_frame.get_motion_data();
			
			// Call function that computes the angle of motion based on the retrieved measures
			algo.process_accel(accel_data);
		}

		// If we only received new depth frame, 
		// but the color did not update, continue
		static int last_frame_number = 0;
		if (colored_frame.get_frame_number() == last_frame_number) continue;
		last_frame_number = colored_frame.get_frame_number();

		// Creating OpenCV Matrix from a color image
		auto color_image = Conversions::frame_to_mat(colored_frame);
		auto depth_mat = Conversions::depth_frame_to_meters(pipe, depth);

		detector->detectAndCompute(color_image, cv::Mat(), keypoints, descriptors);
		//cv::drawKeypoints(color_image, keypoints, Output, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT);

		theta = algo.get_theta();

		if (IsFirst)
		{
			IsFirst = false;
			descriptors_Prev = descriptors.clone();
			color_image_Prev = color_image.clone();
			keypoints_Prev = keypoints;
			thetaPrev = theta;
			DeltaPosition_Prev = DeltaPosition;
			Rotation = Rotation_Prev;
			continue;
		}

		DeltaTheta.x = theta.x - thetaPrev.x;
		if (fabs(DeltaTheta.x) < 0.00000001)
			DeltaTheta.x = 0.0;
		DeltaTheta.y = theta.y - thetaPrev.y;
		if (fabs(DeltaTheta.y) < 0.00000001)
			DeltaTheta.y = 0.0;
		DeltaTheta.z = theta.z - thetaPrev.z;
		if (fabs(DeltaTheta.z) < 0.00000001)
			DeltaTheta.z = 0.0;

		//std::cout << "\r\t Delta Theta = " << std::fixed << std::setprecision(6) << DeltaTheta.x << ", " << DeltaTheta.y << ", " << DeltaTheta.z << std::flush;
		Eigen::Matrix3d Rotation_IMU;
		Rotation_IMU = Eigen::AngleAxisd(DeltaTheta.x, Eigen::Vector3d::UnitZ())
			* Eigen::AngleAxisd(DeltaTheta.y, Eigen::Vector3d::UnitY())
			* Eigen::AngleAxisd(DeltaTheta.z, Eigen::Vector3d::UnitZ());

		//std::cout << "IMU rotation:" << std::endl;
		//std::cout << Rotation_IMU << std::endl << std::endl;

		matcher.match(descriptors, descriptors_Prev, matches);

		double min_dist = 100000;
		
		//-- Quick calculation of max and min distances between keypoints
		for (int i = 0; i < descriptors.rows; i++)
		{
			double dist = matches[i].distance;
			if (dist < min_dist) min_dist = dist;
		}

		//if (m_bVerbose)
		//	printf("\t-- Min dist : %f \n", min_dist);
		
		//-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
		//-- or a small arbitary value ( 0.02 ) in the event that min_dist is very
		//-- small)
		//-- PS.- radiusMatch can also be used here.
		std::vector< cv::DMatch > good_matches;
		for (int i = 0; i < descriptors.rows; i++)
		{
			if (matches[i].distance <= std::max(2.0 * min_dist, Match_Max))
			{
				good_matches.push_back(matches[i]);
			}
		}

		int good_matches_size = (int)good_matches.size();
		std::vector<bool> IsUsed(good_matches_size, false);

		std::vector<std::vector<int>> Groups;

		double Dist_Limit = 0.7;

		for (int i = 0; i < good_matches_size - 1; i++)
		{
			if (IsUsed[i])
				continue;

			IsUsed[i] = true;
			std::vector<int> Group_i;
			Group_i.push_back(i);

			double Dist_i = cv::norm(keypoints[good_matches[i].queryIdx].pt - keypoints_Prev[good_matches[i].trainIdx].pt);

			for (int j = i + 1; j < good_matches_size; j++)
			{
				if (IsUsed[j])
					continue;

				double Dist_j = cv::norm(keypoints[good_matches[j].queryIdx].pt - keypoints_Prev[good_matches[j].trainIdx].pt);

				if (std::fabs(Dist_i- Dist_j) < Dist_Limit)
				{
					IsUsed[j] = true;
					Group_i.push_back(j);
				}
			}
			Groups.push_back(Group_i);
		}

		//std::cout << "\t--There are " << Groups.size() << " Groups:\n";
		double BestScore = 1000000.0;
		int BestIndex = -1;

		for (int i = 0; i < Groups.size(); i++)
		{
			if (Groups[i].size() < 4)
				continue;

			//std::cout << "\t\tGroup " << i << ": ";
	
			std::vector< Eigen::Vector3d> Source;
			std::vector< Eigen::Vector3d> Target;

			for (int j = 0; j < Groups[i].size(); j++)
			{
				//std::cout << Groups[i][j] << " ";

				// Deproject from pixel to point in 3D
				float upixel[2]; // From pixel
				float upoint[3]; // From point (in 3D)

				float vpixel[2]; // To pixel
				float vpoint[3]; // To point (in 3D)

				// Copy pixels into the arrays (to match rsutil signatures)
				upixel[0] = keypoints[good_matches[i].queryIdx].pt.x;
				upixel[1] = keypoints[good_matches[i].queryIdx].pt.y;

				vpixel[0] = keypoints_Prev[good_matches[i].trainIdx].pt.x;
				vpixel[1] = keypoints_Prev[good_matches[i].trainIdx].pt.y;

				// Query the frame for distance
				// Note: this can be optimized
				// It is not recommended to issue an API call for each pixel
				// (since the compiler can't inline these)
				// However, in this example it is not one of the bottlenecks

				auto udist = depth.get_distance(upixel[0], upixel[1]);
				auto vdist = depth.get_distance(vpixel[0], vpixel[1]);

				// Deproject from pixel to point in 3D
				rs2_deproject_pixel_to_point(upoint, &intr, upixel, udist);
				rs2_deproject_pixel_to_point(vpoint, &intr, vpixel, vdist);

				double N1 = 0.0, N2 = 0.0;
				for (int i = 0; i < 3; i++)
				{
					N1 += std::pow(upoint[i], 2.0);
					N2 += std::pow(vpoint[i], 2.0);
				}

				if ((N1 < 0.0000001) || (N2 < 0.0000001))
					continue;

				Source.push_back(Eigen::Vector3d(upoint[0], upoint[1], upoint[2]));
				Target.push_back(Eigen::Vector3d(vpoint[0], vpoint[1], vpoint[2]));
			}
			//std::cout << "\n";

			if ((Source.size() < 4) || (Target.size() < 4))
				continue;
			
			Eigen::MatrixX3d in;
			Eigen::MatrixX3d out;

			in.resize(Source.size(), 3);
			out.resize(Target.size(), 3);

			for (int i = 0; i < Source.size(); i++)
			{
				in.row(i) = Source[i];
				out.row(i) = Target[i];
			}

			Eigen::Vector3d in_ctr = in.colwise().mean();
			Eigen::Vector3d out_ctr = out.colwise().mean();

			for (int i = 0; i < 3; i++)
			{
				in.row(i) = in.row(i) - (in_ctr.transpose().eval());
				out.row(i) = out.row(i) - (out_ctr.transpose().eval());
			}

			// SVD
			Eigen::MatrixXd Cov = (in.transpose().eval()) * out;
			Eigen::JacobiSVD<Eigen::MatrixXd> svd(Cov, Eigen::ComputeThinU | Eigen::ComputeThinV);

			// Find the rotation
			double d = (svd.matrixV() * svd.matrixU().transpose()).determinant();
			if (d > 0)
				d = 1.0;
			else
				d = -1.0;

			Eigen::Matrix3d I = Eigen::Matrix3d::Identity(3, 3);
			I(2, 2) = d;
			Eigen::Matrix3d R = svd.matrixV() * (I * (svd.matrixU().transpose().eval()));

			out_ctr -= in_ctr;

			double score = 0;

			double S1 = std::sqrt((((R - Rotation_IMU).cwiseAbs2()).sum()) / 9.0);
			double S2 = std::sqrt((((R - Rotation_Prev).cwiseAbs2()).sum()) / 9.0);
			double S3 = std::sqrt((((out_ctr - DeltaPosition_Prev).cwiseAbs2()).sum()) / 3.0);

			score = (0.8 * S1) + (0.15 * S3) + (0.05 * S2);

			//std::cout << out_ctr << "\n\n";
			//std::cout << R << "\n\n";
			//std::cout << score << "\n\n";

			if (score < BestScore)
			{
				BestScore = score;
				BestIndex = i;
			}
		}

		if ((BestIndex >= 0) && (BestScore < 1.0))
		{
			std::vector< Eigen::Vector3d> Source;
			std::vector< Eigen::Vector3d> Target;

			for (int j = 0; j < Groups[BestIndex].size(); j++)
			{
				// Deproject from pixel to point in 3D
				float upixel[2]; // From pixel
				float upoint[3]; // From point (in 3D)

				float vpixel[2]; // To pixel
				float vpoint[3]; // To point (in 3D)

				// Copy pixels into the arrays (to match rsutil signatures)
				upixel[0] = keypoints[good_matches[BestIndex].queryIdx].pt.x;
				upixel[1] = keypoints[good_matches[BestIndex].queryIdx].pt.y;

				vpixel[0] = keypoints_Prev[good_matches[BestIndex].trainIdx].pt.x;
				vpixel[1] = keypoints_Prev[good_matches[BestIndex].trainIdx].pt.y;

				// Query the frame for distance
				// Note: this can be optimized
				// It is not recommended to issue an API call for each pixel
				// (since the compiler can't inline these)
				// However, in this example it is not one of the bottlenecks

				auto udist = depth.get_distance(upixel[0], upixel[1]);
				auto vdist = depth.get_distance(vpixel[0], vpixel[1]);

				// Deproject from pixel to point in 3D
				rs2_deproject_pixel_to_point(upoint, &intr, upixel, udist);
				rs2_deproject_pixel_to_point(vpoint, &intr, vpixel, vdist);

				Source.push_back(Eigen::Vector3d(upoint[0], upoint[1], upoint[2]));
				Target.push_back(Eigen::Vector3d(vpoint[0], vpoint[1], vpoint[2]));
			}

			Eigen::MatrixX3d in;
			Eigen::MatrixX3d out;

			in.resize(Source.size(), 3);
			out.resize(Target.size(), 3);

			for (int i = 0; i < Source.size(); i++)
			{
				in.row(i) = Source[i];
				out.row(i) = Target[i];
			}

			Eigen::Vector3d in_ctr = in.colwise().mean();
			Eigen::Vector3d out_ctr = out.colwise().mean();

			for (int i = 0; i < 3; i++)
			{
				in.row(i) = in.row(i) - (in_ctr.transpose().eval());
				out.row(i) = out.row(i) - (out_ctr.transpose().eval());
			}

			// SVD
			Eigen::MatrixXd Cov = (in.transpose().eval()) * out;
			Eigen::JacobiSVD<Eigen::MatrixXd> svd(Cov, Eigen::ComputeThinU | Eigen::ComputeThinV);

			// Find the rotation
			double d = (svd.matrixV() * svd.matrixU().transpose()).determinant();
			if (d > 0)
				d = 1.0;
			else
				d = -1.0;

			Eigen::Matrix3d I = Eigen::Matrix3d::Identity(3, 3);
			I(2, 2) = d;
			Eigen::Matrix3d R = svd.matrixV() * (I * (svd.matrixU().transpose().eval()));

			out_ctr -= in_ctr;

			Rotation_Prev = Rotation;
			//Rotation = (0.2 * R) + (0.8 * Rotation_IMU);
			//Rotation = R;
			Rotation = Rotation_IMU;

			DeltaPosition_Prev = DeltaPosition;
			DeltaPosition = out_ctr;
		}

		double X = DeltaPosition(0);
		double Y = DeltaPosition(1);
		double Z = DeltaPosition(2);

		JsonValues["SENDER"] = "ANCHORING";
		JsonValues["X"] = (double)X;
		JsonValues["Y"] = (double)Y;
		JsonValues["Z"] = (double)Z;
		JsonValues["YAW"] = (double)DeltaTheta.y;
		JsonValues["PITCH"] = (double)DeltaTheta.x;
		JsonValues["ROLL"] = (double)DeltaTheta.z;

		Json::StreamWriterBuilder wbuilder;
		wbuilder.settings_["commentStyle"] = "None";
		wbuilder.settings_["indentation"] = "";
		std::string document;

#ifdef _DEBUG
		{
			document = "{\"PITCH\":" + std::to_string(DeltaTheta.x) +
				",\"ROLL\":" + std::to_string(DeltaTheta.z) +
				",\"SENDER\":\"ANCHORING\",\"X\":" + std::to_string(X) +
				",\"Y\":" + std::to_string(Y) +
				",\"YAW\":" + std::to_string(DeltaTheta.y) +
				",\"Z\":" + std::to_string(Y) + "}\"";
		}
#endif
#ifndef _DEBUG
		{
			document = Json::writeString(wbuilder, JsonValues);
		}
#endif
		file_id << document << "\n";

		//---------------------------------------------
	    // Send a datagram to the receiver
		iResult = sendto(SendSocket,
			document.c_str(), (int)document.length(), 0, (SOCKADDR *)& RecvAddr, sizeof(RecvAddr));
		if (iResult == SOCKET_ERROR) {
			wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
			closesocket(SendSocket);
			WSACleanup();
			return 1;
		}

				
		//-- Draw only "good" matches
		drawMatches(color_image, keypoints, color_image_Prev, keypoints_Prev,
			good_matches, Output, cv::Scalar::all(-1), cv::Scalar::all(-1),
			std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

		descriptors_Prev = descriptors.clone();
		color_image_Prev = color_image.clone();
		keypoints_Prev = keypoints;
		thetaPrev = theta;

		tm = clock() - tm;

		if (m_bVerbose)
		{
			float FPS = CLOCKS_PER_SEC / ((float)tm);
			std::cout << std::fixed << std::setprecision(4) <<
				DeltaPosition.transpose().eval() << " | "
				<< DeltaTheta.x << " " << DeltaTheta.y << " " << DeltaTheta.z
				<< "  (" << FPS << " FPS)      " << "\r";

		//	printf("It took me %d clicks (%f seconds) --> %f FPS.\t\n", tm, ((float)tm) / CLOCKS_PER_SEC, FPS);
		}

		if (m_bDisplay)
		{
			cv::imshow("Color", Output);
			cv::imshow("Depth", depth_mat);
			char c = (char)cv::waitKey(1);
		}

		//if (GetKeyState(VK_CANCEL) & 0x8000)
		if (GetKeyState(VK_ESCAPE) & 0x8000)
			break;
	}

	if (m_bDisplay)
	{
		cv::destroyAllWindows();
	}

	//system("pause");

	file_id.close();

	//---------------------------------------------
	// When the application is finished sending, close the socket.
	iResult = closesocket(SendSocket);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"closesocket failed with error: %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Clean up and quit.
	WSACleanup();

	return EXIT_SUCCESS;
}
