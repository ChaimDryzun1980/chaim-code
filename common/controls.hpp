#pragma once 

namespace Controls
{
	void computeMatricesFromInputs();
	glm::mat4 getModelMatrix();
	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();

	void setModelMatrix(const glm::mat4 &inputMatrix);
	void setViewMatrix(const glm::mat4 &inputMatrix);
	void setProjectionMatrix(const glm::mat4 &inputMatrix);

	void SetFOV(const float inputFOV);
	void SetFocalLength(const float inputFocal);
	void SetInfinity(const float inputFocal);
	float GetFOV();
	float GetFocalLength();
	float getInfinity();
}

