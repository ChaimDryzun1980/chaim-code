// Include GLFW
#include <GLFW/glfw3.h>
extern GLFWwindow* window; // The "extern" keyword here is to access the variable "window" declared in tutorialXXX.cpp. This is a hack to keep the tutorials simple. Please avoid this.

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>

using namespace glm;

#include "controls.hpp"

glm::mat4 ModelMatrix;
glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;

glm::mat4 Controls::getModelMatrix() {
	return ModelMatrix;
}
glm::mat4 Controls::getViewMatrix(){
	return ViewMatrix;
}
glm::mat4 Controls::getProjectionMatrix(){
	return ProjectionMatrix;
}

void Controls::setModelMatrix(const glm::mat4 &inputMatrix)
{
	ModelMatrix = glm::mat4(inputMatrix);
}
void Controls::setViewMatrix(const glm::mat4 &inputMatrix)
{
	ViewMatrix = glm::mat4(inputMatrix);
}
void Controls::setProjectionMatrix(const glm::mat4 &inputMatrix)
{
	ProjectionMatrix = glm::mat4(inputMatrix);
}

// Initial position : on +Z
glm::vec3 position = glm::vec3( 0, 0, 5 ); 

// Initial horizontal angle : toward -Z
float horizontalAngle = 3.14f;

// Initial vertical angle : none
float verticalAngle = 0.0f;

// Initial Field of View
float initialFoV = 45.0f;
float initialFocal = 0.1f;
float initialInfinity = 100.0f;

float speed = 1.0f; // 3 units / second
float mouseSpeed = 0.0003f;

vec3 gCntrlPosition1(0.0f, 0.0f, 0.0f);
vec3 gCntrlOrientation1;
vec3 gCntrlScale1(1.0f, 1.0f, 1.0f);

void Controls::SetFOV(const float inputFOV)
{
	initialFoV = inputFOV;
}
void Controls::SetFocalLength(const float inputFocal)
{
	initialFocal = inputFocal;
}
void Controls::SetInfinity(const float inputFocal)
{
	initialInfinity = inputFocal;
}

float Controls::GetFOV()
{
	return initialFoV;
}
float Controls::GetFocalLength()
{
	return initialFocal;
}
float Controls::getInfinity()
{
	return initialInfinity;
}


void Controls::computeMatricesFromInputs(){

	int width = 1024;
	int height = 768;

	glfwGetWindowSize(window, &width, &height);

	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// Get mouse position
	double xpos, ypos;
	//glfwGetCursorPos(window, &xpos, &ypos);

	// Reset mouse position for next frame
	//glfwSetCursorPos(window, width/2, height/2);

	// Compute new orientation
	//horizontalAngle += mouseSpeed * float(width /2 - xpos );
	//verticalAngle   += mouseSpeed * float(height /2 - ypos );

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 direction(
		cos(verticalAngle) * sin(horizontalAngle), 
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
	);
	
	// Right vector
	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - 3.14f/2.0f), 
		0,
		cos(horizontalAngle - 3.14f/2.0f)
	);
	
	// Up vector
	glm::vec3 up = glm::cross( right, direction );

	// Move forward
	if (glfwGetKey( window, GLFW_KEY_UP ) == GLFW_PRESS){
		position += direction * deltaTime * speed;
	}
	// Move backward
	if (glfwGetKey( window, GLFW_KEY_DOWN ) == GLFW_PRESS){
		position -= direction * deltaTime * speed;
	}
	// Strafe right
	if (glfwGetKey( window, GLFW_KEY_RIGHT ) == GLFW_PRESS){
		position += right * deltaTime * speed;
	}
	// Strafe left
	if (glfwGetKey( window, GLFW_KEY_LEFT ) == GLFW_PRESS){
		position -= right * deltaTime * speed;
	}

	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
		gCntrlOrientation1.x += 3.14159f / 2.0f * deltaTime;
	}

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		gCntrlOrientation1.x -= 3.14159f / 2.0f * deltaTime;
	}

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		gCntrlOrientation1.y += 3.14159f / 2.0f * deltaTime;
	}

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		gCntrlOrientation1.y -= 3.14159f / 2.0f * deltaTime;
	}

	if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS) {
		gCntrlOrientation1.z += 3.14159f / 2.0f * deltaTime;
	}

	if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS) {
		gCntrlOrientation1.z -= 3.14159f / 2.0f * deltaTime;
	}
	
	if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
		gCntrlScale1.x += speed * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
		gCntrlScale1.x -= speed * deltaTime;
		if (gCntrlScale1.x < 0.000001)
			gCntrlScale1.x = 0.000001;
	}
	if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS) {
		gCntrlScale1.y += speed * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
		gCntrlScale1.y -= speed * deltaTime;
		if (gCntrlScale1.y < 0.000001)
			gCntrlScale1.y = 0.000001;
	}
	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS) {
		gCntrlScale1.z += speed * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS) {
		gCntrlScale1.z -= speed * deltaTime;
		if (gCntrlScale1.z < 0.000001)
			gCntrlScale1.z = 0.000001;
	}
	if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS) {
		gCntrlScale1.x += speed * deltaTime;
		gCntrlScale1.y += speed * deltaTime;
		gCntrlScale1.z += speed * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS) {
		gCntrlScale1.x -= speed * deltaTime;
		gCntrlScale1.y -= speed * deltaTime;
		gCntrlScale1.z -= speed * deltaTime;
		if (gCntrlScale1.z < 0.000001)
			gCntrlScale1.z = 0.000001;
		if (gCntrlScale1.y < 0.000001)
			gCntrlScale1.y = 0.000001;
		if (gCntrlScale1.x < 0.000001)
			gCntrlScale1.x = 0.000001;
	}


	//float FoV = initialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this. It's a bit too complicated for this beginner's tutorial, so it's disabled instead.
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
		initialFoV -= 1;
	}
	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
		initialFoV += 1;
	}

	if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) {
		initialFocal -= 1;
	}
	if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS) {
		initialFocal += 1;
	}
	if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS) {
		initialInfinity -= 1;
	}
	if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS) {
		initialInfinity += 1;
	}


	// Build the model matrix
	glm::mat4 RotationMatrix = eulerAngleYXZ(gCntrlOrientation1.y, gCntrlOrientation1.x, gCntrlOrientation1.z);
	glm::mat4 TranslationMatrix = translate(mat4(), gCntrlPosition1); // A bit to the left
	glm::mat4 ScalingMatrix = scale(mat4(), gCntrlScale1);
	ModelMatrix = TranslationMatrix * RotationMatrix * ScalingMatrix;

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	ProjectionMatrix = glm::perspective(glm::radians(initialFoV), (float)width / (float)height, initialFocal, initialInfinity);
	// Camera matrix
	ViewMatrix       = glm::lookAt(
								position,           // Camera is here
								position+direction, // and looks here : at the same position, plus "direction"
								up                  // Head is up (set to 0,-1,0 to look upside-down)
						   );

	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
}